const { generateText } = require('../src/Util.js');

describe('Pruebas de Salidas de Datos', () => {
    test('Salida con datos', () => {
      const text = generateText('Daniel',30);
      expect(text).toBe('Daniel (30 years old)');
      const text2 = generateText('Lucas',25);
      expect(text2).toBe('Lucas (25 years old)');
    });
    test('Salida con datos vacios', () =>{
      const text = generateText('',null);
      expect(text).toBe(' (null years old)');
    });
    test('Salida sin datos', () =>{
      const text = generateText();
      expect(text).toBe('undefined (undefined years old)');
    });
  })
  
  
