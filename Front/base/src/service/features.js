import { baseUrl } from "./categorias";

export const getFeatures = async () => {
    try {
        let url = `${baseUrl}features/`
        const response = await fetch(url);
        if (!response.ok) {
            throw Error("Resource could not be located")
        }
        const data = await response.json();
        return data;
    }   catch (err) {console.log(err)}
};

export const getFeatureById = async (id) => {
    try {
        let url = `${baseUrl}features/${id}`
        const response = await fetch(url);
        if (!response.ok) {
            throw Error("Resource could not be located")
        }
        const data = await response.json();
        return data;
    }   catch (err) {console.log(err)}
};