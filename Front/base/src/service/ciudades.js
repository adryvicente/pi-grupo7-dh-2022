import { baseUrl } from "./categorias";

export const getCiudades = async () => {
    try {
        let url = `${baseUrl}cities/`
        const response = await fetch(url);
        if (!response.ok) {
            throw Error("Resource could not be located")
        }
        const data = await response.json();
        return data;
    }   catch (err) {console.log(err)}
};

export const getCiudadByName = async (name) => {
    try {
        let url = `${baseUrl}cities/name/${name}`
        const response = await fetch(url);
        if (!response.ok) {
            throw Error("Resource could not be located")
        }
        const data = await response.json();
        return data;
    }   catch (err) {console.log(err)}
};
