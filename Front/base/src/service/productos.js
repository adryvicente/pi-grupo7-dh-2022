import { baseUrl } from "./categorias";

export const getProductos = async () => {
    try {
        let url = `${baseUrl}products`
        const response = await fetch(url);
        if (!response.ok) {
            throw Error("Resource could not be located")
        }
        const data = await response.json();
        return data;
    }   catch (err) {console.log(err)}
};

export const getProductById = async (id) => {
    try {
        let url = `${baseUrl}products/${id}`
        const response = await fetch(url);
        if (!response.ok) {
            throw Error("Resource could not be located")
        }
        const data = await response.json();
        return data;
    }   catch (err) {console.log(err)}
};

export const getProductByCategory = async (id) => {
    try {
        let url = `${baseUrl}products/category/${id}`
        const response = await fetch(url);
        if (!response.ok) {
            throw Error("Resource could not be located")
        }
        const data = await response.json();
        return data;
    }   catch (err) {console.log(err)}
};

export const getProductByCity = async (city) => {
    try {
        let url = `${baseUrl}products/cities/${city}`
        const response = await fetch(url);
        if (!response.ok) {
            throw Error("Resource could not be located")
        }
        const data = await response.json();
        return data;
    }   catch (err) {console.log(err)}
};

export const getProductByDates = async (city, checkin, checkout) => {
    try {
        let url = `${baseUrl}products/filters?city=${city}&checkin=${checkin}&checkout=${checkout}`
        const response = await fetch(url);
        if (!response.ok) {
            throw Error("Resource could not be located")
        }
        const data = await response.json();
        return data;
    }   catch (err) {console.log(err)}
};

export const getProductByAdmin = async (token) => {
    try {
        let url = `${baseUrl}products/my-products`
        const response = await fetch(url, {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "Authorization": token
            }
        });
        if (!response.ok) {
            throw Error("Resource could not be located")
        }
        const data = await response.json();
        return data;
    }   catch (err) {console.log(err)}
};

export const postProduct = async (data, token) => {
    let url = `${baseUrl}products`;
    try { 
        const response = await fetch(url, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "Authorization": token
            },
            body: JSON.stringify(data),
        });
        if (!response.ok) {
            return response
        }
        const result = await response.json();
        return result;
    } catch (e) {
        console.log("catch error: ", e);
    }
};

export const patchProduct = async (data, token, id) => {
    let url = `${baseUrl}products/${id}`;
    try { 
        const response = await fetch(url, {
            method: "PATCH",
            headers: {
                "Content-Type": "application/json",
                "Authorization": token
            },
            body: JSON.stringify(data),
        });
        if (!response.ok) {
            return response
        }
        const result = await response.json();
        return result;
    } catch (e) {
        console.log("catch error: ", e);
    }
};