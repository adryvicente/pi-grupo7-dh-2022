import { baseUrl } from "./categorias";

export const postBooking = async (data, token) => {
    let url = `${baseUrl}reserves`;
    try {
        
        const response = await fetch(url, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "Authorization": token
            },
            body: JSON.stringify(data),
        });
        if (!response.ok) {
            return response
        }
        const result = await response.json();
        return result;
    } catch (e) {
        console.log("catch error: ", e);
    }
};

export const getBookingsByProduct = async (id) => {
    try {
        let url = `${baseUrl}reserves/products/${id}`
        const response = await fetch(url);
        if (!response.ok) {
            return response
        }
        const data = await response.json();
        return data;
    }   catch (err) {console.log(err)}
};

export const getBookingByUser = async (token) => {
    let url = `${baseUrl}reserves`;
    try {
        
        const response = await fetch(url, {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "Authorization": token
            }
        });
        if (!response.ok) {
            return response
        }
        const result = await response.json();
        return result;
    } catch (e) {
        console.log("catch error: ", e);
    }
};