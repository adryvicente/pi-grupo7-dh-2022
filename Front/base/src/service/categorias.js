export const baseUrl = "http://localhost:8080/";

export const getCategories = async () => {
    try {
        let url = `${baseUrl}categories/`
        const response = await fetch(url);
        if (!response.ok) {
            throw Error("Resource could not be located")
        }
        const data = await response.json();
        return data;
    }   catch (err) {console.log(err)}
};

export const getCategoryById = async (id) => {
    try {
        let url = `${baseUrl}categories/${id}`
        const response = await fetch(url);
        if (!response.ok) {
            throw Error("Resource could not be located")
        }
        const data = await response.json();
        return data;
    }   catch (err) {console.log(err)}
};