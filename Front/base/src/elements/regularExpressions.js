
export const regular_expressions = {
    name: /^[a-zA-ZÀ-ÿ\s]{1,40}$/, // Letras y espacios, pueden llevar acentos.
    mail: /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/,
    password: /^.{6,12}$/, // 4 a 12 digitos.
    texto: /^[a-zA-ZÀ-ÿ\s]{1,50}$/, 
    notEmpty: /^(?!\s*$).+/,
}