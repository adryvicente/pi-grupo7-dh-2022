import styled, { css } from "styled-components";
import "../styles/global_variables.css";

const colores = {
    borde: "#F0572D",
    error: "#bb2929",
    exito: "#1ed12d",
};

const Formulario = styled.form`
    display: flex;
    flex-direction: column;
    align-items: stretch;
    justify-content: center;
`;

const Label = styled.label`
    display: block;
    font-weight: var(--fontWeightH);
    padding: 1rem;
    min-height: 40px;
    cursor: pointer;
    ${(props) =>
        props.valid === "false" &&
        css`
            color: ${colores.error};
        `}
`;

const GrupoInput = styled.div`
    margin-top: 0.5rem;
    width: 100%;
    display: flex;
    justify-content: flex-start;
    gap: 0.5rem;
`;

const Input = styled.input`
    width: 100%;
    background: #fff;
    border-radius: 3px;
    height: 45px;
    line-height: 45px;
    padding: 0 40px 0 10px;
    transition: 0.3s ease all;
    border: 3px solid var(--fourthColor);
    &:focus {
        border: 3px solid ${colores.borde};
        outline: none;
        box-shadow: 3px 0px 30px rgba(163, 163, 163, 0.4);
    }
    ${(props) =>
        props.valid === "true" &&
        css`
            border: 3px solid transparent;
        `}
    ${(props) =>
        props.valid === "false" &&
        css`
            border: 3px solid ${colores.error} !important;
        `}
    &:disabled {
        cursor: not-allowed;
    }
`;

const TextArea = styled.textarea`
    width: 100%;
    background: #fff;
    border-radius: 3px;
    height: 45px;
    line-height: 45px;
    padding: 0 40px 0 10px;
    transition: 0.3s ease all;
    border: 3px solid var(--fourthColor);
    resize: none;
    &:focus {
        border: 3px solid ${colores.borde};
        outline: none;
        box-shadow: 3px 0px 30px rgba(163, 163, 163, 0.4);
    }
    ${(props) =>
        props.valid === "true" &&
        css`
            border: 3px solid transparent;
        `}
    ${(props) =>
        props.valid === "false" &&
        css`
            border: 3px solid ${colores.error} !important;
        `}
`;

const ErrorLabel = styled.p`
    font-size: 12px;
    margin-bottom: 0;
    color: ${colores.error};
    display: none;
    ${(props) =>
        props.valid === "true" &&
        css`
            display: none;
        `}
    ${(props) =>
        props.valid === "false" &&
        css`
            display: block;
        `}
`;

const BlockSubmit = styled.div`
    margin-top: 1rem;
    background: #f66060;
    border-radius: 5px;
    padding: 0.5rem;
    p {
        margin: 0;
    }
    b {
        margin-left: 10px;
    }
`;

export {
    Formulario,
    Label,
    GrupoInput,
    Input,
    ErrorLabel,
    BlockSubmit,
    TextArea,
};
