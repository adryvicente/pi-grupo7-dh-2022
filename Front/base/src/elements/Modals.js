import styled from 'styled-components';

export const Overlay = styled.div`
    width: 100vw;
    height: 100vh;
    position: fixed;
    top: 0;
    left: 0;
    background: rgba(0,0,0,0.5);
    padding: 40px;
    display: flex;
    align-items: center;
    justify-content: center;
    z-index: 99;
`

export const ModalContainer = styled.div`
    width: 500px;
    min-height: 100px;
    position: relative;
    background: #fff;
    border-radius: 5px;
    box-shadow: 11px 10px 36px 12px rgba(0,0,0,0.6); 
    padding: 20px;
`

export const ModalHeader = styled.div`
    display: flex;
    align-items: center;
    justify-content: space-between;
    margin-bottom: 20px;
    padding-bottom: 20px;
    border-bottom: 1px solid #e8e8e8;

    h3 {
        font-weight: 500;
        font-size: 16px;
        color: var(--primaryColor)
    }
    
`

export const CloseButton = styled.div`
    position: absolute;
    top: 20px;
    right: 20px;

    width: 30px;
    height: 30px;
    border: none;
    background: none;
    cursor: pointer;
    transition: .3s ease all;
    border-radius: 5px;

    &:hover {
        background: #f2f2f2;
    }
    
    svg {
        width: 100%;
        height: 100%;
    }
   
`

export const ModalContent = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;

    h1 {
        font-size: 42px;
        font-weight: 700;
        margin-bottom: 10px;
    }

    p {
        font-size: 18px;
        margin-bottom: 20px;
    }

    svg {
        width: 100%;
        vertical-align: top;
    }

`