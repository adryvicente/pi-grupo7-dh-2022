import React, { useEffect, useRef, useState } from "react";
import Error from "./Error";
import { getFeatureById, getFeatures } from "../service/features";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { library } from "@fortawesome/fontawesome-svg-core";
import { fas } from "@fortawesome/free-solid-svg-icons";
import styles from "../styles/featurePicker.module.css";

library.add(fas);

const FeaturePicker = ({ setFeature, feature, update, supplier }) => {
    const [search, setSearch] = useState("");
    const [display, setDisplay] = useState(false);
    const [options, setOptions] = useState([]);
    const [select, setSelect] = useState([]);

    const wrapperRef = useRef(null);

    useEffect(() => {
        const fetchFeatures = async () => {
            try {
                const data = await getFeatures();
                setOptions(data.listFeatureResponse);
            } catch (err) {
                console.log(err);
            }
        };
        fetchFeatures();
    }, []);

    useEffect(() => {

        const fetchFeatureById = async (id) => {
            try {
                const data = await getFeatureById(id);

                select.push(data);
                
                options.splice(
                    options.findIndex(
                        (object) => object.featureId === data.featureId
                    ),
                    1
                );
            } catch (err) {
                console.log(err);
            }
        };
        if (update) {
            if (supplier.length > 0) {

                supplier.map((f) => fetchFeatureById(f.featureId));
                supplier.map((f) => f.featureId).map((f) => feature.value.push(f))
            }
        }
    }, [update, supplier]);

    const handleClickOutside = (event) => {
        const { current: wrap } = wrapperRef;
        if (wrap && !wrap.contains(event.target)) {
            setDisplay(false);
            setSearch("");
        }
    };

    useEffect(() => {
        window.addEventListener("mousedown", handleClickOutside);
        return () => {
            window.removeEventListener("mousedown", handleClickOutside);
        };
    });

    useEffect(() => {
        const validate = () => {
            if (feature.value.length === 0) {
                setFeature({ ...feature, valid: "false" });
            } else {
                setFeature({ ...feature, valid: "true" });
            }
        };
        validate();
    }, [display]);

    if (options === undefined || options.length === 0) {
        return <Error />;
    }

    return (
        <div>
            <div ref={wrapperRef}>
                <div
                    className={styles.input_wrap}
                    onClick={() => setDisplay(!display)}
                >
                    <FontAwesomeIcon
                        icon="fa-solid fa-magnifying-glass"
                        size="2x"
                    />
                    <input
                        placeholder="Buscar atributo"
                        value={search}
                        onChange={(e) => {
                            setSearch(e.target.value);
                        }}
                        className={styles.input}
                    />
                </div>

                {display && (
                    <div className={styles.gallery}>
                        {options
                            .filter(
                                ({ name }) =>
                                    name.indexOf(search.toUpperCase()) > -1
                            )
                            .map((f, index) => {
                                return (
                                    <button
                                        key={f.featureId}
                                        onClick={(e) => {
                                            e.preventDefault();
                                            setSearch(f.name);
                                            select.push(f);
                                            feature.value.push(f.featureId);
                                            options.splice(index, 1);
                                            setDisplay(false);
                                            setSearch("");
                                        }}
                                        className={styles.option}
                                    >
                                        <FontAwesomeIcon
                                            icon={f.icon}
                                            size="3x"
                                        />
                                        <span>
                                            {f.name.charAt(0).toUpperCase() +
                                                f.name.slice(1).toLowerCase()}
                                        </span>
                                    </button>
                                );
                            })}
                    </div>
                )}
            </div>

            {select.length > 0 && (
                <div id={styles.select}>
                    {select.map((f, index) => {
                        return (
                            <div
                                key={f.featureId}
                                className={styles.selected}
                                onClick={(e) => {
                                    e.preventDefault();
                                    feature.value.splice(f, 1);
                                    Object.assign(feature);
                                    select.splice(index, 1);
                                    setSelect([...select]);
                                }}
                            >
                                <FontAwesomeIcon icon={f.icon} size="3x" />
                                <span>
                                    {f?.name.charAt(0).toUpperCase() +
                                        f?.name.slice(1).toLowerCase()}
                                </span>

                                <FontAwesomeIcon icon="fa-solid fa-trash" />
                            </div>
                        );
                    })}
                </div>
            )}
        </div>
    );
};

export default FeaturePicker;
