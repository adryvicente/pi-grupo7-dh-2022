import React, { useState } from "react";
import ImageSlider from "./ImageSlider";
import styles from "../styles/imageGallery.module.css";
import ImageAutoplay from "./ImageAutoplay";
import Error from "./Error";
import Loading from "./Loading";

const ImageComponent = ({ images, loading }) => {
    const [openSlider, setOpenSlider] = useState(false);

    const handleSlider = () => {
        setOpenSlider(!openSlider);
    };

    if (images === undefined) {
        return <Error />;
    }

    return (
        <>
            {loading ? (
                <Loading />
            ) : (
                <>
                    <div className={styles.wrapper}>
                        <div className={styles.gallery}>
                            {images.map((data) => (
                                <img
                                    className={styles.image}
                                    src={data.imageUrl}
                                    alt={data.title}
                                    key={data.imageId}
                                />
                            ))}
                        </div>
                        {openSlider === false ? (
                            <button
                                className={styles.btn}
                                onClick={handleSlider}
                            >
                                {" "}
                                Ver más{" "}
                            </button>
                        ) : (
                            ""
                        )}
                        {openSlider && (
                            <ImageSlider
                                images={images}
                                handleSlider={handleSlider}
                            />
                        )}
                    </div>
                    <ImageAutoplay images={images} />
                </>
            )}
        </>
    );
};

export default ImageComponent;
