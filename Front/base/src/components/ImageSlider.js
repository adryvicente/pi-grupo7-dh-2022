import React, { useState } from 'react'
import styles from '../styles/imageSlider.module.css'
import {FaRegWindowClose} from "react-icons/fa";

const ImageGallery = ({handleSlider, images}) => {

    const [sliderData, setsliderData] = useState(images[0])

    const handleClick = (index) => {
        const slider = images[index]
        setsliderData(slider)
    }

  return (

    <div className={styles.container}>
    
        <div className={styles.modal}>
            <FaRegWindowClose onClick={handleSlider}  className={styles.esc} />
            <img src={sliderData.imageUrl} alt="current" id={styles.current_img} />
            <div className={styles.row}>
            {
                images.map( (data, i) => <img 
                    className={sliderData.id===(i+1) ? `${styles.clicked}` : ""} 
                    src={data.imageUrl} 
                    alt={data.title} 
                    key={data.imageId} 
                    onClick={() => handleClick(i)} 
                    id={styles.thumbnail_img} 
                    />
                )
            }
            </div>
        </div>
    </div>
  )
}

export default ImageGallery