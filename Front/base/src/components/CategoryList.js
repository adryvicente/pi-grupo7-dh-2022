import React, { useEffect, useState } from "react";
import Category from "./Category";
import styles from "../styles/categoryList.module.css";
import { getCategories } from "../service/categorias";
import { FaGlobe } from "react-icons/fa";
import Loading from "./Loading";

const CategoryList = ({
    setActiveCategory,
    setFilter,
    setActiveCard,
    activeCard,
    scrollToSection,
    resultCards,
}) => {
    const [categorias, setCategorias] = useState([]);
    const [loading, setLoading] = useState(true);

    const fetchCategorias = async () => {
        try {
            const data = await getCategories();
            setCategorias(data.listCategoryResponse);
            setLoading(false);
        } catch (err) {
            console.log(err);
        }
    };

    useEffect(() => {
        fetchCategorias();
    }, []);

    if (categorias === undefined || categorias.length === 0) {
        return (
            <div className={styles.error}>
                <FaGlobe />
                <span> ¡Disculpa, aún estamos trabajando en esto! </span>
            </div>
        );
    }

    return (
        <div className={styles.container}>
            <h2>Buscar por tipo de alojamiento</h2>
            {loading ? (
                <Loading />
            ) : (
                <div className={styles.card}>
                    {categorias.map((c) => {
                        return (
                            <Category
                                key={c.idCategory}
                                title={c.title}
                                img={c.urlImage}
                                setActiveCategory={setActiveCategory}
                                id={c.idCategory}
                                setFilter={setFilter}
                                activeCard={activeCard}
                                setActiveCard={setActiveCard}
                                scrollToSection={scrollToSection}
                                resultCards={resultCards}
                            />
                        );
                    })}
                </div>
            )}
        </div>
    );
};

export default CategoryList;
