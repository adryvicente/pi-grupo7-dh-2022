import React from "react";
import styles from "../styles/headerProducts.module.css";
import { useNavigate } from "react-router-dom";
import { FaChevronLeft, FaMapPin } from "react-icons/fa";

const HeaderProducts = ({ categoryTitle, name, ciudades, address }) => {
    const navigate = useNavigate();

    return (
        <div>
            <div className={styles.header_conteiner}>
                <div className={styles.product}>
                    <h2> {categoryTitle ? categoryTitle : ""} </h2>
                    <h1>{name ? name : ""}</h1>
                </div>

                <button className={styles.back} onClick={() => navigate(-1)}>
                    <FaChevronLeft color="white" />
                </button>
            </div>

            <div className={styles.location_wrap}>
                <FaMapPin />
                <div className={styles.address}>
                    {ciudades && (
                        <h3>
                            {ciudades.name}, {ciudades.province}.
                             {ciudades.country}
                        </h3>
                    )}
                    <h4> {address} </h4>
                </div>
            </div>
        </div>
    );
};

export default HeaderProducts;
