import React, { useEffect, useState } from 'react'
import {HiOutlineCheckCircle} from "react-icons/hi";
import styles from '../styles/bookkingArrival.module.css'

const BookingArrival = ({setArrival}) => {

    const [hours, setHours] = useState([])


    useEffect(() => {
        const getHours = () => {
            
            let hours = []

            for (let i=0; i < 10; i++) {
                hours.push(`0${i}:00`);
            }
            for (let i=10; i < 24; i++) {
                hours.push(`${i}:00`);
            }

            setHours(hours)
        }   
        getHours()     
    }, [])


  return (
    <div className={styles.container}>
        <h2> Tu horario de llegada </h2>
        <div className={styles.wrapper} >
            <div className={styles.title}>
                <div style={{"display": "flex"}}>
                    <HiOutlineCheckCircle size="3vh" />
                </div>
                <span> Tu habitación va a estar lista para el check-in entre las {hours[0]} y las {hours[hours.length -1]}  horas </span>
            </div>
            <span>Indica tu horario de llegada</span>
            <br />
            <select id="arrival" className={styles.input} name="arrival" onChange={(e) => setArrival(e.target.value) } >
                {
                    hours.map((h, index) => <option key={index}> {h} </option>)
                }
            </select>
        </div>
    </div>
  )
}

export default BookingArrival