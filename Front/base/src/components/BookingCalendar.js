import React, { useContext, useEffect } from "react";
import "react-date-range/dist/styles.css"; // main css file
import "react-date-range/dist/theme/default.css"; // theme css file
import { DateRange } from "react-date-range";
import useWindowSize from "../elements/useWindowSize";
import format from 'date-fns/format'
import ProductProvider from "../context/ProductProvider";

const BookingCalendar = ({setDates, availability}) => {
    const size = useWindowSize();
    const today = new Date();
    const { range, setRange } = useContext(ProductProvider)

    const formarDates = () => {
        try {
            let string = ` ${format(range[0].startDate, 'dd/MM/yyyy')} to ${format(range[0].endDate, 'dd/MM/yyyy')} `
            setDates(string.split(" to "))

        } catch (e) {
            console.log(e)
        }
    }

    useEffect(() => {
        formarDates()
        // eslint-disable-next-line
    }, [range])

    return (
        <div
            style={{
                width: "100%",
                background: "#fff",
                textAlign: "center",
                borderRadius: "8px",
                boxShadow: "0px 4px 4px rgba(0, 0, 0, 0.25)",
            }}
        >
            <DateRange
                onChange={(i) => setRange([i.selection])}
                editableDateInputs={true}
                moveRangeOnFirstSelection={false}
                ranges={range}
                months={size > 700 ? 2 : 1}
                direction="horizontal"
                rangeColors={["#F0572D"]}
                showDateDisplay={false}
                showMonthAndYearPickers={false}
                showPreview={true}
                minDate={today}
                monthDisplayFormat="MMMM"
                disabledDates={availability}
            />
        </div>
    );
};

export default BookingCalendar;
