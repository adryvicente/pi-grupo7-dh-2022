import React, { useContext, useState } from "react";

import btn from "../styles/button.module.css";

import styles from "../styles/header.module.css";
import logo from "../media/logo.png";
import logo_and_motto from "../media/logo_and_motto.svg";
import { useLocation, useNavigate } from "react-router-dom";
import { FaBars, FaTimes } from "react-icons/fa";

import SessionProvider from "../context/SessionProvider";

import facebook from "../media/icon facebook.svg";
import ig from "../media/icon ig.svg";
import linkedin from "../media/icon linkedin.svg";
import tweet from "../media/tweet.svg";
import User from "./User";
import UserProvider from "../context/UserProvider";
import AuthProvider from "../context/AuthProvider";

const Header = ({ header }) => {
    const navigate = useNavigate();

    const { user } = useContext(UserProvider);
    const { admin } = useContext(AuthProvider);

    const location = useLocation();

    const [clickBurguer, setBurguer] = useState(true);
    const [clickEsc, setEsc] = useState(false);

    const burguerClick = () => {
        setBurguer(!clickBurguer);
        setEsc(!clickEsc);
    };
    const escClick = () => {
        setEsc(!clickEsc);
        setBurguer(!clickBurguer);
    };

    const { isLogged, handleSession, endSession } = useContext(SessionProvider);
    return (
        <header ref={header}>
            <div className={styles.logo}>
                <button
                    className={`${styles.tablet_mobile} ${styles.btn}`}
                    onClick={() => navigate("/")}
                >
                    {" "}
                    <img src={logo} alt={logo} />{" "}
                </button>
                <button
                    className={`${styles.desktop} ${styles.btn}`}
                    onClick={() => navigate("/")}
                >
                    {" "}
                    <img src={logo_and_motto} alt={logo_and_motto} />{" "}
                </button>
            </div>
            <button
                className={`${styles.btn} ${styles.mobile} ${styles.icons}`}
                onClick={burguerClick}
            >
                <FaBars />
            </button>
            <nav
                className={
                    clickEsc
                        ? `${styles.navbar} ${styles.active}`
                        : `${styles.navbar}`
                }
            >
                {user && isLogged ? (
                    <>
                        {admin && (
                            <div>
                                <button
                                onClick={() => {navigate("/administration")
                                burguerClick()
                                }}
                                className={btn.avg_btn}
                                id={styles.admin_btn}
                                >Administración</button>
                            </div>
                        )}

                        {!admin && (
                            <div>
                                <button
                                onClick={() => {navigate("/my-bookings")
                                burguerClick()
                                }}
                                className={btn.avg_btn}
                                id={styles.admin_btn}
                                >Mis reservas</button>
                            </div>
                        )}

                        <div className={`${styles.user_nav} `}>
                            <div className={styles.top_guest_nav}>
                                <button
                                    onClick={() => {
                                        endSession();
                                        handleSession();
                                    }}
                                    className={`${styles.tablet_desktop} ${styles.btn}`}
                                    id={styles.close_session}
                                >
                                    <FaTimes />
                                </button>
                                <button
                                    className={`${styles.btn} ${styles.mobile} ${styles.icons} ${styles.esc} `}
                                    onClick={escClick}
                                >
                                    <FaTimes />
                                </button>
                                <div className={styles.user_container}>
                                    {user && <User />}
                                    <div className={styles.greeting_container}>
                                        <h2 className={styles.h2}>Hola,</h2>
                                        <hr />
                                        <span className={styles.user_name}>
                                            {user.name} {user.lastname}
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div className={styles.mobile_end_session}>
                                <span>
                                    ¿Deseas
                                    <button
                                        className={styles.btn}
                                        onClick={() => {
                                            endSession();
                                            handleSession();
                                        }}
                                        style={{ marginLeft: "10px" }}
                                    >
                                        cerrar sesión
                                    </button>
                                    ?
                                </span>
                                <hr className={styles.mobile} />
                            </div>
                        </div>
                    </>
                ) : (
                    <div className={`${styles.guest_nav} `}>
                        <div className={styles.top_guest_nav}>
                            <button
                                className={`${styles.btn} ${styles.mobile} ${styles.icons} ${styles.esc} `}
                                onClick={escClick}
                            >
                                <FaTimes />
                            </button>
                            <div className={styles.title}>
                                <h2 className={styles.mobile}>MENÚ</h2>
                            </div>
                        </div>
                        <div className={styles.nav_links}>
                            {location.pathname === "/signup" ? (
                                <> </>
                            ) : (
                                <button
                                    className={btn.avg_btn}
                                    onClick={() => {
                                        burguerClick();
                                        navigate("/signup");
                                    }}
                                >
                                    Crear cuenta
                                </button>
                            )}
                            <hr className={styles.mobile} />
                            {location.pathname === "/login" ? (
                                <> </>
                            ) : (
                                <button
                                    className={btn.avg_btn}
                                    onClick={() => {
                                        burguerClick();
                                        navigate("/login");
                                    }}
                                >
                                    Iniciar sesión
                                </button>
                            )}
                        </div>
                    </div>
                )}
                <div className={`${styles.social_media} ${styles.mobile}`}>
                    <img
                        className={styles.mobile}
                        src={facebook}
                        alt="facebook logo"
                    />
                    <img
                        className={styles.mobile}
                        src={linkedin}
                        alt="linkedin logo"
                    />
                    <img
                        className={styles.mobile}
                        src={tweet}
                        alt="tweet logo"
                    />
                    <img
                        className={styles.mobile}
                        src={ig}
                        alt="instagram logo"
                    />
                </div>
            </nav>
        </header>
    );
};

export default Header;
