import React from "react";
import { Navigate, Outlet } from "react-router-dom";

const PrivateRoutes = ({ isLogged }) => {
    return isLogged ? <Outlet /> : <Navigate to="/" />;
};

export default PrivateRoutes;
