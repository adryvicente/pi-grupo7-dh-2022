import React from "react";
import styles from "../styles/loading.module.css";

const Loading = () => {
    return (
        <div className={styles.center}>
            <div className={styles.ring}></div>
            <span className={styles.loading}>Cargando ...</span>
        </div>
    );
};

export default Loading;
