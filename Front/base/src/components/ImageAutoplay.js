import React from 'react'
import styles from '../styles/imageAutoplay.module.css'
import { Swiper, SwiperSlide } from 'swiper/react';
import { Navigation, Pagination, Autoplay } from 'swiper';
import 'swiper/css';
import 'swiper/css/navigation';
import 'swiper/css/pagination';
import 'swiper/css/scrollbar';
import 'swiper/css/autoplay';
import '../styles/helper.css'

const ImageAutoplay = ({images}) => {

  return (
    <>
        <Swiper
            modules={[Navigation, Pagination, Autoplay]}
            slidesPerView={1}
            autoplay={{ delay: 3000, disableOnInteraction: false }}
            className={styles.wrapper}
            pagination={{type: "fraction"}}
            allowTouchMove={false}
            navigation
            
    >
      {
        images.map( (data) => <SwiperSlide key={data.imageId}> <img className={styles.image} src={data.imageUrl} alt={data.title} /> </SwiperSlide>)
        }
    </Swiper>
</>
  )
}

export default ImageAutoplay