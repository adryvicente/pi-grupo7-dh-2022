import React, { useContext, useEffect, useState } from "react";
import { getBookingByUser } from "../service/booking";
import SessionProvider from "../context/SessionProvider";
import Loading from "./Loading";
import format from "date-fns/format";
import { useNavigate } from "react-router-dom";
import { AiOutlineCalendar, AiOutlineFieldTime } from "react-icons/ai";
import btn from "../styles/button.module.css";
import styles from '../styles/myBookings.module.css'
import ScrollToTop from "./ScrollToTop";
import Back from "./Back";

const MyBookings = () => {
    const { getToken } = useContext(SessionProvider);
    const [loading, setLoading] = useState(false);
    const [bookings, setBookings] = useState([]);
    const navigate = useNavigate();

    useEffect(() => {
        const fetchBookings = async () => {
            const token = getToken();
            try {
                setLoading(true);
                const data = await getBookingByUser(token);
                setBookings(data.listReservesResponse);
                setLoading(false);
            } catch (err) {
                console.log(err);
            }
        };
        fetchBookings();
    }, [getToken]);


    return (
        <>
            <ScrollToTop />
            <Back />
            {loading ? (
                <Loading />
            ) : (
                <div className={styles.container}>
                    <h1> {bookings.length === 0 ? "Cuando hagas tu primera reserva, podrás ver el contenido aquí" : "Mis reservas"} </h1>
                    <div className={styles.wrap}>
                        {bookings.map((b) => (
                            <div key={b.idReserve} className={styles.content}>
                                <div className={styles.element}>
                                    {" "}
                                    <AiOutlineCalendar />
                                    <p>
                                        {" "}
                                        {format(
                                            new Date(
                                                b.checkIn.replace(/-/g, "/")
                                            ),
                                            "dd/MM/yyyy"
                                        )}{" "}
                                    </p>
                                </div>
                                <div className={styles.element}>
                                    {" "}
                                    <AiOutlineCalendar />
                                    <p>
                                        {" "}
                                        {format(
                                            new Date(
                                                b.checkOut.replace(/-/g, "/")
                                            ),
                                            "dd/MM/yyyy"
                                        )}{" "}
                                    </p>
                                </div>
                                <div className={styles.element}>
                                    {" "}
                                    <AiOutlineFieldTime />
                                    <p> Check-in: {b.hourIn} </p>
                                </div>

                                <button
                                    onClick={() =>
                                        navigate(`/product/${b.productId}`)
                                    }
                                    className={btn.avg_btn}
                                >
                                    {" "}
                                    Ver Alojamiento{" "}
                                </button>
                            </div>
                        ))}
                    </div>
                </div>
            )}
        </>
    );
};

export default MyBookings;
