import React, { useRef, useState } from 'react'
import styles from "../styles/home.module.css"
import CategoryList from './CategoryList'
import ListCard from './ListCard'
import Searchbar from './Searchbar'

const Home = ({scrollToSection, header}) => {

  const [activeCategory, setActiveCategory] = useState(null);
  const [activeLocation, setActiveLocation] = useState(null);

  const [search, setSearch] = useState("");
  const [filter, setFilter] = useState(false);
  
  const [activeCard, setActiveCard] = useState(`${styles.inactive}`);

  const resultCards = useRef(null);

  return (
    <div className={styles.home_container}>
        <Searchbar 
            setActiveLocation={setActiveLocation} 
            setActiveCategory={setActiveCategory}
            setFilter={setFilter} 
            scrollToSection={scrollToSection}
            resultCards={resultCards}
            activeLocation={activeLocation}
            search={search}
            setSearch={setSearch}

            filter={filter}
            />
        <CategoryList 
            setActiveCategory={setActiveCategory} 
            setFilter={setFilter} 
            activeCategory={activeCategory}
            setActiveCard={setActiveCard} 
            activeCard={activeCard}
            scrollToSection={scrollToSection}
            resultCards={resultCards}
            />
        <ListCard 
            activeCategory={activeCategory} 
            activeLocation={activeLocation} 
            filter={filter} 
            setFilter={setFilter}
            setActiveCategory={setActiveCategory}
            setActiveLocation={setActiveLocation}
            setActiveCard={setActiveCard}
            resultCards={resultCards}
            scrollToSection={scrollToSection}
            header={header}
            setSearch={setSearch}
           />
    </div>
  )
}

export default Home