import React, { useEffect, useRef, useState } from "react";
import { getCiudades } from "../service/ciudades";
import styles from "../styles/locationPicker.module.css";
import Error from "./Error";
import { GoLocation } from "react-icons/go";
import { FaPlaneDeparture } from "react-icons/fa";

const LocationPicker = ({
    setActiveLocation,
    setActiveCategory,
    search,
    setSearch,
    setMsg,
    placeholder,
    styled,
    setCity,
    city,
    update,
}) => {
    const [display, setDisplay] = useState(false);
    const [options, setOptions] = useState([]);
    const wrapperRef = useRef(null);

    const fetchCiudades = async () => {
        try {
            const data = await getCiudades();
            setOptions(data.listCityResponse);
        } catch (err) {
            console.log(err);
        }
    };

    const setSearchInput = (i, id, all) => {
        if (setActiveCategory && setActiveLocation) {
            setActiveCategory(null);
            setActiveLocation(id);
        }
        if (setCity) {
            setCity({ ...city, value: all });
        }
        if (setMsg) {
            setMsg(false);
        }

        setSearch(i);
        setDisplay(false);
    };

    const onLocationChange = (e) => {
        setSearch(e.target.value);
    };

    const validate = () => {
        if (setCity) {
            if (
                Object.keys(city.value).length === 0 &&
                city.value.constructor === Object
            ) {
                setCity({ ...city, valid: "false" });
            } else {
                setCity({ ...city, valid: "true" });
            }
        }
    };

    useEffect(() => {
        validate();
    }, [city?.value]);

    useEffect(() => {
        fetchCiudades();
    }, []);

    useEffect(() => {
        window.addEventListener("mousedown", handleClickOutside);
        return () => {
            window.removeEventListener("mousedown", handleClickOutside);
        };
    });

    useEffect(() => {
        if (update && city !== undefined) {
            setSearch(city.value.name);
        }
    }, [update]);

    const handleClickOutside = (event) => {
        const { current: wrap } = wrapperRef;
        if (wrap && !wrap.contains(event.target)) {
            setDisplay(false);
        }
    };

    if (options === undefined) {
        return <Error />;
    }
    return (
        <div ref={wrapperRef}>
            <div
                className={styles.container}
                onClick={() => {
                    if (setMsg) {
                        setMsg(false);
                    }
                }}
                style={styled}
            >
                <FaPlaneDeparture />
                <input
                    placeholder={placeholder}
                    onClick={() => setDisplay(!display)}
                    value={search}
                    onChange={(e) => onLocationChange(e)}
                    className={styles.input}
                />
            </div>

            {display && (
                <div className={styles.menu}>
                    {options
                        .filter(
                            ({ name }) =>
                                name.indexOf(search.toUpperCase()) > -1
                        )
                        .map((v) => {
                            return (
                                <div
                                    key={v.idCity}
                                    onClick={() => {
                                        setSearchInput(v.name, v.idCity, v);
                                    }}
                                >
                                    <div className={styles.option}>
                                        <GoLocation />
                                        <span>
                                            {v.name}, {v.province}. {v.country}
                                        </span>
                                    </div>
                                    <hr id={styles.hr} />
                                </div>
                            );
                        })}
                </div>
            )}
        </div>
    );
};

export default LocationPicker;
