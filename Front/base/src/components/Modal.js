import React from "react";
import { RiCloseFill } from "react-icons/ri";

import {
    CloseButton,
    ModalContainer,
    ModalContent,
    ModalHeader,
    Overlay,
} from "../elements/Modals";

const Modal = ({
    children,
    state,
    setState,
    title = "¡Hola!",
    showCloseButton,
    showHeader,
}) => {
    return (
        <>
            {state && (
                <Overlay>
                    <ModalContainer>
                    {
                        showHeader && <ModalHeader>
                            <h3>{title}</h3>
                        </ModalHeader>
                    }
                        
                        {showCloseButton && (
                            <CloseButton>
                                <RiCloseFill onClick={() => setState(false)} />
                            </CloseButton>
                        )}

                        <ModalContent>{children}</ModalContent>
                    </ModalContainer>
                </Overlay>
            )}
        </>
    );
};

export default Modal;
