import React, { useState } from "react";
import styles from "../styles/searchbar.module.css";
import DatePicker from "./DatePicker";
import btn from "../styles/button.module.css";
import LocationPicker from "./LocationPicker";


const Searchbar = ({
    setActiveLocation,
    setFilter,
    setActiveCategory,
    scrollToSection,
    resultCards,
    activeLocation,
    search,
    setSearch,
}) => {
    const [msg, setMsg] = useState(false);

    const onSubmit = () => {
        if (activeLocation) {
            setFilter(true);
            setMsg(false);
            scrollToSection(resultCards);
        }
        if (!activeLocation) {
            setMsg(true);
        }
    };

    return (
        <div className={styles.container}>
            <h1> Busca ofertas en hoteles, casas y mucho más </h1>
            <div className={styles.wrap}>
                <LocationPicker
                    setActiveLocation={setActiveLocation}
                    setActiveCategory={setActiveCategory}
                    search={search}
                    setSearch={setSearch}
                    setMsg={setMsg}
                    placeholder="¿A dónde vamos?"
                    update={false}
                />
                <DatePicker setFilter={setFilter} />
                <div>
                    <button
                        className={btn.avg_btn}
                        onClick={() => onSubmit()}
                        id={styles.btn}
                    >
                        Buscar
                    </button>
                    {msg && (
                        <div className={styles.popup}>
                            <span className={styles.msg}>
                                Introduce un destino para empezar a buscar
                            </span>
                        </div>
                    )}
                </div>
            </div>
        </div>
    );
};

export default Searchbar;
