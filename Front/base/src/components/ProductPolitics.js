import React from "react";
import styles from "../styles/politics.module.css";
import { CiRuler, CiEdit, CiBullhorn } from "react-icons/ci";

const Politics = ({ cancellation, security, rules }) => {
    //console.log(cancellation, security, rules)
    return (
        <div className={styles.container}>
            <h2>Qué tenes que saber</h2>

            <div className={styles.politicas}>
                <div>
                    <h3>Normas</h3>
                    <ul>
                        {rules &&
                            rules.split(";").map((r, index) => (
                                <li
                                    key={index}
                                    style={{
                                        display: "flex",
                                        alignItems: "center",
                                        gap: "1vw",
                                    }}
                                >
                                    <CiRuler size="3vh" /> {r}
                                </li>
                            ))}
                    </ul>
                </div>

                <div>
                    <h3>Seguridad</h3>
                    <ul>
                        {security &&
                            security.split(";").map((r, index) => (
                                <li
                                    key={index}
                                    style={{
                                        display: "flex",
                                        alignItems: "center",
                                        gap: "1vw",
                                    }}
                                >
                                    <CiEdit size="3vh" /> {r}
                                </li>
                            ))}
                    </ul>
                </div>

                <div>
                    <h3>Cancelación</h3>
                    <ul>
                        {cancellation &&
                            cancellation.split(";").map((r, index) => (
                                <li
                                    key={index}
                                    style={{
                                        display: "flex",
                                        alignItems: "center",
                                        gap: "1vw",
                                    }}
                                >
                                    <CiBullhorn size="3vh" /> {r}
                                </li>
                            ))}
                    </ul>
                </div>
            </div>
        </div>
    );
};

export default Politics;
