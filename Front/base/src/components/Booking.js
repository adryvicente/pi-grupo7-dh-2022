import React, { useContext, useEffect, useState } from "react";
import ProductProvider from "../context/ProductProvider";
import styles from "../styles/booking.module.css";
import Back from "./Back";
import BookingArrival from "./BookingArrival";
import BookingCalendar from "./BookingCalendar";
import BookingDetail from "./BookingDetail";
import BookingForm from "./BookingForm";
import Politics from "./ProductPolitics";
import ScrollToTop from "./ScrollToTop";
import Error from "./Error";
import Modal from "./Modal";
import btn from "../styles/button.module.css";
import { useNavigate } from "react-router-dom";
import { postBooking } from "../service/booking";
import format from "date-fns/format";
import SessionProvider from "../context/SessionProvider";
import UserProvider from "../context/UserProvider";

const Booking = () => {
    const { getToken} = useContext(SessionProvider)
    const {user} = useContext(UserProvider)
    
    const { product, images, ciudades, availability, range } = useContext(ProductProvider);
    const [image, setImage] = useState();
    const [validForm, setValidForm] = useState(true);
    const navigate = useNavigate();
    const [successModal, setSuccessModal] = useState(false);
    const [dates, setDates] = useState([]);
    const [city, setCity] = useState({ value: "", valid: null });
    const [arrival, setArrival] = useState();

    useEffect(() => {
        try {
            setImage(images[0].imageUrl);
        } catch (e) {
            console.log(e);
        }
    }, [images]);

    

    const fetchBooking = async () => {
        const token = getToken()
        try {
            const response = await postBooking({
                hourIn: arrival,
                checkIn: format( range[0].startDate, "yyyy-MM-dd"),
                checkOut: format( range[0].endDate, "yyyy-MM-dd"),
                productId: product.idProduct,
                customerId: user.id || user.idUser
            }, token)
            if (response.idReserve){
                setValidForm(true)
                setSuccessModal(true)
            } else {
                setValidForm(false)
            }

        } catch (e) {
            console.log(e);
        }
    };

    if (product.length === 0) {
        return <Error />;
    }

    if (images.length === 0) {
        return <Error />;
    }

    if (ciudades.length === 0) {
        return <Error />;
    }
    if (validForm && successModal) {
        return (
            <Modal
                state={successModal}
                setState={setSuccessModal}
                showHeader={false}
                showCloseButton={false}
            >
                <svg
                    width="78"
                    height="74"
                    viewBox="0 0 78 74"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                >
                    <path
                        d="M78 36.9823L69.3491 27.1534L70.5545 14.1424L57.7555 11.2432L51.0545 2.67029e-05L39 5.162L26.9455 2.67029e-05L20.2445 11.2432L7.44545 14.1071L8.65091 27.118L0 36.9823L8.65091 46.8113L7.44545 59.8576L20.2445 62.7568L26.9455 74L39 68.8027L51.0545 73.9647L57.7555 62.7215L70.5545 59.8223L69.3491 46.8113L78 36.9823ZM31.9091 54.6603L17.7273 40.5179L22.7264 35.5328L31.9091 44.6546L55.2736 21.355L60.2727 26.3756L31.9091 54.6603Z"
                        fill="#F0572D"
                    />
                </svg>

                <h1> ¡Muchas gracias! </h1>
                <p>Su reserva se ha realizado con éxito</p>
                <button
                    className={btn.avg_btn}
                    id={styles.btn}
                    onClick={() => navigate("/")}
                >
                    Volver al home
                </button>
            </Modal>
        );
    }

    return (
        <>
            <ScrollToTop />
            <div className={styles.container}>
                <Back />
                <div className={styles.col1}>
                    <div className={styles.row1}>
                        <BookingForm city={city} setCity={setCity} />
                        <BookingCalendar setDates={setDates} availability={availability} />
                        <BookingArrival setArrival={setArrival} />
                    </div>
                    <BookingDetail
                        product={product}
                        image={image}
                        ciudades={ciudades}
                        dates={dates}
                        city={city}
                        arrival={arrival}
                        fetchBooking={fetchBooking}
                    />
                </div>
                <div style={{ background: "#fff" }}>
                    <Politics
                        cancellation={product.cancellationPolicies}
                        security={product.helathAndSecurity}
                        rules={product.rules}
                    />
                </div>
            </div>
        </>
    );
}


export default Booking;
