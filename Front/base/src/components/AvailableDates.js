import React, { useContext, useEffect, useState } from "react";
import { DateRange } from "react-date-range";
import btn from "../styles/button.module.css";
import styles from "../styles/availableDates.module.css";
import "react-date-range/dist/styles.css"; // main css file
import "react-date-range/dist/theme/default.css"; // theme css file
import "../styles/helper.css";
import { useLocation, useNavigate } from "react-router-dom";
import useWindowSize from "../elements/useWindowSize";
import SessionProvider from "../context/SessionProvider";
import Modal from "./Modal";
import { BiErrorAlt } from "react-icons/bi";
import ProductProvider from "../context/ProductProvider";

const AvailableDates = () => {
    const size = useWindowSize();
    const location = useLocation();
    const navigate = useNavigate();
    const today = new Date();
    const [logModal, setLogModal] = useState(false);
    const [errorModal, setErrorModal] = useState(false);

    const {
        range,
        setRange,
        isAvailable,
        availability,
        rangeError,
        setRangeError,
    } = useContext(ProductProvider);

    const { isLogged } = useContext(SessionProvider);

    const onSubmit = (e) => {
        e.preventDefault();

        if (!isLogged) {
            setLogModal(true);
        }
        if (isLogged) {
            if (rangeError.length > 0) {
                setErrorModal(true);
            } else {
                navigate(`${location.pathname}/booking`);
            }
        }
    };

    useEffect(() => {
        const fetchAvailability = async () => {
            try {
                const data = await isAvailable(range, availability);
                setRangeError(data);
            } catch (error) {}
        };
        fetchAvailability();
    }, [range, availability]);

    if (!isLogged && logModal) {
        return (
            <Modal
                state={logModal}
                setState={setLogModal}
                showHeader={true}
                showCloseButton={false}
                title="ERROR"
            >
                <BiErrorAlt size="5vh" />

                <h1> No se pudo procesar su reserva </h1>
                <p> Para realizar una reserva necesitas estar logueado </p>
                <button
                    className={btn.avg_btn}
                    id={styles.btn}
                    onClick={() => {
                        navigate("/login");
                        setLogModal(false);
                    }}
                >
                    Ingresar
                </button>
            </Modal>
        );
    }

    if (errorModal) {
        return (
            <Modal
                state={errorModal}
                setState={setErrorModal}
                showHeader={true}
                showCloseButton={true}
                title="ERROR"
            >
                <BiErrorAlt size="5vh" />

                <h1> No se puede iniciar su reserva </h1>
                <p> Lamentablemente, la fecha no se encuentra disponible </p>
            </Modal>
        );
    }

    return (
        <>
            <h2 className={styles.title}> Fechas disponibles </h2>
            <div className={styles.wrapper}>
                <DateRange
                    editableDateInputs={true}
                    onChange={(i) => {
                        setRange([i.selection]);
                    }}
                    moveRangeOnFirstSelection={false}
                    disabledDates={availability}
                    showMonthAndYearPickers={false}
                    months={size > 700 ? 2 : 1}
                    direction="horizontal"
                    className={styles.calendar}
                    ranges={range}
                    rangeColors={["#F0572D", "#F0572D", "#F0572D"]}
                    showDateDisplay={false}
                    minDate={today}
                    monthDisplayFormat="MMMM"
                />
                <div className={styles.booking}>
                    <span>
                        Agregá tus fechas de viaje para obtener precios exactos
                    </span>
                    <button
                        onClick={(e) => onSubmit(e)}
                        className={btn.avg_btn}
                        id={styles.btn}
                    >
                        Iniciar reserva
                    </button>
                </div>
            </div>
        </>
    );
};

export default AvailableDates;
