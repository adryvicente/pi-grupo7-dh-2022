import React from "react";
import { MapContainer, TileLayer, Marker, Popup } from "react-leaflet";
import styles from "../styles/productMap.module.css";

const ProductMap = ({ ciudades, address, position }) => {



    return (
        <div className={styles.container}>
            <h2> ¿Dónde vas a estar? </h2>
            <hr id={styles.hr} />

            {ciudades && (
                <h3>
                    {address} {ciudades.name}, {ciudades.province}. {ciudades.country}
                </h3>
            )}

            { position && (
                <MapContainer
                    center={position}
                    zoom={13}
                    style={{
                        width: "100%",
                        height: "500px",
                        borderRadius: "20px",
                    }}
                    scrollWheelZoom={true}
                >
                    <TileLayer
                        attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                    />
                    <Marker
                        position={position}
                    >
                        <Popup>{address}</Popup>
                    </Marker>
                </MapContainer>
            )}
        </div>
    );
};

export default ProductMap;
