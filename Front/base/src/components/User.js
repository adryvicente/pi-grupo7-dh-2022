import React, { useContext, useEffect, useState } from "react";
import UserProvider from "../context/UserProvider";
import styles from "../styles/User.module.css";
import Error from "./Error";

const User = () => {
    const {user} = useContext(UserProvider);
    const [avatar, setAvatar] = useState();

    useEffect(() => {
        const getinitials = () => {
            if (user) {
                try {
                    const result = user.name.toUpperCase().charAt(0) + user.lastname.toUpperCase().charAt(0);
                    setAvatar(result);
                } catch (e) {
                    console.log();
                }
            } else {
                setAvatar("");
            }
        };
        getinitials();
    }, [user, avatar]);

    if (user === undefined) {
        return <Error />;
    }
    return (
        <div className={styles.container}>
            <h1> {avatar} </h1>
        </div>
    );
};

export default User;
