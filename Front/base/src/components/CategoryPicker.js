import React, { useEffect, useRef, useState } from "react";
import { getCategories } from "../service/categorias";
import Error from "./Error";
import styles from "../styles/categoryPicker.module.css";
import {
    MdOutlineKeyboardArrowDown,
    MdOutlineKeyboardArrowUp,
} from "react-icons/md";

const CategoryPicker = ({ setCategory, category, update }) => {
    const [categories, setCategories] = useState();
    const [display, setDisplay] = useState(false);
    const [value, setValue] = useState("")
    const wrapperRef = useRef(null);

    const validate = () => {

            if (category.value !== undefined) {

                setCategory({ ...category, valid: "true" });
            } else {

                setCategory({ ...category, valid: "false" });
            }
        };

    useEffect(() => {
        const fetchCategories = async () => {
            try {
                const data = await getCategories();
                setCategories(data.listCategoryResponse);
            } catch (error) {
                console.log(error);
            }
        };
        fetchCategories();
    }, []);

    useEffect(() => {
        
        validate();
    }, [category.value]);

    useEffect(() => {
        window.addEventListener("mousedown", handleClickOutside);
        return () => {
            window.removeEventListener("mousedown", handleClickOutside);
        };
    });

    useEffect(() => {
        
       if (update && category.value.length !== 0 && categories) {
        setValue(category.value)
        setCategory({...category ,value: categories.filter((f) => f.title === category.value)[0].idCategory})
       }
    }, [update])

    const handleClickOutside = (event) => {
        const { current: wrap } = wrapperRef;
        if (wrap && !wrap.contains(event.target)) {
            setDisplay(false);
        }
    };

    if (categories === undefined) {
        return <Error />;
    }
    return (
        <div className={styles.selector} ref={wrapperRef}>
            <div id={styles.selectField} onClick={() => setDisplay(!display)}>
                <input
                    className={styles.input}
                    placeholder="Elige una categoría"
                    value={value}
                    onChange={(e) => {
                        setValue(e.target.value)
                        setCategory({ ...category, value: e.target.value });
                    }}
                />
                {display ? (
                    <MdOutlineKeyboardArrowUp size="20px" />
                ) : (
                    <MdOutlineKeyboardArrowDown size="20px" />
                )}
            </div>

            {display && (
                <ul id={styles.list}>
                    {categories.map((c) => {
                        return (
                            <div
                                key={c.idCategory}
                                onClick={() => {
                                    setDisplay(false);
                                    setCategory({
                                        ...category,
                                        value: c.idCategory,
                                    });
                                    setValue(c.title)
                                    
                                }}
                            >
                                <li className={styles.options}>
                                    <img src={c.urlImage} alt={c.title} />
                                    <span> {c.title} </span>
                                </li>
                                <hr className={styles.hr} />
                            </div>
                        );
                    })}
                </ul>
            )}
        </div>
    );
};

export default CategoryPicker;
