import React from "react";
import styles from "../styles/category.module.css";

const Category = ({
    id,
    title,
    img,
    results,
    setActiveCategory,
    setFilter,
    setActiveCard,
    activeCard,
    scrollToSection,
    resultCards,
}) => {
    return (
        <div
            className={styles.category_container}
            onClick={() => {
                setFilter(true);
                setActiveCategory(id);
                setActiveCard(id);
                scrollToSection(resultCards);
            }}
            id={activeCard === id ? `${styles.active}` : ""}
        >
            <picture>
                <img className={styles.img} src={`${img}`} alt={title} />
            </picture>
            <h3> {title} </h3>
            <h4>
                {results} {title}
            </h4>
        </div>
    );
};

export default Category;
