import React from 'react'
import { useNavigate } from 'react-router-dom';
import styles from '../styles/back.module.css'
import { FaChevronLeft } from "react-icons/fa";

const Back = () => {

    const navigate = useNavigate()

  return (
    <div className={styles.wrap} >
        <span>Volver</span>
        <FaChevronLeft onClick={() => navigate(-1)} className={styles.icon} />
      </div>
  )
}

export default Back