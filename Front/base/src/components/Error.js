import React from 'react';
import styles from '../styles/notFound.module.css';
import error from '../media/error.svg'
import ScrollToTop from './ScrollToTop'
import Back from './Back';

const NotFound = () => {

  return (
    <>
    <ScrollToTop />
    <div className={styles.container}>
      <Back />
      <h1> ¡Oh no! Error del sistema </h1>
      <div className={styles.img_wrap}>
        <img src={error} alt="Error" className={styles.img} />
      </div>
    </div>
    </>
  )
}

export default NotFound