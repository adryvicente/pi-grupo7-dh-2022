import React, { useContext, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import SessionProvider from "../context/SessionProvider";
import { getProductByAdmin } from "../service/productos";
import styles from "../styles/administration.module.css";
import btn from "../styles/button.module.css";
import ScrollToTop from "./ScrollToTop";
import Loading from "./Loading";

const Administration = () => {
    const navigate = useNavigate();
    const [products, setProducts] = useState([]);
    const [loading, setLoading] = useState();

    const { getToken } = useContext(SessionProvider);

    const token = getToken();
    useEffect(() => {
        const fetchProductos = async () => {
            try {
                const data = await getProductByAdmin(token);
                setProducts(data.listProductResponse);
                setLoading(false);
            } catch (err) {
                console.log(err);
            }
        };
        fetchProductos();
    }, [token]);

    return (
        <>
            <ScrollToTop />
            {loading ? (
                <Loading />
            ) : (
                <div className={styles.container}>
                    <div className={styles.admin}>
                        <h1>Administración</h1>
                        <button
                            className={btn.avg_btn}
                            onClick={() =>
                                navigate("/administration/createProduct")
                            }
                        >
                            Crear propiedad
                        </button>
                    </div>
                    <h2> Mis propiedades </h2>
                    {products.length === 0 ? (
                        <div></div>
                    ) : (
                        <div className={styles.product_list}>
                            {products.map((p, index) => (
                                <div key={index} className={styles.product}>
                                    <div className={styles.img_container}>
                                        <img
                                            src={p.images[0].imageUrl}
                                            alt={p.images[0].title}
                                        />
                                    </div>
                                    <div className={styles.detail}>
                                        <h3 id={`${styles}.${p.categoryTitle}`}>
                                            {" "}
                                            {p.categoryTitle}{" "}
                                        </h3>
                                        <h4> {p.name} </h4>

                                        <span> {p.address} </span>
                                        <span>
                                            {p.city.name}, {p.city.province}.{" "}
                                            {p.city.country}
                                        </span>
                                    </div>
                                    <hr />
                                    <button
                                        onClick={() =>
                                            navigate(
                                                `/administration/updateProduct/${p.idProduct}`
                                            )
                                        }
                                    >
                                        {" "}
                                        Editar{" "}
                                    </button>
                                </div>
                            ))}
                        </div>
                    )}
                </div>
            )}
        </>
    );
};

export default Administration;
