import styles from "../styles/addImage.module.css";
import { IoIosRemoveCircle, IoIosAddCircleOutline } from "react-icons/io";

const AddImage = ({
    valid_name,
    valid_url,
    images,
    setImages,
    setValidImages,
}) => {
    const handleChange = (e, index) => {
        const values = [...images];
        values[index][e.target.name] = e.target.value;
        setImages(values);
    };

    const handleAddField = (e) => {
        e.preventDefault();
        if (images.length < 5) {
            setImages([...images, { title: "", imageUrl: "" }]);
        }
    };

    const handleRemoveField = (e, index) => {
        e.preventDefault();
        const values = [...images];
        if (images.length > 1) {
            values.splice(index, 1);
        }
        setImages(values);
    };

    const validacion = () => {
        if (valid_name.test(images.title) && valid_url.test(images.imageUrl)) {
            setValidImages(true);
        } else {
            setValidImages(false);
        }
    };
    
    return (
        <div>
            {images.map((i, index) => (
                
                <div key={index} className={styles.container}>
                
                    <div className={styles.sub}>
                        <label>
                            <span className={styles.label}>Nombre</span>

                            <input
                                name="title"
                                type="text"
                                value={i.title}
                                onChange={(e) => handleChange(e, index)}
                                onKeyUp={validacion}
                                onBlur={validacion}
                                className={styles.input}
                            />
                        </label>
                        <label>
                            <span className={styles.label}>
                                URL de la imágen
                            </span>

                            <input
                                name="imageUrl"
                                type="url"
                                value={i.imageUrl}
                                onChange={(e) => handleChange(e, index)}
                                onKeyUp={validacion}
                                onBlur={validacion}
                                className={styles.input}
                            />
                        </label>
                    </div>
                    <div className={styles.buttons}>
                        {images.length > 1 && (
                            <button
                                className={styles.button}
                                onClick={(e, index) =>
                                    handleRemoveField(e, index)
                                }
                            >
                                <IoIosRemoveCircle color="#F0572D" size="3vw" />
                            </button>
                        )}
                        {images.length < 5 && (
                            <button
                                className={styles.button}
                                onClick={(e) => handleAddField(e)}
                            >
                                <IoIosAddCircleOutline
                                    color="#F0572D"
                                    size="3vw"
                                />
                            </button>
                        )}
                    </div>
                </div>
            ))}
        </div>
    );
};

export default AddImage;
