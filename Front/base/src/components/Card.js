import React from "react";
import { useNavigate } from "react-router-dom";
import styles from "../styles/card.module.css";
import Error from "./Error";

const Card = ({ img, category, name, location, description, id }) => {
    const navigate = useNavigate();
    const showDetail = () => {
        navigate(`product/${id}`);
    };

    if (img === undefined) {
        return <Error />;
    }

    return (
        <div className={styles.container}>
            <div className={styles.container__left}>
                <img
                    src={img.imageUrl}
                    className={styles.card__image}
                    alt={img.title}
                ></img>
            </div>
            <div className={styles.container__rigth}>
                <div className={styles.top}>
                    <h3 className={styles.category}>{category}</h3>
                    <h2>{name}</h2>
                    <h3> {location.name} </h3>
                </div>
                <div className={styles.bottom}>
                    <p>{description}</p>

                    <button onClick={showDetail}>Ver detalles</button>
                </div>
            </div>
        </div>
    );
};
export default Card;
