import React from 'react'
import facebook from '../media/icon facebook.svg';
import ig from '../media/icon ig.svg';
import linkedin from '../media/icon linkedin.svg';
import tweet from '../media/tweet.svg';
import styles from "../styles/footer.module.css"

const Footer = () => {
  return (
    <footer className={styles.footer_container}>
        <div className={styles.copyDiv}>
            <span>@2022 Digital Booking. Todos los derechos reservados</span>
        </div>
        <div>
        <div className={styles.social_media}>
                    <img className={styles.mobile_menu} src={facebook} alt="facebook logo" />
                    <img className={styles.mobile_menu} src={linkedin} alt="linkedin logo" />
                    <img className={styles.mobile_menu} src={tweet} alt="tweet logo" />
                    <img className={styles.mobile_menu} src={ig} alt="instagram logo" />
                </div>
        </div>
    </footer>
  )
}

export default Footer