import React, { useContext, useState } from "react";
import styles from "../styles/home.module.css";
import HeaderProducts from "./HeaderProducts";
import Features from "./ProductFeatures";
import Politics from "./ProductPolitics";
import { useEffect } from "react";
import Description from "./Description";
import ImageComponent from "./ImageComponent";
import AvailableDates from "./AvailableDates";
import { getProductById } from "../service/productos";
import { useParams } from "react-router-dom";
import ScrollToTop from "./ScrollToTop";
import Error from "./Error";
import ProductProvider from "../context/ProductProvider";
import Loading from "./Loading";
import { getBookingsByProduct } from "../service/booking";
import ProductMap from "./ProductMap";

const PageProducts = () => {
    let { id } = useParams();
    const [loading, setLoading] = useState(true);

    const { createProduct, product, images, features, ciudades, getDates, position } =
        useContext(ProductProvider);

    useEffect(() => {
        const fetchProductoById = async (id) => {
            try {
                const data = await getProductById(id);
                createProduct(data);
                setLoading(false);
                
            } catch (err) {
                console.log(err);
            }
        };
        fetchProductoById(id);
       
    }, [id]);

    const fetchProductByAvailability = async (id) => {
        try {
            const data = await getBookingsByProduct(id);
            const ranged = data.listReservesResponse;
            getDates(ranged);
            setLoading(false);
        } catch (error) {
            console.log("Error");
        }
    };


    useEffect(() => {
        fetchProductByAvailability(id);
        // eslint-disable-next-line
    }, [id]);

    if (product === undefined) {
        return <Error />;
    }

    return (
        <>
            <ScrollToTop />
            {loading ? (
                <Loading />
            ) : (
                <div className={styles.page_product}>
                    <HeaderProducts
                        ciudades={ciudades}
                        categoryTitle={product.categoryTitle}
                        name={product.name}
                        address={product.address}
                    />
                    <ImageComponent
                        images={images}
                        className={styles.gallery}
                        loading={loading}
                    />
                    <Description
                        description={product.description}
                        descriptionTitle={product.descriptionTitle}
                    />
                    <Features features={features} />
                    <AvailableDates  />
                    <ProductMap 
                        ciudades={ciudades}
                        address={product.address}
                        position={position}
                    />
                    <Politics 
                        cancellation={product.cancellationPolicies}
                        security={product.helathAndSecurity}
                        rules={product.rules}
                    />
                </div>
            )}
        </>
    );
};

export default PageProducts;
