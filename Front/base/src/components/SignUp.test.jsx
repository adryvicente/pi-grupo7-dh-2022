// import { render } from 'react-router-dom';
// import SignUp from '../src/components/SignUp.js';


// it('Salida de Nombre', () =>{
//     render(<SignUp/>)
//     expect("text").toBe('Nombre: Catalina Sua, Email: cata@correo.com');
// });

import React from 'react';
import { render, screen } from '@testing-library/react';
import SignUp from './SignUp.js';
import { BrowserRouter } from 'react-router-dom';


test('Comment component is rendered correctly', () => {
      render(<BrowserRouter>
               <SignUp/>
            </BrowserRouter>
         );

      // expect( 
      //        screen.getByText('Crear cuenta')
      //        ).toBeInTheDocument();
      
  const linkElement = screen.getByText("¿Ya tienes una cuenta?")
  expect(linkElement).toHaveTextContent("cuenta");
   });
