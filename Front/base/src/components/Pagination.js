import React from "react";

import styles from '../styles/pagination.module.css'

const Pagination = ({ cardsPerPage, totalCards, paginate, currentPage, scrollToSection, header }) => {
    const cardNumbers = [];
    for (let i = 1; i <= Math.ceil(totalCards / cardsPerPage); i++) {
        cardNumbers.push(i);
    }

    return (
        <div className={styles.container}>
            <ul className={styles.list}>

            {cardNumbers.map( (p) => (
                <li key={p} >
                <button onClick={() => {
                    scrollToSection(header)
                    paginate(p)
                    } } className={currentPage === p ? `${styles.active}` : `${styles.inactive}` } > {p} </button>
                </li>
            ) )}

            </ul>
        </div>
    );
};

export default Pagination;
