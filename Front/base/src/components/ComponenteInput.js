import React from "react";
import {
    Input,
    Label,
    GrupoInput,
    ErrorLabel,
    TextArea,
} from "../elements/Formularios";
import { FaCheckCircle, FaTimesCircle } from "react-icons/fa";

const ComponenteInput = ({
    state,
    setState,
    type,
    label,
    placeholder,
    name,
    error_msg,
    reg_ex,
    funcion,
    readOnly,
    required,
    styled,
    textarea,
    disabled
}) => {
    const onChange = (e) => {
        setState({ ...state, value: e.target.value });
        
    };

    const validacion = () => {
        if (reg_ex) {
            if (reg_ex.test(state.value)) {
                setState({ ...state, valid: "true" });
            } else {
                setState({ ...state, valid: "false" });
            }
        }
        if (funcion) {
            funcion();
        }
    };

    return (
        <div>
            <Label htmlFor={name} valid={state.valid}>
                {label}
            </Label>
            {textarea ? (
                <TextArea
                    type={type}
                    placeholder={placeholder}
                    id={name}
                    value={state.value}
                    onChange={readOnly ? null : onChange}
                    onKeyUp={readOnly ? null : validacion}
                    onBlur={readOnly ? null : validacion}
                    valid={state.valid}
                    readOnly={readOnly ? true : false}
                    required={required ? true : false}
                    style={styled}
                    
                />
            ) : (
                <Input
                    type={type}
                    placeholder={placeholder}
                    id={name}
                    value={state.value}
                    onChange={readOnly ? null : onChange}
                    onKeyUp={readOnly ? null : validacion}
                    onBlur={readOnly ? null : validacion}
                    valid={state.valid}
                    readOnly={readOnly ? true : false}
                    required={required ? true : false}
                    style={styled}
                    disabled={disabled}
                />
            )}

            <GrupoInput>
                {state.valid === "true" && <FaCheckCircle color="green" />}
                {state.valid === "false" && <FaTimesCircle color="red" />}
                <ErrorLabel valid={state.valid}>{error_msg}</ErrorLabel>
            </GrupoInput>
        </div>
    );
};

export default ComponenteInput;
