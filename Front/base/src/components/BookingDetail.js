import React, { useState } from "react";
import { AiFillStar } from "react-icons/ai";
import { IoLocationSharp } from "react-icons/io5";
import btn from "../styles/button.module.css";
import styles from "../styles/bookignDetail.module.css";
import Modal from "./Modal";
import { BiErrorAlt } from "react-icons/bi";

const BookingDetail = ({
    product,
    image,
    ciudades,
    dates,
    city,
    arrival,
    fetchBooking,
}) => {

    

    const [errorModal, setErrorModal] = useState(false);
    const onSubmit = (e) => {
        e.preventDefault();
        if (
            dates.length > 0 &&
            city.valid === "true" &&
            arrival !== undefined
        ) {
            try {
                fetchBooking()
                
            } catch (e) {console.log(e)}
            
        } else {
            setErrorModal(true);
        }
    };

    if (errorModal) {
        return (
            <Modal
                state={errorModal}
                setState={setErrorModal}
                showHeader={true}
                showCloseButton={true}
            >
                <BiErrorAlt size="5vh" />
                <h1> No se pudo procesar su reserva </h1>
                <p>
                    Debe completar todos los campos. Por favor, revise la
                    información ingresada
                </p>
            </Modal>
        );
    }

    return (
        <div className={styles.container}>
            <h2>Detalle de la reserva</h2>
            {image ? (
                <img className={styles.img_wrap} src={image} alt="portada" />
            ) : (
                <></>
            )}
            <div className={styles.details}>
                <span> {product.categoryTitle} </span>
                <h3> {product.name} </h3>
                <div>
                    <AiFillStar color="#F0572D" />
                    <AiFillStar color="#F0572D" />
                    <AiFillStar color="#F0572D" />
                    <AiFillStar color="#F0572D" />
                    <AiFillStar color="#F0572D" />
                </div>
            </div>

            <div className={styles.address}>
                <IoLocationSharp size="3vh" color="#F0572D" />
                <span>
                    {ciudades.name}, {ciudades.province}. {ciudades.country}. <br />
                    {product.address}
                </span>
            </div>
            <div>
                <hr />

                <div className={styles.dates}>
                    <span> Check in </span>

                    {dates.length === 0 ? (
                        <span> _/_/_ </span>
                    ) : (
                        <span> {dates[0]} </span>
                    )}
                </div>
                <hr />
                <div className={styles.dates}>
                    <span> Check out </span>
                    {dates.length === 0 ? (
                        <span> _/_/_ </span>
                    ) : (
                        <span> {dates[1]} </span>
                    )}
                </div>
                <hr />
            </div>
            <button
                className={btn.avg_btn}
                id={styles.btn}
                onClick={(e) => onSubmit(e)}
            >
                Confirmar reserva
            </button>
        </div>
    );
};

export default BookingDetail;
