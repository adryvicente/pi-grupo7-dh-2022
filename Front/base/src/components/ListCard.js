import React, { useContext, useEffect, useState } from "react";
import {
    getProductByCategory,
    getProductByCity,
    getProductByDates,
    getProductos,
} from "../service/productos";
import styles from "../styles/listCard.module.css";
import Card from "./Card";
import btn from "../styles/button.module.css";
import { FaRegEdit } from "react-icons/fa";
import Loading from "./Loading";
import Pagination from "./Pagination";
import ProductProvider from "../context/ProductProvider";

const ListCard = ({
    activeCategory,
    activeLocation,
    filter,
    setFilter,
    setActiveLocation,
    setActiveCategory,
    setActiveCard,
    resultCards,
    scrollToSection,
    header,
    setSearch,
}) => {
    const [productos, setProductos] = useState([]);
    const [title, setTitle] = useState("Recomendaciones");
    const [loading, setLoading] = useState(true);
    const [currentPage, setCurrentPage] = useState(1);
    const [cardsPerPage] = useState(6);
    const { searchDates } = useContext(ProductProvider);

    const fetchProductos = async () => {
        try {
            const data = await getProductos();
            setProductos(data.listProductResponse);
            setLoading(false);
        } catch (err) {
            console.log(err);
        }
    };

    const fetchProductosByCategory = async (id) => {
        try {
            const data = await getProductByCategory(id);
            setProductos(data.listProductResponse);
            setTitle(data.listProductResponse[0].categoryTitle);
            setLoading(false);
        } catch (err) {
            console.log(err);
        }
    };

    const fetchProductosByCity = async (id) => {
        try {
            const data = await getProductByCity(id);
            setProductos(data.listProductResponse);
            setTitle(data.listProductResponse[0].city.name);
            setLoading(false);
        } catch (err) {
            console.log(err);
        }
    };

    const fetchProductsByDates = async (city, checkin, checkout) => {
        try {
            const data = await getProductByDates(city, checkin, checkout);
            setProductos(data.listProductResponse);
            if (data.listProductResponse.length !== 0) {
                setTitle(data.listProductResponse[0].city.name);
            }
            setLoading(false);
        } catch (err) {
            console.log(err);
        }
    };

    const handleFilters = () => {
        setTitle("Recomendaciones");
        setActiveCategory(null);
        setActiveLocation(null);
        setActiveCard("");
        setFilter(false);
        setSearch("");
        scrollToSection(header);
    };

    useEffect(() => {
        if (filter === false) {
            fetchProductos();
        }
        if (filter === true && activeCategory !== null) {
            paginate(1);
            fetchProductosByCategory(activeCategory);
        }

        if (filter === true && activeLocation !== null) {
            paginate(1);

            if (
                searchDates.checkIn.length > 0 &&
                searchDates.checkOut.length > 0
            ) {
                fetchProductsByDates(
                    activeLocation,
                    searchDates.checkIn,
                    searchDates.checkOut
                );
            } else {
                fetchProductosByCity(activeLocation);
            }
        }

        // eslint-disable-next-line
    }, [filter, activeCategory, activeLocation]);

    // Get current posts
    const indexOfLastPost = currentPage * cardsPerPage;
    const indexOfFirstPost = indexOfLastPost - cardsPerPage;
    const currentPost = productos.slice(indexOfFirstPost, indexOfLastPost);

    // Change page
    const paginate = (p) => setCurrentPage(p);

    if (productos.length === 0) {
        return (
            <div className={styles.error}>
                <FaRegEdit />
                <span> ¡Próximamente! </span>
                {filter && (
                    <button
                        className={btn.avg_btn}
                        id={styles.btn}
                        onClick={handleFilters}
                    >
                        Limpiar filtros
                    </button>
                )}
            </div>
        );
    }

    return (
        <div
            className={styles.container}
            ref={resultCards ? resultCards : null}
        >
            <div className={styles.top}>
                <h2> {title && title} </h2>
                {filter && (
                    <button
                        className={btn.avg_btn}
                        id={styles.btn}
                        onClick={handleFilters}
                    >
                        Limpiar filtros
                    </button>
                )}
            </div>
            {loading ? (
                <Loading />
            ) : (
                <div className={styles.card}>
                    {currentPost.map((i) => (
                        <Card
                            key={i.idProduct}
                            id={i.idProduct}
                            img={i.images[0]}
                            category={i.categoryTitle}
                            title={i.categoryTitle}
                            location={i.city}
                            description={i.description}
                            name={i.name}
                        ></Card>
                    ))}
                </div>
            )}
            <div>
                <Pagination
                    cardsPerPage={cardsPerPage}
                    totalCards={productos.length}
                    paginate={paginate}
                    currentPage={currentPage}
                    scrollToSection={scrollToSection}
                    header={resultCards}
                />
            </div>
        </div>
    );
};
export default ListCard;
