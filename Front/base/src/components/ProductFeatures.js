import React from "react";
import styles from "../styles/features.module.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { library } from "@fortawesome/fontawesome-svg-core";
import { fas } from "@fortawesome/free-solid-svg-icons";

library.add(fas);

const Features = ({ features }) => {
    return (
        <div className={styles.container}>
            <h2>¿Qué ofrece este lugar?</h2>
            <hr />
            <div className={styles.features}>
                {features.map((f) => (
                    <div key={f.featureId} className={styles.icon}>
                        <span>
                            {f.name.charAt(0).toUpperCase() + f.name.slice(1).toLowerCase()}
                        </span>
                        <FontAwesomeIcon icon={f.icon} size="2x" />
                    </div>
                ))}
            </div>
            <div></div>
        </div>
    );
};

export default Features;
