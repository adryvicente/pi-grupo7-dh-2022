import React, { useContext, useEffect, useState } from "react";
import { Formulario } from "../elements/Formularios";
import ComponenteInput from "./ComponenteInput";
import styles from "../styles/bookingForm.module.css";
import { regular_expressions } from "../elements/regularExpressions";
import UserProvider from "../context/UserProvider";

const BookingForm = ({ city, setCity }) => {
    const reg_ex = regular_expressions;

    const {user} = useContext(UserProvider);

    const [name, setName] = useState({ value: "", valid: null });
    const [lastName, setLastName] = useState({ value: "", valid: null });
    const [email, setEmail] = useState({ value: "", valid: null });

    useEffect(() => {
        if (user) {
            setName({ value: user.name, valid: "true" });
            setLastName({ value: user.lastname, valid: "true" });
            setEmail({ value: user.email, valid: "true" });
        }
    }, [user]);

    return (
        <div className={styles.container}>
            <h2>Completá tus datos</h2>
            <Formulario className={styles.form}>
                <div className={styles.sub}>
                    <ComponenteInput
                        state={name}
                        setState={setName}
                        type="text"
                        label="Nombre"
                        placeholder="Ingrese su nombre"
                        name="name"
                        readOnly
                        
                    />
                    <ComponenteInput
                        state={lastName}
                        setState={setLastName}
                        type="text"
                        label="Apellido"
                        placeholder="Ingrese su apellido"
                        name="lastName"
                        readOnly
                    />
                </div>
                <div className={styles.sub}>
                    <ComponenteInput
                        state={email}
                        setState={setEmail}
                        type="email"
                        label="Correo electrónico"
                        placeholder="ex: hola@hola.com"
                        name="email"
                        readOnly
                    />
                    <ComponenteInput
                        state={city}
                        setState={setCity}
                        type="text"
                        label="Ciudad"
                        placeholder="Ejemplo: Rosario, Santa Fe"
                        name="name"
                        error_msg="Este campo es obligatorio"
                        reg_ex={reg_ex.texto}
                        required
                    />
                </div>
            </Formulario>
        </div>
    );
};

export default BookingForm;
