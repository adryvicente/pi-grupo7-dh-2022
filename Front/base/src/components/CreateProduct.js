import React, { useContext, useState } from "react";
import { Formulario } from "../elements/Formularios";
import ComponenteInput from "./ComponenteInput";
import LocationPicker from "./LocationPicker";
import btn from "../styles/button.module.css";
import styles from "../styles/createProduct.module.css";
import CategoryPicker from "./CategoryPicker";
import { regular_expressions } from "../elements/regularExpressions";
import Modal from "./Modal";
import { BiErrorAlt } from "react-icons/bi";
import { useNavigate } from "react-router-dom";
import { postProduct } from "../service/productos";
import SessionProvider from "../context/SessionProvider";
import FeaturePicker from "./FeaturePicker";
import AddImage from "./AddImage";

const CreateProduct = () => {
    const reg_ex = regular_expressions;
    const navigate = useNavigate();
    const { getToken } = useContext(SessionProvider);

    const [productName, setProductName] = useState({ value: "", valid: null });
    const [adress, setAdress] = useState({ value: "", valid: null });

    const [latitud, setLatitud] = useState({ value: "", valid: null });
    const [longitud, setLongitud] = useState({ value: "", valid: null });

    const [description, setDescription] = useState({ value: "", valid: null });
    const [descriptionTitle, setDescriptionTitle] = useState({
        value: "",
        valid: null,
    });
    const [feature, setFeature] = useState({ value: [], valid: null });
    const [normas, setNormas] = useState({ value: "", valid: null });
    const [seguridad, setSeguridad] = useState({ value: "", valid: null });
    const [cancelacion, setCancelacion] = useState({ value: "", valid: null });

    const [images, setImages] = useState([{ title: "", imageUrl: "" }]);
    const [validImages, setValidImages] = useState(false);

    const [category, setCategory] = useState({ value: undefined, valid: null });
    const [city, setCity] = useState({ value: {}, valid: null });

    const [search, setSearch] = useState("");

    const [validForm, setValidForm] = useState(false);
    const [errorModal, setErrorModal] = useState(false);
    const [successModal, setSuccessModal] = useState(false);

    const onSubmit = async (e) => {
        e.preventDefault();

        if (
            productName.valid === "true" &&
            adress.valid === "true" &&
            description.valid === "true" &&
            descriptionTitle.valid === "true" &&
            feature.valid === "true" &&
            normas.valid === "true" &&
            seguridad.valid === "true" &&
            cancelacion.valid === "true" &&
            validImages &&
            category.valid === "true" &&
            city.valid === "true"
        ) {
            try {
                fetchProduct();
                setValidForm(true);
                setSuccessModal(true);
                setErrorModal(false);
            } catch (error) {}
        } else {
            setValidForm(false);
            setSuccessModal(false);
            setErrorModal(true);
        }
    };

    const fetchProduct = async () => {
        try {
            const token = getToken();
            const response = await postProduct(
                {
                    name: productName.value,
                    descriptionTitle: descriptionTitle.value,
                    description: description.value,
                    address: adress.value,
                    categoryId: category.value,
                    cityId: city.value.idCity,
                    featuresId: feature.value,
                    images: images,
                    rules: normas.value,
                    healthAndSecurity: seguridad.value,
                    cancellationPolicies: cancelacion.value,
                    latitude: Number(latitud.value),
                    longitude: Number(longitud.value),
                },
                token
            );

            if (response.idProduct) {
                setValidForm(true);
                setSuccessModal(true);
            } else {
                setValidForm(false);
            }
        } catch (e) {
            console.log(e);
        }
    };

    // ERROR MODAL
    if (!validForm && errorModal) {
        return (
            <Modal
                state={errorModal}
                setState={setErrorModal}
                showHeader={true}
                showCloseButton={true}
                title="ERROR"
            >
                <BiErrorAlt size="5vh" />

                <h1> No se puede procesar la creación de su propiedad</h1>
                <p> Por favor, rellene los campos obligatorios </p>
            </Modal>
        );
    }

    // SUCCESS MODAL 
    if (validForm && successModal) {
        return (
            <Modal
                state={successModal}
                setState={setSuccessModal}
                showHeader={false}
                showCloseButton={false}
            >
                <svg
                    width="78"
                    height="74"
                    viewBox="0 0 78 74"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                >
                    <path
                        d="M78 36.9823L69.3491 27.1534L70.5545 14.1424L57.7555 11.2432L51.0545 2.67029e-05L39 5.162L26.9455 2.67029e-05L20.2445 11.2432L7.44545 14.1071L8.65091 27.118L0 36.9823L8.65091 46.8113L7.44545 59.8576L20.2445 62.7568L26.9455 74L39 68.8027L51.0545 73.9647L57.7555 62.7215L70.5545 59.8223L69.3491 46.8113L78 36.9823ZM31.9091 54.6603L17.7273 40.5179L22.7264 35.5328L31.9091 44.6546L55.2736 21.355L60.2727 26.3756L31.9091 54.6603Z"
                        fill="#F0572D"
                    />
                </svg>

                <h1> ¡Muchas gracias! </h1>
                <p>Su propiedad se ha cargado con éxito</p>
                <button
                    className={btn.avg_btn}
                    id={styles.btn}
                    onClick={() => navigate("/")}
                >
                    Volver al home
                </button>
            </Modal>
        );
    }

    return (
        <div className={styles.container}>
            <h1>Crear propiedad</h1>
            <Formulario className={styles.form} onSubmit={onSubmit}>
                {/* Product Detail */}
                <div className={styles.detail}>
                    <div className={styles.sub}>
                        <ComponenteInput
                            state={productName}
                            setState={setProductName}
                            type="text"
                            label="Nombre de la propiedad"
                            placeholder="Ingrese el nombre de la propiedad"
                            name="name"
                            reg_ex={reg_ex.name}
                        />
                        <div>
                            <label id={styles.label}>Categoría</label>
                            <CategoryPicker
                                setCategory={setCategory}
                                category={category}
                                reg_ex={reg_ex.name}
                            />
                        </div>
                    </div>
                    <div className={styles.sub}>
                        <ComponenteInput
                            state={adress}
                            setState={setAdress}
                            type="text"
                            label="Dirección"
                            placeholder="Ingrese la dirección de la propiedad"
                            name="adress"
                            reg_ex={reg_ex.notEmpty}
                        />
                        <div>
                            <label id={styles.label}>Ciudad</label>
                            <LocationPicker
                                search={search}
                                setSearch={setSearch}
                                placeholder="Ciudad"
                                styled={{ width: "100%" }}
                                reg_ex={reg_ex.notEmpty}
                                setCity={setCity}
                                city={city}
                            />
                        </div>
                    </div>
                    <div className={styles.sub}>
                        <ComponenteInput
                            state={latitud}
                            setState={setLatitud}
                            type="number"
                            label="Latitud"
                            placeholder="Ejemplo: -31.16933937969396"
                            name="latitud"
                            reg_ex={reg_ex.notEmpty}
                        />
                        <ComponenteInput
                            state={longitud}
                            setState={setLongitud}
                            type="number"
                            label="Longitud"
                            placeholder="Ejemplo: -63.60680916041048"
                            name="longitud"
                            reg_ex={reg_ex.notEmpty}
                        />
                    </div>
                    <div>
                        <ComponenteInput
                            state={descriptionTitle}
                            setState={setDescriptionTitle}
                            type="text"
                            label="Título de la descripción"
                            placeholder="Escriba aquí"
                            name="adress"
                            reg_ex={reg_ex.notEmpty}
                        />
                        <ComponenteInput
                            state={description}
                            setState={setDescription}
                            type="text"
                            label="Descripción"
                            placeholder="Escriba aquí"
                            name="description"
                            styled={{ height: "150px" }}
                            reg_ex={reg_ex.notEmpty}
                            textarea
                        />
                    </div>
                </div>
                {/* Product Features */}
                <div className={styles.detail}>
                    <h2>Agregar atributos</h2>
                    <FeaturePicker
                        setFeature={setFeature}
                        feature={feature}
                        update={false}
                    />
                </div>
                {/* Product politics */}
                <div className={styles.detail}>
                    <h2>Políticas del producto</h2>
                    <div className={styles.sub} id={styles.politics}>
                        <div className={styles.politics}>
                            <h3>Normas de la casa</h3>
                            <ComponenteInput
                                state={normas}
                                setState={setNormas}
                                type="text"
                                label="Descripción"
                                placeholder="Ejemplo: No se permite fumar en el establecimiento;No se admiten mascotas"
                                name="normas"
                                styled={{ height: "150px" }}
                                textarea
                                reg_ex={reg_ex.notEmpty}
                            />
                        </div>
                        <div>
                            <h3>Salud y seguridad</h3>
                            <ComponenteInput
                                state={seguridad}
                                setState={setSeguridad}
                                type="text"
                                label="Descripción"
                                placeholder="Ejemplo: La unidad cuenta con vigilancia las 24 horas;La unidad cuenta con matafuegos"
                                name="seguridad"
                                styled={{ height: "150px" }}
                                textarea
                                reg_ex={reg_ex.notEmpty}
                            />
                        </div>
                        <div>
                            <h3>Política de cancelación</h3>
                            <ComponenteInput
                                state={cancelacion}
                                setState={setCancelacion}
                                type="text"
                                label="Descripción"
                                placeholder="Escriba aquí"
                                name="cancelacion"
                                styled={{ height: "150px" }}
                                textarea
                                reg_ex={reg_ex.notEmpty}
                            />
                        </div>
                    </div>
                </div>
                {/* Product Images */}
                <div className={styles.detail}>
                    <h2>Cargar imágenes</h2>
                    <div className={styles.image}>
                        <AddImage
                            valid_name={reg_ex.name}
                            valid_url={reg_ex.notEmpty}
                            images={images}
                            setImages={setImages}
                            validImages={validImages}
                            setValidImages={setValidImages}
                        />
                    </div>
                </div>
                {/* BUTTON CREATE */}
                <div className={styles.detail}>
                    <button
                        className={btn.avg_btn}
                        id={styles.create}
                        type="submit"
                        disabled={validImages ? false : true}
                    >
                        Crear
                    </button>
                </div>
            </Formulario>
        </div>
    );
};

export default CreateProduct;
