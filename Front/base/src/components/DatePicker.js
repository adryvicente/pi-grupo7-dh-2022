import React, { useContext } from "react";
import { useEffect, useRef, useState } from "react";

import "react-date-range/dist/styles.css"; // main css file
import "react-date-range/dist/theme/default.css"; // theme css file
import styles from "../styles/datePicker.module.css";

import { DateRange } from "react-date-range";
import format from "date-fns/format";
import useWindowSize from "../elements/useWindowSize";
import { FaCalendarCheck } from "react-icons/fa";

import "../styles/calendar.css";
import ProductProvider from "../context/ProductProvider";

const DatePicker = ({ setFilter }) => {
    const today = new Date();
    const [open, setOpen] = useState(false);
    const size = useWindowSize();

    const { range, setRange, postDates } = useContext(ProductProvider);

    const hideOnEscape = (e) => {
        if (e.key === "Escape") {
            setOpen(false);
        }
    };

    const refOne = useRef(null);

    const hideOnClickOutside = (e) => {
        if (refOne.current && !refOne.current.contains(e.target)) {
            setOpen(false);
        }
    };

    useEffect(() => {
        document.addEventListener("keydown", hideOnEscape, true);
        document.addEventListener("click", hideOnClickOutside, true);
    }, []);

    return (
        <div className={styles.container}>
            <div className={styles.inputContainer}>
                <FaCalendarCheck color="black" />
                <input
                    value={
                        range[0].startDate.toLocaleDateString() ===
                        range[0].endDate.toLocaleDateString()
                            ? " Elige una fecha"
                            : ` ${format(
                                  range[0].startDate,
                                  "dd/MM/yyyy"
                              )} to ${format(range[0].endDate, "dd/MM/yyyy")} `
                    }
                    readOnly
                    onClick={() => setOpen(!open)}
                    className={styles.input}
                />
            </div>

            {open && (
                <div ref={refOne} className="location">
                    <DateRange
                        showMonthAndYearPickers={false}
                        onChange={(i) => {
                            setRange([i.selection]);
                            postDates([i.selection]);
                        }}
                        editableDateInputs={true}
                        moveRangeOnFirstSelection={false}
                        ranges={range}
                        months={size > 1055 ? 2 : 1}
                        direction="horizontal"
                        rangeColors={["#F0572D", "#F0572D", "#F0572D"]}
                        showDateDisplay={false}
                        minDate={today}
                        monthDisplayFormat="MMMM"
                    />
                </div>
            )}
        </div>
    );
};

export default DatePicker;
