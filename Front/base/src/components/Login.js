import styles from "../styles/login.module.css";
import btn from "../styles/button.module.css";
import React, { useContext, useState } from "react";
import { NavLink, Navigate } from "react-router-dom";
import ComponenteInput from "./ComponenteInput";
import { Formulario, BlockSubmit } from "../elements/Formularios";

import { FaExclamationTriangle } from "react-icons/fa";
import SessionProvider from "../context/SessionProvider";
import { regular_expressions } from "../elements/regularExpressions";
import AuthProvider from "../context/AuthProvider";

const Login = () => {
    const [email, setEmail] = useState({ value: "", valid: null });
    const [password, setPassword] = useState({ value: "", valid: null });
    const [validForm, setValidForm] = useState(null);

    const { handleSession } = useContext(SessionProvider);

    const reg_ex = regular_expressions;
    const { postAuthUser } = useContext(AuthProvider);

    const onSubmit = async (e) => {
        e.preventDefault();
        const response = await postAuthUser({
            email: email.value,
            password: password.value,
        });
        if (response.token) {
            let auth = response.token
            window.sessionStorage.setItem("session", auth)
            setValidForm(true);
            
        } else {
            setValidForm(false);
        }
        handleSession();
    };

    if (validForm) {
        return <Navigate to="/" />;
    }

    return (
        <div className={styles.login_container}>
            <div>
                <h1>Iniciar sesión</h1>
            </div>
            <div className={styles.formulario}>
                <Formulario action="" onSubmit={onSubmit}>
                    <ComponenteInput
                        state={email}
                        setState={setEmail}
                        type="email"
                        label="Correo electrónico"
                        placeholder="ex: hola@hola.com"
                        name="user_email"
                        error_msg="Ingrese un correo electrónico válido"
                        reg_ex={reg_ex.mail}
                    />
                    <ComponenteInput
                        state={password}
                        setState={setPassword}
                        type="password"
                        label="Contraseña"
                        placeholder="* * * * * * *"
                        name="user_password"
                        error_msg="Este campo es obligatorio"
                        reg_ex={reg_ex.password}
                    />
                    {validForm === false && (
                        <BlockSubmit>
                            <p>
                                <FaExclamationTriangle />
                                <b>Error:</b> No se pudo enviar el formulario,
                                por favor revise la información ingresada.
                            </p>
                        </BlockSubmit>
                    )}
                    <div className={styles.buttons}>
                        <button
                            type="submit"
                            className={btn.avg_btn}
                            id={styles.redirect_btn}
                        >
                            Ingresar
                        </button>
                        <span>
                            ¿Aún no tienes cuenta?
                            <NavLink to={"/signup"} className={styles.redirect}>
                                Registrate
                            </NavLink>
                        </span>
                    </div>
                </Formulario>
            </div>
        </div>
    );
};

export default Login;
