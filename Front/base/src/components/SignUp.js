import styles from "../styles/signup.module.css";
import btn from "../styles/button.module.css";
import React, { useContext, useState } from "react";
import { Navigate, NavLink } from "react-router-dom";
import ComponenteInput from "./ComponenteInput";
import { Formulario, BlockSubmit } from "../elements/Formularios";
import { FaExclamationTriangle } from "react-icons/fa";
import SessionProvider from "../context/SessionProvider";
import UserProvider from "../context/UserProvider";
import { regular_expressions } from "../elements/regularExpressions";
import Modal from "./Modal";
import { BiErrorAlt } from "react-icons/bi";

const SignUp = () => {
    const [email, setEmail] = useState({ value: "", valid: null });
    const [name, setName] = useState({ value: "", valid: null });
    const [lastName, setLastName] = useState({ value: "", valid: null });
    const [password, setPassword] = useState({ value: "", valid: null });
    const [repPassword, setRepPassword] = useState({ value: "", valid: null });
    const [userRol, setUserRol] = useState({ value: "", valid: null });
    const [validForm, setValidForm] = useState(null);
    const [modal, setModal] = useState(false);

    const reg_ex = regular_expressions;

    const { handleSession } = useContext(SessionProvider);
    const { fetchUser, setUser } = useContext(UserProvider);

    const onSubmit = async (e) => {
        e.preventDefault();
        if (
            name.valid === "true" &&
            lastName.valid === "true" &&
            email.valid === "true" &&
            password.valid === "true" &&
            repPassword.valid === "true" &&
            userRol.valid === true
        ) {
            const response = await fetchUser({
                email: email.value,
                lastname: lastName.value,
                name: name.value,
                password: password.value,
                cityId: 1,
                role: userRol.value,
            });

            if (response.name) {
                const auth = response.token;
                setUser(response);
                setValidForm(true);
                window.sessionStorage.setItem("session", auth);
                handleSession();
            } else {
                setModal(true);
            }
        } else {
            setValidForm(false);
        }
    };

    const validatePassword = () => {
        if (password.value.length > 0) {
            if (password.value !== repPassword.value) {
                setRepPassword((prevState) => {
                    return { ...prevState, valid: "false" };
                });
            } else {
                setRepPassword((prevState) => {
                    return { ...prevState, valid: "true" };
                });
            }
        }
    };

    if (!modal && validForm) {
        return <Navigate to="/" />;
    }
    if (modal && !validForm) {
        return (
            <Modal
                state={modal}
                setState={setModal}
                showHeader={true}
                showCloseButton={true}
                title="ERROR"
            >
                <BiErrorAlt size="5vh" />
                <h1> No se pudo crear su cuenta </h1>
                <p> El correo electrónico ingresado se encuentra en uso</p>
            </Modal>
        );
    }

    return (
        <div className={styles.container}>
            <div>
                <h1>Crear cuenta</h1>
            </div>
            <div className={styles.signup_container}>
                <Formulario action="" onSubmit={onSubmit}>
                    <ComponenteInput
                        state={name}
                        setState={setName}
                        type="text"
                        label="Nombre"
                        placeholder="Ingrese su nombre"
                        name="name"
                        error_msg="Este campo es obligatorio"
                        reg_ex={reg_ex.name}
                    />
                    <ComponenteInput
                        state={lastName}
                        setState={setLastName}
                        type="text"
                        label="Apellido"
                        placeholder="Ingrese su apellido"
                        name="lastName"
                        error_msg="Este campo es obligatorio"
                        reg_ex={reg_ex.name}
                    />
                    <ComponenteInput
                        state={email}
                        setState={setEmail}
                        type="email"
                        label="Correo electrónico"
                        placeholder="ex: hola@hola.com"
                        name="email"
                        error_msg="Ingrese un correo electrónico válido"
                        reg_ex={reg_ex.mail}
                    />
                    <ComponenteInput
                        state={password}
                        setState={setPassword}
                        type="password"
                        label="Contraseña"
                        placeholder="Ingrese una contraseña"
                        name="password"
                        error_msg="Este campo es obligatorio"
                        reg_ex={reg_ex.password}
                    />
                    <ComponenteInput
                        state={repPassword}
                        setState={setRepPassword}
                        type="password"
                        label="Repita la contraseña"
                        placeholder="Confirmar contraseña"
                        name="password2"
                        error_msg="Las contraseñas no coinciden"
                        funcion={validatePassword}
                    />
                    <div>
                        <div>
                            <label className={styles.label}>
                                <input
                                    name="userRol"
                                    type="radio"
                                    value="USER"
                                    onChange={(e) => {
                                        setUserRol({
                                            valid: e.target.checked,
                                            value: e.target.value,
                                        });
                                    }}
                                />
                                <span>Quiero reservar estadías</span>
                            </label>
                        </div>
                        <div>
                            <label className={styles.label}>
                                <input
                                    name="userRol"
                                    type="radio"
                                    value="ADMIN"
                                    onChange={(e) => {
                                        setUserRol({
                                            valid: e.target.checked,
                                            value: e.target.value,
                                        });
                                    }}
                                />
                                <span>Quiero registrar propiedades</span>
                            </label>
                        </div>
                    </div>
                    {validForm === false && (
                        <BlockSubmit>
                            <p>
                                <FaExclamationTriangle />
                                <b>Error:</b> No se pudo enviar el formulario,
                                por favor revise la información ingresada.
                            </p>
                        </BlockSubmit>
                    )}
                    <div className={styles.buttons}>
                        <button
                            className={btn.avg_btn}
                            onSubmit={onSubmit}
                            id={styles.redirect_btn}
                        >
                            Crear cuenta
                        </button>
                        <span>
                            {" "}
                            ¿Ya tienes una cuenta?{" "}
                            <NavLink to={"/login"} className={styles.redirect}>
                                {" "}
                                Iniciar sesión
                            </NavLink>{" "}
                        </span>
                    </div>
                </Formulario>
            </div>
        </div>
    );
};

export default SignUp;
