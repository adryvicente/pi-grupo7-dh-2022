import React from 'react'
import styles from "../styles/description.module.css"

const Description = ({descriptionTitle, description}) => {
  
  return (

    <div className={styles.conteiner_description}>
      <h2>{descriptionTitle}</h2>
      <p>{description}</p>

    </div>

  
  )
}
export default Description;
