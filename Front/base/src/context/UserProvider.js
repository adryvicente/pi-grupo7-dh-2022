import { createContext, useState } from "react";
import { baseUrl } from "../service/categorias";

const Context = createContext();

export const UserProvider = ({ children }) => {
    const [user, setUser] = useState({});
    

    const fetchUser = async (data) => {
        let url = `${baseUrl}auth/register`;
        try {
            const response = await fetch(url, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                },
                body: JSON.stringify(data),
            });
            if (!response.ok) {
                return response
            }
            const result = await response.json();
            return result;
        } catch (error) {
            console.log("catch error: ", error);
        }
    };

    return (
        <Context.Provider
            value={{
                user,
                fetchUser,
                setUser
            }}
        >
            {children}
        </Context.Provider>
    );
};

export default Context;
