import { createContext, useState } from "react";
import { baseUrl } from "../service/categorias";

const Context = createContext();

export const AuthProvider = ({ children }) => {
    const [authenticatedUser, setAuthenticatedUser] = useState()
    const [admin, setAdmin] = useState(false)

    const postAuthUser = async (data) => {
        let url = `${baseUrl}auth/login`;
        try {
            const response = await fetch(url, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                },
                body: JSON.stringify(data),
            });
            if (!response.ok) {
                throw new Error("Oucrrió un error en la petición");
            }
            const result = await response.json();
            
            return result;
        } catch (error) {
            console.log("catch error: ", error);
        }
    };

    const getAuthUser = async (token) => {
        let url = `${baseUrl}auth/me`;
        try {
            const response = await fetch(url, {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": token
                },
                
            });
            if (!response.ok) {
                throw new Error("Oucrrió un error en la petición");
            }
            const result = await response.json();            
            return result;
        } catch (error) {
            console.log("catch error: ", error);
        }
    }

    return (
        <Context.Provider
            value={{
                postAuthUser,
                getAuthUser,
                setAuthenticatedUser,
                authenticatedUser,
                setAdmin,
                admin
            }}
        >
            {children}
        </Context.Provider>
    );
};
export default Context;
