import { createContext, useState } from "react";
import format from "date-fns/format";

const Context = createContext();
export const ProductProvider = ({ children }) => {
    const [product, setProduct] = useState({});
    const [images, setImages] = useState([]);
    const [features, setFeatures] = useState([]);
    const [ciudades, setCiudades] = useState([]);
    const [position, setPosition] = useState();

    const [availability, setAvailability] = useState([]);
    const [range, setRange] = useState([
        {
            startDate: new Date(),
            endDate: new Date(),
            key: "selection",
        },
    ]);
    const [rangeError, setRangeError] = useState([]);
    const [searchDates, setSearchDates] = useState({
        checkIn: "",
        checkOut: "",
    });

    const createProduct = (data) => {
        if (data !== undefined) {
            setProduct(data);
            setImages(data.images.slice(0, 5));
            setFeatures(data.features);

            setCiudades(data.city);
            setPosition([data.latitude, data.longitude]);
        }
    };

    function dateRange(startDate, endDate, steps = 1) {
        const dateArray = [];
        let currentDate = new Date(startDate);

        while (currentDate <= new Date(endDate)) {
            dateArray.push(new Date(currentDate));
            currentDate.setUTCDate(currentDate.getUTCDate() + steps);
        }

        return dateArray;
    }

    function obtainRanges(arr) {
        const dates = [];
        arr.map((d) => dates.push(dateRange(d[0], d[1])));

        return dates;
    }

    function disabledArray(arr) {
        const disabled = [];
        arr.map((col) => col.map((d) => disabled.push(d)));

        return disabled;
    }

    const getDates = async (arr) => {
        try {
            let raw_ranges = [];

            await arr.map((d) =>
                raw_ranges.push([
                    new Date(d.checkIn.replace(/-/g, "/")),
                    new Date(d.checkOut.replace(/-/g, "/")),
                ])
            );

            const list = obtainRanges(raw_ranges);
            const disabled = disabledArray(list);
            setAvailability(disabled);
        } catch (error) {
            console.log(error);
        }
    };

    async function isAvailable(range, availability) {
        const result = [];
        const ranged = dateRange(range[0].startDate, range[0].endDate);
        try {
            availability.map((booked) => {
                ranged.map((date) => {
                    if (
                        booked.toLocaleDateString() ===
                        date.toLocaleDateString()
                    ) {
                        result.push(date);
                    }
                });
                return result;
            });
            return result;
        } catch (error) {
            console.log(error);
        }
    }

    const postDates = (dates) => {
        if (dates !== undefined) {
            setSearchDates({
                checkIn: format(dates[0].startDate, "yyyy-MM-dd"),
                checkOut: format(dates[0].endDate, "yyyy-MM-dd"),
            });
        }
    };

    return (
        <Context.Provider
            value={{
                createProduct,
                product,
                images,
                features,
                ciudades,
                getDates,
                availability,
                range,
                setRange,
                isAvailable,
                rangeError,
                setRangeError,
                position,
                postDates,
                searchDates,
            }}
        >
            {children}
        </Context.Provider>
    );
};

export default Context;
