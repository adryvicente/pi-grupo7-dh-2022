import { UserProvider } from "./UserProvider";
import { SessionProvider } from "./SessionProvider";
import { ProductProvider } from "./ProductProvider";
import { AuthProvider } from "./AuthProvider";

export const Contexts = ({ children }) => {
    return (
        <AuthProvider>
            <UserProvider>
                <SessionProvider>
                    <ProductProvider>{children}</ProductProvider>
                </SessionProvider>
            </UserProvider>
        </AuthProvider>
    );
};
