import {createContext, useState} from 'react';

const Context = createContext()

export const SessionProvider = ({children}) => {

    const [isLogged, setIsLogged] = useState(false)

    const handleSession = () => {
        const data = getToken()
        if (data) {
            setIsLogged(true)
        }
    }

    const getToken = () => {
        return window.sessionStorage.getItem("session")
    }

    const refreshPage = ()=>{
        window.location.reload(false);
     }

    const endSession = () => {
        window.sessionStorage.removeItem("session")
        setIsLogged(false)
        refreshPage()
    }

    return(
        <Context.Provider value={{
            isLogged,
            handleSession,
            endSession,
            getToken,
            refreshPage
        }} >
            {children}
        </Context.Provider>
    )

}

export default Context;