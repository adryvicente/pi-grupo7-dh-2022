import "./App.css";
import Home from "./components/Home";
import Login from "./components/Login";
import SignUp from "./components/SignUp";
import PageProducts from "./components/PageProducts";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Header from "./components/Header";
import Footer from "./components/Footer";
import NotFound from "./components/NotFound";
import { useContext, useEffect, useRef } from "react";
import Booking from "./components/Booking";
import AuthProvider from "./context/AuthProvider";
import UserProvider from "./context/UserProvider";
import SessionProvider from "./context/SessionProvider";
import CreateProduct from "./components/CreateProduct";
import Administration from "./components/Administration";
import UpdateProduct from "./components/UpdateProduct";
import PrivateRoutes from "./components/PrivateRoutes";
import MyBookings from "./components/MyBookings";

function App() {
    const header = useRef(null);
    const { getAuthUser, setAdmin, admin } = useContext(AuthProvider);
    const { setUser } = useContext(UserProvider);
    const { getToken, handleSession, isLogged } = useContext(SessionProvider);

    const scrollToSection = (ref) => {
        window.scrollTo({
            top: ref.current.offsetTop,
            behavior: "smooth",
        });
    };

    useEffect(() => {
        const token = getToken();
        const fetchUser = async (token) => {
            try {
                const data = await getAuthUser(token);

                setUser(data);
                if (data.role === "ROLE_ADMIN") {
                    setAdmin(true);
                }
                if (data.role === "ROLE_USER") {
                    setAdmin(false);
                }
                handleSession();
            } catch (err) {
                console.log(err);
            }
        };
        if (token !== null) {
            fetchUser(token);
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [getAuthUser, getToken, handleSession, setUser]);

    return (
        <main>
            <BrowserRouter>
                <Header header={header} />
                <Routes>
                    <Route
                        path="/"
                        element={
                            <Home
                                scrollToSection={scrollToSection}
                                header={header}
                            />
                        }
                    />
                    <Route path="/login" element={<Login />} />
                    <Route path="/signup" element={<SignUp />} />
                    <Route path="/product/:id" element={<PageProducts />} />
                    <Route element={<PrivateRoutes isLogged={isLogged} />}>
                        <Route
                            path="/product/:id/booking"
                            element={<Booking />}
                        />
                        <Route
                            path="/my-bookings"
                            element={<MyBookings />}
                        />
                    </Route>
                    <Route element={<PrivateRoutes isLogged={admin} />}>
                        <Route
                            path="/administration"
                            element={<Administration />}
                        />
                        <Route
                            path="/administration/createProduct"
                            element={<CreateProduct />}
                        />
                        <Route
                            path="/administration/updateProduct/:id"
                            element={<UpdateProduct />}
                        />
                    </Route>

                    <Route path="*" element={<NotFound />} />
                </Routes>
                <Footer />
            </BrowserRouter>
        </main>
    );
}

export default App;
