-- INSERTS PARA CATEGORIAS

INSERT INTO digital_booking.categories
(description, title, url_image)
VALUES('Hoteles', 'Hoteles', 'https://0521ptc3n1-grupo7-imagenes.s3.us-east-2.amazonaws.com/images/categoria/01_hoteles.jpeg');

INSERT INTO digital_booking.categories
(description, title, url_image)
VALUES('Hostels', 'Hostels', 'https://0521ptc3n1-grupo7-imagenes.s3.us-east-2.amazonaws.com/images/categoria/02_Hostels.webp');

INSERT INTO digital_booking.categories
(description, title, url_image)
VALUES('Departamentos', 'Departamentos', 'https://0521ptc3n1-grupo7-imagenes.s3.us-east-2.amazonaws.com/images/categoria/03_departamentos.jpeg');

INSERT INTO digital_booking.categories
(description, title, url_image)
VALUES('Bed and breakfast', 'Bed and breakfast', 'https://0521ptc3n1-grupo7-imagenes.s3.us-east-2.amazonaws.com/images/categoria/04_B_B.webp');

-- INSERT PARA CIUDADES

INSERT INTO digital_booking.cities
(country, name, province)
VALUES('Argentina', 'BUENOS AIRES', 'Ciudad de Buenos Aires');

INSERT INTO digital_booking.cities
(country, name, province)
VALUES('Argentina', 'CORDOBA', 'Córdoba');

INSERT INTO digital_booking.cities
(country, name, province)
VALUES('Argentina', 'ROSARIO', 'Santa Fe');

INSERT INTO digital_booking.cities
(country, name, province)
VALUES('Argentina', 'MAR DEL PLATA', 'Buenos Aires');

INSERT INTO digital_booking.cities
(country, name, province)
VALUES('Argentina', 'SAN MIGUEL DE TUCUMAN', 'Tucumán');

INSERT INTO digital_booking.cities
(country, name, province)
VALUES('Argentina', 'SALTA', 'Salta');

INSERT INTO digital_booking.cities
(country, name, province)
VALUES('Argentina', 'SANTA FE', 'Santa Fe');

INSERT INTO digital_booking.cities
(country, name, province)
VALUES('Argentina', 'CORRIENTES', 'Corrientes');

INSERT INTO digital_booking.cities
(country, name, province)
VALUES('Argentina', 'BAHIA BLANCA', 'Buenos Aires');

INSERT INTO digital_booking.cities
(country, name, province)
VALUES('Argentina', 'RESISTENCIA', 'Chaco');

INSERT INTO digital_booking.cities
(country, name, province)
VALUES('Argentina', 'POSADAS', 'Misiones');

INSERT INTO digital_booking.cities
(country, name, province)
VALUES('Argentina', 'QUILMES', 'Buenos Aires');

INSERT INTO digital_booking.cities
(country, name, province)
VALUES('Argentina', 'MERLO', 'Buenos Aires');

INSERT INTO digital_booking.cities
(country, name, province)
VALUES('Argentina', 'SAN SALVADOR DE JUJUY', 'Jujuy');

INSERT INTO digital_booking.cities
(country, name, province)
VALUES('Argentina', 'GUAYMALLEN', 'Mendoza');

INSERT INTO digital_booking.cities
(country, name, province)
VALUES('Argentina', 'SANTIAGO DEL ESTERO', 'Santiago del Estero');

INSERT INTO digital_booking.cities
(country, name, province)
VALUES('Argentina', 'GREGORIO DE LAFERRERE', 'Buenos Aires');

INSERT INTO digital_booking.cities
(country, name, province)
VALUES('Argentina', 'JOSE C. PAZ', 'Buenos Aires');

INSERT INTO digital_booking.cities
(country, name, province)
VALUES('Argentina', 'PARANA', 'Entre Ríos');

INSERT INTO digital_booking.cities
(country, name, province)
VALUES('Argentina', 'BANFIELD', 'Buenos Aires');

INSERT INTO digital_booking.cities
(country, name, province)
VALUES('Argentina', 'GONZALEZ CATALAN', 'Buenos Aires');

INSERT INTO digital_booking.cities
(country, name, province)
VALUES('Argentina', 'NEUQUEN', 'Neuquén');

INSERT INTO digital_booking.cities
(country, name, province)
VALUES('Argentina', 'FORMOSA', 'Formosa');

INSERT INTO digital_booking.cities
(country, name, province)
VALUES('Argentina', 'LANUS', 'Buenos Aires');

INSERT INTO digital_booking.cities
(country, name, province)
VALUES('Argentina', 'LA PLATA', 'Buenos Aires');

INSERT INTO digital_booking.cities
(country, name, province)
VALUES('Argentina', 'GODOY CRUZ', 'Mendoza');

INSERT INTO digital_booking.cities
(country, name, province)
VALUES('Argentina', 'ISIDRO CASANOZA', 'Buenos Aires');

INSERT INTO digital_booking.cities
(country, name, province)
VALUES('Argentina', 'LAS HERAS', 'Mendoza');

INSERT INTO digital_booking.cities
(country, name, province)
VALUES('Argentina', 'BERAZATEGUI', 'Buenos Aires');

INSERT INTO digital_booking.cities
(country, name, province)
VALUES('Argentina', 'LA RIOJA', 'La Rioja');

INSERT INTO digital_booking.cities
(country, name, province)
VALUES('Argentina', 'COMODORO RIVADAVIA', 'Chubut');

INSERT INTO digital_booking.cities
(country, name, province)
VALUES('Argentina', 'MORENO', 'Buenos Aires');

INSERT INTO digital_booking.cities
(country, name, province)
VALUES('Argentina', 'SAN LUIS', 'San Luis');

INSERT INTO digital_booking.cities
(country, name, province)
VALUES('Argentina', 'SAN MIGUEL', 'Buenos Aires');

INSERT INTO digital_booking.cities
(country, name, province)
VALUES('Argentina', 'SAN FERNANDO DEL VALLE DE CATAMARCA', 'Catamarca');

INSERT INTO digital_booking.cities
(country, name, province)
VALUES('Argentina', 'RIO CUARTO', 'Córdoba');

INSERT INTO digital_booking.cities
(country, name, province)
VALUES('Argentina', 'VIRREY DEL PINO', 'Buenos Aires');

INSERT INTO digital_booking.cities
(country, name, province)
VALUES('Argentina', 'SAN FERNANDO DEL VALLE DE CATAMARCA', 'Catamarca');

INSERT INTO digital_booking.cities
(country, name, province)
VALUES('Argentina', 'SAN CARLOS DE BARILOCHE', 'Río Negro');

-- INSERT PARA FEATURES

INSERT INTO digital_booking.features
(icon, name)
VALUES('fa-solid fa-kitchen-set', 'COCINA');

INSERT INTO digital_booking.features
(icon, name)
VALUES('fa-solid fa-tv', 'TELEVISOR');

INSERT INTO digital_booking.features
(icon, name)
VALUES('fa-solid fa-temperature-arrow-down', 'AIRE ACONDICIONADO');

INSERT INTO digital_booking.features
(icon, name)
VALUES('fa-solid fa-paw', 'APTO MASCOTAS');

INSERT INTO digital_booking.features
(icon, name)
VALUES('fa-solid fa-car', 'ESTACIONAMIENTO GRATUITO');

INSERT INTO digital_booking.features
(icon, name)
VALUES('fa-solid fa-person-swimming', 'PILETA');

INSERT INTO digital_booking.features
(icon, name)
VALUES('fa-solid fa-wifi', 'WI-FI');

INSERT INTO digital_booking.features
(icon, name)
VALUES('fa-solid fa-leaf', 'JARDIN');

INSERT INTO digital_booking.features
(icon, name)
VALUES('fa-solid fa-hand-sparkles', 'SERVICIO DE LIMPIEZA DIARIO');

INSERT INTO digital_booking.features
(icon, name)
VALUES('fa-solid fa-camera', 'CAMARAS DE SEGURIDAD');

INSERT INTO digital_booking.features
(icon, name)
VALUES('fa-solid fa-bottle-droplet', 'SHAMPOO Y ACONDICIONADOR');

INSERT INTO digital_booking.features
(icon, name)
VALUES('fa-solid fa-temperature-high', 'AGUA CALIENTE');

INSERT INTO digital_booking.features
(icon, name)
VALUES('fa-solid fa-mattress-pillow', 'ALMOHADAS Y MANTAS ADICIONALES');

INSERT INTO digital_booking.features
(icon, name)
VALUES('fa-solid fa-book', 'LIBROS Y MATERIAL DE LECTURA');

INSERT INTO digital_booking.features
(icon, name)
VALUES('fa-solid fa-baby-carriage', 'ELEMENTOS PARA CUIDADO DE BEBES');

INSERT INTO digital_booking.features
(icon, name)
VALUES('fa-solid fa-suitcase-medical', 'BOTIQUÍN DE PRIMEROS AUXILIOS');

INSERT INTO digital_booking.features
(icon, name)
VALUES('fa-solid fa-mug-hot', 'CAFETERA');

INSERT INTO digital_booking.features
(icon, name)
VALUES('fa-solid fa-wine-glass-empty', 'COPAS DE VINO');

INSERT INTO digital_booking.features
(icon, name)
VALUES('fa-solid fa-door-open', 'ENTRADA INDEPENDIENTE');

INSERT INTO digital_booking.features
(icon, name)
VALUES('fa-solid fa-smoking', 'APTO FUMADORES');

INSERT INTO digital_booking.features
(icon, name)
VALUES('fa-solid fa-ban-smoking', 'PROHIBIDO FUMAR');

INSERT INTO digital_booking.features
(icon, name)
VALUES('fa-solid fa-bath', 'HIDROMASAJE');

INSERT INTO digital_booking.features
(icon, name)
VALUES('fa-solid fa-bacon', 'DESAYUNO INCLUIDO');

-- INSERT PARA LOS ROLES

INSERT INTO digital_booking.roles
(name)
VALUES('ROLE_ADMIN');

INSERT INTO digital_booking.roles
(name)
VALUES('ROLE_USER');

-- INSERT PARA USUARIOS

INSERT INTO digital_booking.users
(email, lastname, name, password, city_id, role_id)
VALUES('email@admin.com', 'Turismo', 'City', '$2a$10$7cNZqtxPwoECp2umjyN5ZeysqqgpCQdHg.G6E3aRVvHe4Goaly7s.', 1, 1);

INSERT INTO digital_booking.users
(email, lastname, name, password, city_id, role_id)
VALUES('email@admin2.com', 'TRI', 'TRABAJO', '$2a$10$7cNZqtxPwoECp2umjyN5ZeysqqgpCQdHg.G6E3aRVvHe4Goaly7s.', 1, 1);

INSERT INTO digital_booking.users
(email, lastname, name, password, city_id, role_id)
VALUES('email@admin3.com', 'VIAJA', 'VIVI', '$2a$10$7cNZqtxPwoECp2umjyN5ZeysqqgpCQdHg.G6E3aRVvHe4Goaly7s.', 1, 1);

INSERT INTO digital_booking.users
(email, lastname, name, password, city_id, role_id)
VALUES('email@user.com', 'usertest', 'test', '$2a$10$7cNZqtxPwoECp2umjyN5ZeysqqgpCQdHg.G6E3aRVvHe4Goaly7s.', 1, 2);

INSERT INTO digital_booking.users
(email, lastname, name, password, city_id, role_id)
VALUES('email@user1.com', 'Bruno', 'Rodriguez', '$2a$10$7cNZqtxPwoECp2umjyN5ZeysqqgpCQdHg.G6E3aRVvHe4Goaly7s.', 1, 2);

-- INSERT PARA PRODUCTOS


INSERT INTO digital_booking.products
(address, cancellation_policies, description, 
title_description, health_security, latitude, 
longitude, name, rules, category_id, city_id, user_id)
VALUES(
	'San Martín 2309',
	'La cancelación no es reembolsable',
	'El HOTEL REAL está bien situado en el centro de Buenos Aires, a 800 metros del Obelisco de Buenos Aires, a menos de 1 km del teatro Colón y a 13 minutos a pie de la basílica del Santísimo Sacramento. El alojamiento cuenta con recepción 24 horas, servicio de conserjería y wifi gratis en todas las instalaciones. El hotel dispone de habitaciones familiares.',
	'Alojate en el corazón de Buenos Aires',
	'No hay un detector de monóxido de carbono;No hay un detector de humo;No apto para bebés (menores de 2 años)',
	-34.60261203987142,
	-58.3987730580946,
	'HOTEL REAL', 
	'No se permite fumar;No se admiten mascotas;No se permiten fiestas ni eventos',
	1, 
	1,
	1);

INSERT INTO digital_booking.products
(address, cancellation_policies, description, 
title_description, health_security, latitude, 
longitude, name, rules, category_id, city_id, user_id)
VALUES(
	'Manuel Belgrano 1816',
	'La cancelación no es reembolsable',
	'El Hotel Iris Mendoza está muy bien situado en la autopista MRN7 y a poca distancia del centro de Mendoza. Ofrece habitaciones con aire acondicionado y baño privado. Cada mañana se sirve un desayuno bufé y hay conexión Wi-Fi gratuita, en las zonas comunes. El establecimiento también cuenta con aparcamiento gratuito.', 
	'Descubrí Mendoza',
	'Detector de humo;No apto para niños o bebés',
	-33.04922234846566,
	-68.77211284306125,
	'Montañas Azules Apart Hotel Iris Mendoza', 
	'No se permite fumar;No se admiten mascotas;No se permiten fiestas ni eventos',
	1, 
	15,
	1);

INSERT INTO digital_booking.products
(address, cancellation_policies, description, 
title_description, health_security, latitude, 
longitude, name, rules, category_id, city_id, user_id)
VALUES(
	'Maria Remedios del Valle 1847',
	'La cancelación no es reembolsable',
	'LOFT CARLOS está situado en Salta, a menos de 1 km de El Tren a las Nubes, a 16 minutos a pie del centro comercial El Palacio Galerías y a 1,4 km del estadio El Gigante del Norte. Este apartamento se encuentra a 2,9 km del teleférico Salta - San Bernardo ya 6,1 km del centro de convenciones de Salta. Este apartamento dispone de aire acondicionado, 1 dormitorio, TV de pantalla plana y cocina.', 
	'Salta, La Linda',
	'No hay un detector de monóxido de carbono;No hay un detector de humo',
	-24.783397833545987,
	-65.4885854967031,
	'Hotel FULANO',
	'No se permite fumar;No se admiten mascotas;No se permiten fiestas ni eventos',
	1, 
	6,
	2);

INSERT INTO digital_booking.products
(address, cancellation_policies, description, 
title_description, health_security, latitude, 
longitude, name, rules, category_id, city_id, user_id)
VALUES(
	'Juana Azurduy 1862',
	'La cancelación no es reembolsable',
	'El Súper alojamiento en Rosario tiene balcón y se encuentra en Rosario, a solo 1,3 km del parque Independencia y a 400 metros del Museo Municipal de Bellas Artes. El establecimiento se encuentra a 2,2 km de la Ruta Modernista ya 2,7 km del centro de convenciones Patio de la Madera. Este apartamento con aire acondicionado está equipado con 1 dormitorio, TV de pantalla plana y cocina. Los puntos de interés populares cerca del apartamento incluyen Children s Garden, Pasaje Monroe y Newells Old Boys Stadium.', 
	'Viví Rosario, Viví Bien', 
	'No se indicó si hay detector de monóxido de carbono;No se indicó si hay detector de humo;No apto para bebés (menores de 2 años)',
	-32.95783557425344,
	-60.665963444446064,
	'SúperDuper Alojamiento en Rosario', 
	'No es posible ingresar al alojamiento con personas no incluidas en la reserva;No utilizar el inmueble para actividades comerciales;El departamento debe ser devuelto en el mismo estado en que se recibió, exceptuando lo afectado por el buen uso',
	3, 
	3,
	2);

INSERT INTO digital_booking.products
(address, cancellation_policies, description, 
title_description, health_security, latitude, 
longitude, name, rules, category_id, city_id, user_id)
VALUES(
	'Martín Miguel de Güemes 1821',
	'La cancelación no es reembolsable',
	'El Departamento NULA-RAMA, situado en Mar del Plata, a 7,3 km del Casino Central de Mar del Plata, ofrece alojamiento con zona de parrilla, wifi gratis, servicio de conserjería y mostrador de información turística. Este apartamento, ubicado en un edificio que data de 1983, se encuentra a 8 km del Torreón del Monje ya 12 km del puerto de Mar del Plata. El apartamento tiene 2 dormitorios, un baño, ropa de cama, toallas, TV de pantalla plana por cable, cocina totalmente equipada y terraza con vistas al jardín.', 
	'¡La Costa, sólo para vos!', 
	'La propiedad no ofrece shampoo ni jabón ni elementos de higiene personal;Sí se ofrecen elementos y productos de limpieza para cocina y baño, papel higiénico, y ropa blanca (sábanas y toallas);No hay estacionamiento en la propiedad',
	-38.018171359364956,
	-57.52767958953819,
	'Departamento NULA-RAMA',
	'El huésped deberá reponer o abonar lo faltante o averiado.El departamento esta ubicado en un edificio habitado por familias, por lo que se solicita se respeten los horarios de descanso dentro del mismo, entre las 14 y las 16 hs. y a partir de las 22 hs. hasta las 8 hs.-, especialmente días laborales, cuidando el volumen del televisor, música, etc.-',
	3, 
	4,
	2);

INSERT INTO digital_booking.products
(address, cancellation_policies, description, 
title_description, health_security, latitude, 
longitude, name, rules, category_id, city_id, user_id)
VALUES(
	'Av. Chacabuco 1400',
	'La cancelación no es reembolsable',
	'Afluentes se encuentra en Santiago del Estero, a 4 km de Chumillo, a 5,7 km de Maco ya 9,1 km de Santiago del Estero. Se encuentra a 3,7 km de Santiago del Estero y ofrece wifi gratis y recepción 24 horas. Este apartamento tiene aire acondicionado, 1 dormitorio, TV de pantalla plana por cable y cocina con nevera. El establecimiento proporciona toallas y ropa de cama por un suplemento. En los alrededores se puede pescar. El apartamento ofrece un servicio de alquiler de coches. Rubia Moreno y La Banda están a 10 km de La Río Grande. El aeropuerto más cercano es el aeropuerto de Mal Paso, a 9 km. El servicio de enlace con el aeropuerto está disponible por un suplemento.', 
	'Anímate.',
	'No hay un detector de monóxido de carbono; No hay un detector de humo',
	-27.806113829262024,
	-64.25486086307554,
	'Afluentes',
	'Apto para fumadores;Se admiten mascotas;No se permiten fiestas ni eventos',
	3, 
	16, 
	1);

INSERT INTO digital_booking.products
(address, cancellation_policies, description, 
title_description, health_security, latitude, 
longitude, name, rules, category_id, city_id, user_id)
VALUES(
	'M. Adela Caría 1234',
	'La cancelación no es reembolsable',
	'La Reina Uva ofrece alojamiento con balcón y wifi gratis en San Salvador de Jujuy, a 300 metros de la plazoleta de Santa Rita, a 1,6 km de la plaza de los Reyes Magos y a 1,5 km de la plaza Francisco Argañaras. El establecimiento, situado a 200 metros de la plaza de los Arquitectos, cuenta con jardín y aparcamiento privado gratuito. Esta casa de 2 dormitorios y 1 baño cuenta con TV de pantalla plana. El alojamiento está equipado con una cocina. La casa de vacaciones ofrece una terraza. Los puntos de interés populares cerca de La Reina Uva incluyen el Derrivador Plaza Francisco Argañaras, Talud Carrillo y la Plaza de Los Tucanes. El aeropuerto más cercano es el aeropuerto internacional Gobernador Horacio Guzmán, a 39 km del alojamiento.', 
	'Viajá sin dramas.', 
	'No se indicó si hay detector de monóxido de carbono;No se indicó si hay detector de humo',
	-24.1859490566879,
	-65.28981405444563,
	'La Reina Uva',
	'Check-in sin restricción de horario con caja de seguridad para llaves;No se permite fumar;No se admiten mascotas;No se permiten fiestas ni eventos',
	2, 
	14,
	1);

INSERT INTO digital_booking.products
(address, cancellation_policies, description, 
title_description, health_security, latitude, 
longitude, name, rules, category_id, city_id, user_id)
VALUES(
	'Elena D. Martínez Fontes 5678',
	'La cancelación no es reembolsable',
	'El lugar cuenta con aire acondicionado, 1 dormitorio, TV de pantalla plana y cocina.', 
	'Ideal para disfrutar!',
	'No hay un detector de monóxido de carbono; No hay un detector de humo',
	-32.90300805822585,
	-68.86858328530943,
	'Cheyarote', 
	'No se puede hacer ruidos despúes de las 22.00;Si llevas mascotas dejarlas en el patio y no dentro del departamento ya que podrían ocasionar destrozos; Esta prohibido fumar en interiores',
	2, 
	26,
	2);

INSERT INTO digital_booking.products
(address, cancellation_policies, description, 
title_description, health_security, latitude, 
longitude, name, rules, category_id, city_id, user_id)
VALUES(
	'Irene M. Bernasconi 1928',
	'La cancelación no es reembolsable',
	'Este bed and breakfast dispone de balcón, vistas a la ciudad, zona de estar, TV por cable, zona de cocina totalmente equipada con heladera y horno y baño compartido con bidet y secador de pelo. Hay fogones, hervidor de agua y cafetera. El Hostel Co cuenta con terraza. El aeropuerto internacional Resistencia es el más cercano y está a 7 km del alojamiento. A las parejas les gusta la ubicación. Le pusieron un puntaje de 9,3 para un viaje de a dos.', 
	'Viajá!', 
	'La unidad cuenta con detector de monóxido de carbono; No se indicó si hay detector de humo',
	-27.430646020007657,
	-58.98865097855436,
	'B&B CoOL',
	'Mantener limpios los lugares comunes en relación a los animales;No se permite la estadia de mas personas de las que se hayan reservado.',
	4, 
	10,
	3);

INSERT INTO digital_booking.products
(address, cancellation_policies, description, 
title_description, health_security, latitude, 
longitude, name, rules, category_id, city_id, user_id)
VALUES(
	'Carmen Pujals 3847',
	'La cancelación no es reembolsable',
	'B&B CBA está ubicado en un lugar céntrico, junto al Museo de Bellas Artes de Córdoba y a 5 minutos a pie de la Mezquita. Ofrece WiFi gratuita, y la mayoría de las habitaciones están distribuidas alrededor de patios típicos andaluces con jardín.', 
	'No busqués más, ¡es ahora!',
	'La unidad cuenta con detector de monóxido de carbono; No se indicó si hay detector de humo',
	-31.427342123190112,
	-64.18249668270012,
	'B&B CBA', 
	'Hora sin ruido de 0:00hs a 09:00hs; Prohíbido fumar en interiores',
	4, 
	2,
	3);

INSERT INTO digital_booking.products
(address, cancellation_policies, description, 
title_description, health_security, latitude, 
longitude, name, rules, category_id, city_id, user_id)
VALUES(
	'Andrés Guacurarí 383',
	'La cancelación no es reembolsable',
	'Todos los alojamientos disponen de aire acondicionado, baño privado, TV de pantalla plana, cocina totalmente equipada y balcón. El departamento está a 6 km de Encarnación y a 47 km de Coronel José F. Bogado. El aeropuerto Libertador General José de San Martín es el más cercano y está a 8 km del Apart Boutique1.', 
	'Descubrí Misiones', 
	'No hay un detector de monóxido de carbono; No hay un detector de humo',
	-27.35766267261267,
	-55.90692103182664,
	'Apart Boutique1', 
	'No se permiten fiestas ni eventos; Ordenar antes de retirarse',
	3, 
	11,
	3);


INSERT INTO digital_booking.products
(address, cancellation_policies, description, 
title_description, health_security, latitude, 
longitude, name, rules, category_id, city_id, user_id)
VALUES(
	'1 de Mayo 89',
	'La cancelación no es reembolsable',
	'El SMA FLATS ofrece alojamiento con patio a menos de 1 km de la catedral de María Auxiliadora de Almagro. Hay estacionamiento privado y wifi gratis en todas las instalaciones. Los alojamientos disponen de aire acondicionado, cocina totalmente equipada con zona de comedor, TV de pantalla plana y baño privado con bidet. Hay heladera, horno, fogones y hervidor de agua. El mirador Balcón del Valle se encuentra a 2,7 km del aparthotel, mientras que el río Limay está a 5 km. El aeropuerto más cercano es el aeropuerto internacional Presidente Perón, ubicado a 8 km del SMA FLATS.', 
	'Conectá con la naturaleza', 
	'Detector de humo;No apto para niños o bebés',
	-38.97256033468642,
	-68.05741356835732,
	'SMART FLATS', 
	'Check-in sin restricción de horario con caja de seguridad para llaves;No se permite fumar',
	4, 
	22,
	3);

INSERT INTO digital_booking.products
(address, cancellation_policies, description, title_description, health_security, latitude, longitude, name, rules, category_id, city_id, user_id)
VALUES(
	'12 de Octubre 150',
	'Cancelación gratuita durante 48 horas previas al check-in',
	'ESPECTACULAR departamento 5 Estrellas en el CENTRO de BARILOCHE en el Recién Estrenado Edificio de Categoria Bariloche Design. Decorado con el mejor gusto y calidad para un publico exigente. Studio con Vista al Hermoso Lago Nahuel Huapi, se encuentra a solo 250 metros del Centro Cívico, en frente al Casino de Bariloche. Cuenta con Cama Matrimonial de Standart Hotelero , Tv, CABLE, WIFI, cocina Americana Totalmente Equipada, Comoda mesa de comedor, y baño completo.',
	'Conocé el Sur',
	'No hay un detector de monóxido de carbono;No hay un detector de humo',
	-41.13281325455122,
	-71.30878831043248,
	'Studio de 5 Estrellas Centro de Bariloche',
	'3 huéspedes como máximo;No se permite fumar;No se permiten fiestas ni eventos',
	3,
	39,
	2);

INSERT INTO digital_booking.products
(address, cancellation_policies, description, title_description, health_security, latitude, longitude, name, rules, category_id, city_id, user_id)
VALUES(
	'Anasagasti 134',
	'Cancelación gratuita durante 48 horas previas al check-in',
	'Departamento luminoso, cálido y moderno de 42 mt2, completamente equipado para cuatro personas. Ubicado en el centro de la ciudad y a su vez en un área segura y tranquila, para poder disfrutar de una fantástica estadía. A pocas cuadras del centro cívico y del polo gastronómico y cervecero de Bariloche. Próximo a las principales líneas de colectivos que conducen a todas las atracciones de la región.',
	'Explorar Bariloche!',
	'No hace falta un detector de monóxido de carbono (todos los electrodomésticos son eléctricos y no funcionan con gas, gasoil, querosén, o madera);No hay un detector de humo',
	-41.14009434040333, 
	-71.3089779013003,
	'Departamento en complejo residencial',
	'4 huéspedes como máximo;Horario en que se debe guardar silencio: 23:00 - 08:00;No se permiten fiestas ni eventos',
	3,
	39,
	1);

INSERT INTO digital_booking.products
(address, cancellation_policies, description, title_description, health_security, latitude, longitude, name, rules, category_id, city_id, user_id)
VALUES(
	'Almte. Brown 1140',
	'Cancelación gratuita durante 48 horas previas al check-in',
	'Hotel cómodo para descansar, a pocas cuadras del centro de Bariloche y con negocios cercanos para realizar compras',
	'Un lujo',
	'No se indicó si hay detector de monóxido de carbono;No se indicó si hay detector de humo',
	-41.148595593708066, 
	-71.29632863200848,
	'FRESQUITO RESORT',
	'No se permiten fiestas ni eventos',
	1,
	39,
	2);


-- INSER PARA FEATURE_PRODUCT
INSERT INTO digital_booking.feature_product
(feature_id, product_id)
VALUES(1, 1);

INSERT INTO digital_booking.feature_product
(feature_id, product_id)
VALUES(2, 1);

INSERT INTO digital_booking.feature_product
(feature_id, product_id)
VALUES(3, 1);

INSERT INTO digital_booking.feature_product
(feature_id, product_id)
VALUES(4, 1);

INSERT INTO digital_booking.feature_product
(feature_id, product_id)
VALUES(4, 2);

INSERT INTO digital_booking.feature_product
(feature_id, product_id)
VALUES(5, 2);

INSERT INTO digital_booking.feature_product
(feature_id, product_id)
VALUES(6, 2);

INSERT INTO digital_booking.feature_product
(feature_id, product_id)
VALUES(1, 2);

INSERT INTO digital_booking.feature_product
(feature_id, product_id)
VALUES(2, 3);

INSERT INTO digital_booking.feature_product
(feature_id, product_id)
VALUES(1, 3);

INSERT INTO digital_booking.feature_product
(feature_id, product_id)
VALUES(7, 3);

INSERT INTO digital_booking.feature_product
(feature_id, product_id)
VALUES(8, 3);

INSERT INTO digital_booking.feature_product
(feature_id, product_id)
VALUES(8, 4);

INSERT INTO digital_booking.feature_product
(feature_id, product_id)
VALUES(9, 4);

INSERT INTO digital_booking.feature_product
(feature_id, product_id)
VALUES(5, 4);

INSERT INTO digital_booking.feature_product
(feature_id, product_id)
VALUES(2, 4);

INSERT INTO digital_booking.feature_product
(feature_id, product_id)
VALUES(2, 5);

INSERT INTO digital_booking.feature_product
(feature_id, product_id)
VALUES(3, 5);

INSERT INTO digital_booking.feature_product
(feature_id, product_id)
VALUES(6, 5);

INSERT INTO digital_booking.feature_product
(feature_id, product_id)
VALUES(7, 5);

INSERT INTO digital_booking.feature_product
(feature_id, product_id)
VALUES(7, 6);

INSERT INTO digital_booking.feature_product
(feature_id, product_id)
VALUES(9, 6);

INSERT INTO digital_booking.feature_product
(feature_id, product_id)
VALUES(3, 6);

INSERT INTO digital_booking.feature_product
(feature_id, product_id)
VALUES(4, 6);

INSERT INTO digital_booking.feature_product
(feature_id, product_id)
VALUES(4, 7);

INSERT INTO digital_booking.feature_product
(feature_id, product_id)
VALUES(1, 7);

INSERT INTO digital_booking.feature_product
(feature_id, product_id)
VALUES(9, 7);

INSERT INTO digital_booking.feature_product
(feature_id, product_id)
VALUES(6, 7);

INSERT INTO digital_booking.feature_product
(feature_id, product_id)
VALUES(2, 8);

INSERT INTO digital_booking.feature_product
(feature_id, product_id)
VALUES(8, 8);

INSERT INTO digital_booking.feature_product
(feature_id, product_id)
VALUES(4, 8);

INSERT INTO digital_booking.feature_product
(feature_id, product_id)
VALUES(7, 8);

INSERT INTO digital_booking.feature_product
(feature_id, product_id)
VALUES(9, 9);

INSERT INTO digital_booking.feature_product
(feature_id, product_id)
VALUES(7, 9);

INSERT INTO digital_booking.feature_product
(feature_id, product_id)
VALUES(3, 9);

INSERT INTO digital_booking.feature_product
(feature_id, product_id)
VALUES(2, 9);

INSERT INTO digital_booking.feature_product
(feature_id, product_id)
VALUES(4, 10);

INSERT INTO digital_booking.feature_product
(feature_id, product_id)
VALUES(6, 10);

INSERT INTO digital_booking.feature_product
(feature_id, product_id)
VALUES(1, 10);

INSERT INTO digital_booking.feature_product
(feature_id, product_id)
VALUES(7, 10);

INSERT INTO digital_booking.feature_product
(feature_id, product_id)
VALUES(7, 11);

INSERT INTO digital_booking.feature_product
(feature_id, product_id)
VALUES(4, 11);

INSERT INTO digital_booking.feature_product
(feature_id, product_id)
VALUES(5, 11);

INSERT INTO digital_booking.feature_product
(feature_id, product_id)
VALUES(1, 11);

INSERT INTO digital_booking.feature_product
(feature_id, product_id)
VALUES(9, 12);

INSERT INTO digital_booking.feature_product
(feature_id, product_id)
VALUES(6, 12);

INSERT INTO digital_booking.feature_product
(feature_id, product_id)
VALUES(8, 12);

INSERT INTO digital_booking.feature_product
(feature_id, product_id)
VALUES(3, 12);

INSERT INTO digital_booking.feature_product
(feature_id, product_id)
VALUES(1, 13);

INSERT INTO digital_booking.feature_product
(feature_id, product_id)
VALUES(2, 13);

INSERT INTO digital_booking.feature_product
(feature_id, product_id)
VALUES(7, 13);

INSERT INTO digital_booking.feature_product
(feature_id, product_id)
VALUES(17, 13);

INSERT INTO digital_booking.feature_product
(feature_id, product_id)
VALUES(10, 14);

INSERT INTO digital_booking.feature_product
(feature_id, product_id)
VALUES(11, 14);

INSERT INTO digital_booking.feature_product
(feature_id, product_id)
VALUES(12, 14);

INSERT INTO digital_booking.feature_product
(feature_id, product_id)
VALUES(13, 14);

INSERT INTO digital_booking.feature_product
(feature_id, product_id)
VALUES(14, 14);

INSERT INTO digital_booking.feature_product
(feature_id, product_id)
VALUES(6, 15);

INSERT INTO digital_booking.feature_product
(feature_id, product_id)
VALUES(7, 15);

INSERT INTO digital_booking.feature_product
(feature_id, product_id)
VALUES(22, 15);

INSERT INTO digital_booking.feature_product
(feature_id, product_id)
VALUES(23, 15);

INSERT INTO digital_booking.feature_product
(feature_id, product_id)
VALUES(9, 15);

-- INSERT PARA IMAGENES

INSERT INTO digital_booking.images
(image_url, title, product_id)
VALUES('https://0521ptc3n1-grupo7-imagenes.s3.us-east-2.amazonaws.com/images/1/101_recepcion.webp',
	'recepcion', 
	1);

INSERT INTO digital_booking.images
(image_url, title, product_id)
VALUES('https://0521ptc3n1-grupo7-imagenes.s3.us-east-2.amazonaws.com/images/1/102_bedroom.jpeg',
	'bedroom', 
	1);

INSERT INTO digital_booking.images
(image_url, title, product_id)
VALUES('https://0521ptc3n1-grupo7-imagenes.s3.us-east-2.amazonaws.com/images/1/103_outside.jpg',
	'outside', 
	1);

INSERT INTO digital_booking.images
(image_url, title, product_id)
VALUES('https://0521ptc3n1-grupo7-imagenes.s3.us-east-2.amazonaws.com/images/1/104_pool.jpeg',
	'pool', 
	1);

INSERT INTO digital_booking.images
(image_url, title, product_id)
VALUES('https://0521ptc3n1-grupo7-imagenes.s3.us-east-2.amazonaws.com/images/1/105_dining+room.jpeg',
	'dining room', 
	1);

INSERT INTO digital_booking.images
(image_url, title, product_id)
VALUES('https://0521ptc3n1-grupo7-imagenes.s3.us-east-2.amazonaws.com/images/2/206_recepcion.jpeg',
	'recepcion', 
	2);

INSERT INTO digital_booking.images
(image_url, title, product_id)
VALUES('https://0521ptc3n1-grupo7-imagenes.s3.us-east-2.amazonaws.com/images/2/207_dining_room.jpeg',
	'dining room', 
	2);

INSERT INTO digital_booking.images
(image_url, title, product_id)
VALUES('https://0521ptc3n1-grupo7-imagenes.s3.us-east-2.amazonaws.com/images/2/208_outside.webp',
	'outside', 
	2);

INSERT INTO digital_booking.images
(image_url, title, product_id)
VALUES('https://0521ptc3n1-grupo7-imagenes.s3.us-east-2.amazonaws.com/images/2/209_pool.jpeg',
	'pool', 
	2);

INSERT INTO digital_booking.images
(image_url, title, product_id)
VALUES('https://0521ptc3n1-grupo7-imagenes.s3.us-east-2.amazonaws.com/images/2/210_bed_room.jpeg',
	'bed room', 
	2);

INSERT INTO digital_booking.images
(image_url, title, product_id)
VALUES('https://0521ptc3n1-grupo7-imagenes.s3.us-east-2.amazonaws.com/images/3/311_recepcion.jpeg',
	'recepcion', 
	3);

INSERT INTO digital_booking.images
(image_url, title, product_id)
VALUES('https://0521ptc3n1-grupo7-imagenes.s3.us-east-2.amazonaws.com/images/3/312_bedroom.jpeg',
	'bedroom', 
	3);

INSERT INTO digital_booking.images
(image_url, title, product_id)
VALUES('https://0521ptc3n1-grupo7-imagenes.s3.us-east-2.amazonaws.com/images/3/313_outside.jpeg',
	'outside', 
	3);

INSERT INTO digital_booking.images
(image_url, title, product_id)
VALUES('https://0521ptc3n1-grupo7-imagenes.s3.us-east-2.amazonaws.com/images/3/314_dining_room.jpeg',
	'dining room', 
	3);

INSERT INTO digital_booking.images
(image_url, title, product_id)
VALUES('https://0521ptc3n1-grupo7-imagenes.s3.us-east-2.amazonaws.com/images/3/315_room.jpeg',
	'room', 
	3);

INSERT INTO digital_booking.images
(image_url, title, product_id)
VALUES('https://0521ptc3n1-grupo7-imagenes.s3.us-east-2.amazonaws.com/images/4/416_recepcion.webp',
	'recepcion', 
	4);

INSERT INTO digital_booking.images
(image_url, title, product_id)
VALUES('https://0521ptc3n1-grupo7-imagenes.s3.us-east-2.amazonaws.com/images/4/417_bedroom.jpeg',
	'bedroom', 
	4);

INSERT INTO digital_booking.images
(image_url, title, product_id)
VALUES('https://0521ptc3n1-grupo7-imagenes.s3.us-east-2.amazonaws.com/images/4/418_outside.webp',
	'outside', 
	4);

INSERT INTO digital_booking.images
(image_url, title, product_id)
VALUES('https://0521ptc3n1-grupo7-imagenes.s3.us-east-2.amazonaws.com/images/4/419_dining_room.jpeg',
	'dining room', 
	4);

INSERT INTO digital_booking.images
(image_url, title, product_id)
VALUES('https://0521ptc3n1-grupo7-imagenes.s3.us-east-2.amazonaws.com/images/4/420_recepcion.jpeg',
	'recepcion', 
	4);

INSERT INTO digital_booking.images
(image_url, title, product_id)
VALUES('https://0521ptc3n1-grupo7-imagenes.s3.us-east-2.amazonaws.com/images/5/521_outside.jpeg',
	'outside', 
	5);

INSERT INTO digital_booking.images
(image_url, title, product_id)
VALUES('https://0521ptc3n1-grupo7-imagenes.s3.us-east-2.amazonaws.com/images/5/522_recepcion.webp',
	'recepcion', 
	5);

INSERT INTO digital_booking.images
(image_url, title, product_id)
VALUES('https://0521ptc3n1-grupo7-imagenes.s3.us-east-2.amazonaws.com/images/5/523_bedroom.jpeg',
	'bedroom', 
	5);

INSERT INTO digital_booking.images
(image_url, title, product_id)
VALUES('https://0521ptc3n1-grupo7-imagenes.s3.us-east-2.amazonaws.com/images/5/524_pool.jpeg',
	'pool', 
	5);

INSERT INTO digital_booking.images
(image_url, title, product_id)
VALUES('https://0521ptc3n1-grupo7-imagenes.s3.us-east-2.amazonaws.com/images/5/525_dining_room.webp',
	'dining room', 
	5);

INSERT INTO digital_booking.images
(image_url, title, product_id)
VALUES('https://0521ptc3n1-grupo7-imagenes.s3.us-east-2.amazonaws.com/images/6/626_outside.jpeg',
	'outside', 
	6);

INSERT INTO digital_booking.images
(image_url, title, product_id)
VALUES('https://0521ptc3n1-grupo7-imagenes.s3.us-east-2.amazonaws.com/images/6/627_recepcion.jpeg',
	'recepcion', 
	6);

INSERT INTO digital_booking.images
(image_url, title, product_id)
VALUES('https://0521ptc3n1-grupo7-imagenes.s3.us-east-2.amazonaws.com/images/6/628_bedroom.webp',
	'bedroom', 
	6);

INSERT INTO digital_booking.images
(image_url, title, product_id)
VALUES('https://0521ptc3n1-grupo7-imagenes.s3.us-east-2.amazonaws.com/images/6/629_pool.jpeg',
	'pool', 
	6);

INSERT INTO digital_booking.images
(image_url, title, product_id)
VALUES('https://0521ptc3n1-grupo7-imagenes.s3.us-east-2.amazonaws.com/images/6/630_dining_room.webp',
	'dining room', 
	6);

INSERT INTO digital_booking.images
(image_url, title, product_id)
VALUES('https://0521ptc3n1-grupo7-imagenes.s3.us-east-2.amazonaws.com/images/7/731_recepcion.jpeg',
	'recepcion', 
	7);

INSERT INTO digital_booking.images
(image_url, title, product_id)
VALUES('https://0521ptc3n1-grupo7-imagenes.s3.us-east-2.amazonaws.com/images/7/732_bedroom.jpeg',
	'bedroom', 
	7);

INSERT INTO digital_booking.images
(image_url, title, product_id)
VALUES('https://0521ptc3n1-grupo7-imagenes.s3.us-east-2.amazonaws.com/images/7/733_outside.webp',
	'outside', 
	7);

INSERT INTO digital_booking.images
(image_url, title, product_id)
VALUES('https://0521ptc3n1-grupo7-imagenes.s3.us-east-2.amazonaws.com/images/7/734_pool.jpeg',
	'pool', 
	7);

INSERT INTO digital_booking.images
(image_url, title, product_id)
VALUES('https://0521ptc3n1-grupo7-imagenes.s3.us-east-2.amazonaws.com/images/7/735_dining_room.webp',
	'dining room', 
	7);

INSERT INTO digital_booking.images
(image_url, title, product_id)
VALUES('https://0521ptc3n1-grupo7-imagenes.s3.us-east-2.amazonaws.com/images/8/836_recepcion.jpeg',
	'recepcion', 
	8);

INSERT INTO digital_booking.images
(image_url, title, product_id)
VALUES('https://0521ptc3n1-grupo7-imagenes.s3.us-east-2.amazonaws.com/images/8/837_bedroom.webp',
	'bedroom', 
	8);

INSERT INTO digital_booking.images
(image_url, title, product_id)
VALUES('https://0521ptc3n1-grupo7-imagenes.s3.us-east-2.amazonaws.com/images/8/838_outside.jpeg',
	'outside', 
	8);

INSERT INTO digital_booking.images
(image_url, title, product_id)
VALUES('https://0521ptc3n1-grupo7-imagenes.s3.us-east-2.amazonaws.com/images/8/839_pool.jpeg',
	'pool', 
	8);

INSERT INTO digital_booking.images
(image_url, title, product_id)
VALUES('https://0521ptc3n1-grupo7-imagenes.s3.us-east-2.amazonaws.com/images/8/840_dining_room.jpeg',
	'dining room', 
	8);

INSERT INTO digital_booking.images
(image_url, title, product_id)
VALUES('https://0521ptc3n1-grupo7-imagenes.s3.us-east-2.amazonaws.com/images/9/941_recepcion.jpeg',
	'recepcion', 
	9);

INSERT INTO digital_booking.images
(image_url, title, product_id)
VALUES('https://0521ptc3n1-grupo7-imagenes.s3.us-east-2.amazonaws.com/images/9/942_bedroom.webp',
	'bedroom', 
	9);

INSERT INTO digital_booking.images
(image_url, title, product_id)
VALUES('https://0521ptc3n1-grupo7-imagenes.s3.us-east-2.amazonaws.com/images/9/943_outside.webp',
	'outside', 
	9);

INSERT INTO digital_booking.images
(image_url, title, product_id)
VALUES('https://0521ptc3n1-grupo7-imagenes.s3.us-east-2.amazonaws.com/images/9/944_pool.jpeg',
	'pool', 
	9);

INSERT INTO digital_booking.images
(image_url, title, product_id)
VALUES('https://0521ptc3n1-grupo7-imagenes.s3.us-east-2.amazonaws.com/images/9/945_dining_room.jpeg',
	'dining room', 
	9);

INSERT INTO digital_booking.images
(image_url, title, product_id)
VALUES('https://0521ptc3n1-grupo7-imagenes.s3.us-east-2.amazonaws.com/images/10/1046_recepcion.jpeg',
	'recepcion', 
	10);

INSERT INTO digital_booking.images
(image_url, title, product_id)
VALUES('https://0521ptc3n1-grupo7-imagenes.s3.us-east-2.amazonaws.com/images/10/1047_bedroom.jpeg',
	'bedroom', 
	10);

INSERT INTO digital_booking.images
(image_url, title, product_id)
VALUES('https://0521ptc3n1-grupo7-imagenes.s3.us-east-2.amazonaws.com/images/10/1048_outside.jpeg',
	'outside', 
	10);

INSERT INTO digital_booking.images
(image_url, title, product_id)
VALUES('https://0521ptc3n1-grupo7-imagenes.s3.us-east-2.amazonaws.com/images/10/1049_pool.jpeg',
	'pool', 
	10);

INSERT INTO digital_booking.images
(image_url, title, product_id)
VALUES('https://0521ptc3n1-grupo7-imagenes.s3.us-east-2.amazonaws.com/images/10/1050_dining_room.webp',
	'dining room', 
	10);

INSERT INTO digital_booking.images
(image_url, title, product_id)
VALUES('https://0521ptc3n1-grupo7-imagenes.s3.us-east-2.amazonaws.com/images/11/1151_cascada.webp',
	'dining room', 
	11);

INSERT INTO digital_booking.images
(image_url, title, product_id)
VALUES('https://0521ptc3n1-grupo7-imagenes.s3.us-east-2.amazonaws.com/images/11/1152_edificio.jpeg',
	'dining room', 
	11);

INSERT INTO digital_booking.images
(image_url, title, product_id)
VALUES('https://0521ptc3n1-grupo7-imagenes.s3.us-east-2.amazonaws.com/images/11/1153_iguazu.jpeg',
	'dining room', 
	11);

INSERT INTO digital_booking.images
(image_url, title, product_id)
VALUES('https://0521ptc3n1-grupo7-imagenes.s3.us-east-2.amazonaws.com/images/11/1154.jpeg',
	'dining room', 
	11);

INSERT INTO digital_booking.images
(image_url, title, product_id)
VALUES('https://0521ptc3n1-grupo7-imagenes.s3.us-east-2.amazonaws.com/images/11/1155_paracaidas.webp',
	'dining room', 
	11);

INSERT INTO digital_booking.images
(image_url, title, product_id)
VALUES('https://0521ptc3n1-grupo7-imagenes.s3.us-east-2.amazonaws.com/images/12/1256_fachada.jpeg',
	'dining room', 
	12);

INSERT INTO digital_booking.images
(image_url, title, product_id)
VALUES('https://0521ptc3n1-grupo7-imagenes.s3.us-east-2.amazonaws.com/images/12/1257_comedor.jpeg',
	'dining room', 
	12);

INSERT INTO digital_booking.images
(image_url, title, product_id)
VALUES('https://0521ptc3n1-grupo7-imagenes.s3.us-east-2.amazonaws.com/images/12/1258_dormitorio.jpeg',
	'dining room', 
	12);

INSERT INTO digital_booking.images
(image_url, title, product_id)
VALUES('https://0521ptc3n1-grupo7-imagenes.s3.us-east-2.amazonaws.com/images/12/1259_pileta.webp',
	'dining room', 
	12);

INSERT INTO digital_booking.images
(image_url, title, product_id)
VALUES('https://0521ptc3n1-grupo7-imagenes.s3.us-east-2.amazonaws.com/images/12/1260_fijo.webp',
	'dining room', 
	12);

INSERT INTO digital_booking.images
(image_url, title, product_id)
VALUES(
	'https://images.pexels.com/photos/14175804/pexels-photo-14175804.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1',
	'frente', 
	13);

INSERT INTO digital_booking.images
(image_url, title, product_id)
VALUES(
	'https://images.pexels.com/photos/8279631/pexels-photo-8279631.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1',
	'desayuno', 
	13);

INSERT INTO digital_booking.images
(image_url, title, product_id)
VALUES(
	'https://images.pexels.com/photos/12509875/pexels-photo-12509875.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1',
	'puerto', 
	13);

INSERT INTO digital_booking.images
(image_url, title, product_id)
VALUES(
	'https://images.pexels.com/photos/2516401/pexels-photo-2516401.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1',
	'snow', 
	13);

INSERT INTO digital_booking.images
(image_url, title, product_id)
VALUES(
	'https://images.pexels.com/photos/11857305/pexels-photo-11857305.jpeg?auto=compress&cs=tinysrgb&w=1600',
	'bedroom', 
	13);

INSERT INTO digital_booking.images
(image_url, title, product_id)
VALUES('https://images.pexels.com/photos/7488914/pexels-photo-7488914.jpeg?auto=compress&cs=tinysrgb&w=1600', 'frente', 14);

INSERT INTO digital_booking.images
(image_url, title, product_id)
VALUES('https://images.pexels.com/photos/12870167/pexels-photo-12870167.jpeg?auto=compress&cs=tinysrgb&w=1600', 'cama', 14);

INSERT INTO digital_booking.images
(image_url, title, product_id)
VALUES('https://images.pexels.com/photos/1684168/pexels-photo-1684168.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1', 'vista', 14);

INSERT INTO digital_booking.images
(image_url, title, product_id)
VALUES('https://images.pexels.com/photos/4042406/pexels-photo-4042406.jpeg?auto=compress&cs=tinysrgb&w=1600', 'hielo', 14);

INSERT INTO digital_booking.images
(image_url, title, product_id)
VALUES('https://images.pexels.com/photos/12329137/pexels-photo-12329137.jpeg?auto=compress&cs=tinysrgb&w=1600', 'ventana', 14);

INSERT INTO digital_booking.images
(image_url, title, product_id)
VALUES('https://images.pexels.com/photos/12453722/pexels-photo-12453722.jpeg?auto=compress&cs=tinysrgb&w=1600', 'hotel', 15);

INSERT INTO digital_booking.images
(image_url, title, product_id)
VALUES('https://images.pexels.com/photos/12619949/pexels-photo-12619949.jpeg?auto=compress&cs=tinysrgb&w=1600', 'pileta', 15);

INSERT INTO digital_booking.images
(image_url, title, product_id)
VALUES('https://images.pexels.com/photos/1579739/pexels-photo-1579739.jpeg?auto=compress&cs=tinysrgb&w=1600', 'desayuno', 15);

INSERT INTO digital_booking.images
(image_url, title, product_id)
VALUES('https://images.pexels.com/photos/13537862/pexels-photo-13537862.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1', 'frio', 15);

INSERT INTO digital_booking.images
(image_url, title, product_id)
VALUES('https://images.pexels.com/photos/931887/pexels-photo-931887.jpeg?auto=compress&cs=tinysrgb&w=1600', 'moderno', 15);


-- INSERT PARA RESERVAS

INSERT INTO digital_booking.reserves
(check_in, check_out, hour_in, user_id, product_id)
VALUES('2022-12-15', '2022-12-17', '13:00', 4, 1);

INSERT INTO digital_booking.reserves
(check_in, check_out, hour_in, user_id, product_id)
VALUES('2022-12-23', '2022-12-28', '09:00', 4, 5);

INSERT INTO digital_booking.reserves
(check_in, check_out, hour_in, user_id, product_id)
VALUES('2022-12-17', '2022-12-24', '17:00', 4, 3);

INSERT INTO digital_booking.reserves
(check_in, check_out, hour_in, user_id, product_id)
VALUES('2023-01-02', '2023-01-16', '10:00', 5, 1);

INSERT INTO digital_booking.reserves
(check_in, check_out, hour_in, user_id, product_id)
VALUES('2023-01-05', '2023-01-12', '15:00', 5, 3);

INSERT INTO digital_booking.reserves
(check_in, check_out, hour_in, user_id, product_id)
VALUES('2023-01-04', '2023-01-09', '10:00', 5, 1);


