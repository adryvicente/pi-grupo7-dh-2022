package dh.ctd.back.repository;

import dh.ctd.back.entity.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;



@Repository
public interface ICategoryRepository extends JpaRepository<Category, Long> {

}

