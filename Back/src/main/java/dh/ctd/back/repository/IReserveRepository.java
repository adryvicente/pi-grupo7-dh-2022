package dh.ctd.back.repository;

import dh.ctd.back.entity.Product;
import dh.ctd.back.entity.Reserve;
import dh.ctd.back.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.util.List;

@Repository
public interface IReserveRepository extends JpaRepository<Reserve, Long> {
     List<Reserve> findAllByProduct(Product productSelected);

    List<Reserve> findAllByCustomer(User userAuthenticated);
}
