package dh.ctd.back.repository;

import dh.ctd.back.entity.Image;
import dh.ctd.back.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public interface IImageRepository extends JpaRepository<Image, Long> {

    List<Image> findAllImagesByProduct(Product product);

    void deleteAllByProduct(Product product);
}
