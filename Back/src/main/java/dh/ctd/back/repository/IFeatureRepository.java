package dh.ctd.back.repository;

import dh.ctd.back.entity.Feature;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IFeatureRepository extends JpaRepository<Feature, Long> {

}
