package dh.ctd.back.repository;

import dh.ctd.back.entity.City;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ICityRepository extends JpaRepository<City, Long> {

    City findCityByName(String name);

}
