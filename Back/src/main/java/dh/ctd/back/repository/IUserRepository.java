package dh.ctd.back.repository;

import dh.ctd.back.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface IUserRepository extends JpaRepository<User, Long> {

    //Optional<User> findByEmail(String email);

    User findByEmail(String email);
}
