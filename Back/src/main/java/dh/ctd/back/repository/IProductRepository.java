package dh.ctd.back.repository;

import dh.ctd.back.entity.Category;
import dh.ctd.back.entity.City;
import dh.ctd.back.entity.Product;
import dh.ctd.back.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.util.List;

@Repository
public interface IProductRepository extends JpaRepository<Product, Long> {
    List<Product> findAllByCategory(Category categorySelected);
    List<Product> findAllByCity(City city);

    @Query(
            value = "select * from products p where p.product_id not in (select product_id from reserves r where (r.check_in between :checkin and :checkout or r.check_out between :checkin and :checkout) or (r.check_in > :checkin and r.check_out < :checkout))",
            nativeQuery = true
    )
    List<Product> findByDateFilter(Date checkin, Date checkout);

    @Query(
            value = "select * from products p where p.city_id = :city and p.product_id not in (select product_id from reserves r where (r.check_in between :checkin and :checkout or r.check_out between :checkin and :checkout) or (r.check_in > :checkin and r.check_out < :checkout))",
            nativeQuery = true
    )
    List<Product> findByDateAndCityFilter(Long city, Date checkin, Date checkout);


    List<Product> findAllByOwner(User user);
}
