package dh.ctd.back;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@SpringBootApplication
public class BackApplication {

	public static void main(String[] args) {

		SpringApplication.run(BackApplication.class, args);
	}

	@Configuration
	@EnableWebMvc
	public class WebConfig implements WebMvcConfigurer {

		@Override
		public void addCorsMappings(CorsRegistry registry) {

			registry.addMapping("/**")
					.allowedOriginPatterns("*")
					.allowCredentials(true)
					.allowedMethods("GET", "POST", "PUT", "DELETE", "PATCH")
					.allowedHeaders("Origin", "Content-type", "Accept", "Authorization")
					.maxAge(3600);
		}
	}

}
