package dh.ctd.back.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import java.util.ArrayList;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class CreateProductoRequest {

    private String name;
    private String descriptionTitle;
    private String description;
    private String address;
    private Long categoryId;
    private Long cityId;
    private ArrayList<Long> featuresId;
    private ArrayList<CreateImageRequest> images;
    private String rules;
    private String healthAndSecurity;
    private String cancellationPolicies;
    private Double latitude;
    private Double longitude;

}
