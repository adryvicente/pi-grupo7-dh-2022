package dh.ctd.back.dto.response;

import lombok.*;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ListReservesResponse {

    private List<ReserveResponse> listReservesResponse;


}
