package dh.ctd.back.dto.response;


import dh.ctd.back.dto.Customer;
import dh.ctd.back.entity.Product;
import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ReserveResponse {

    private Long idReserve;
    private String hourIn;
    private String checkIn;
    private String checkOut;
    private Long productId;
    private Long customerId;
}
