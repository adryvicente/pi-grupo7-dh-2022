package dh.ctd.back.dto.response;
import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class CategoryResponse {

    private Long idCategory;
    private String title;
    private String description;
    private String urlImage;
}
