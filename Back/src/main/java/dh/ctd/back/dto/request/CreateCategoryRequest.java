package dh.ctd.back.dto.request;
import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class CreateCategoryRequest {

    private String title;
    private String description;
    private String urlImage;    
    
}