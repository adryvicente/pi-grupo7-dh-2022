package dh.ctd.back.dto.request;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CreateReserveRequest {

    private String hourIn;
    private String checkIn;
    private String checkOut;

    private Long productId;
    private Long customerId;






}
