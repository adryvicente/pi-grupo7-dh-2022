package dh.ctd.back.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class ProductResponse {

    private Long idProduct;
    private String name;
    private String descriptionTitle;
    private String description;
    private String categoryTitle;
    private String address;
    private CityResponse city;
    private List<FeatureResponse> features;
    private List<ImageResponse> images;
    private String rules;
    private String helathAndSecurity;
    private String cancellationPolicies;
    private Double latitude;
    private Double longitude;
    private String owner;
}
