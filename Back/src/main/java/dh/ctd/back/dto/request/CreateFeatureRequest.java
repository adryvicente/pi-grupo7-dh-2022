package dh.ctd.back.dto.request;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class CreateFeatureRequest {
    private String name;
    private String icon;

}
