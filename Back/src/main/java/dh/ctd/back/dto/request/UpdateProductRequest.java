package dh.ctd.back.dto.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class UpdateProductRequest {
    private String name;
    private String descriptionTitle;
    private String description;
    private String address;
    private Long categoryId;
    private Long cityId;
    private ArrayList<Long> featuresId;
    private ArrayList<CreateImageRequest> images;
    private String rules;
    private String healthAndSecurity;
    private String cancellationPolicies;
    private Double latitude;
    private Double longitude;

}
