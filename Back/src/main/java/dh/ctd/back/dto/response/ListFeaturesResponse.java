package dh.ctd.back.dto.response;

import lombok.*;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class ListFeaturesResponse {
    private List<FeatureResponse> listFeatureResponse;
}
