package dh.ctd.back.dto.response;
import lombok.*;



@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class FeatureResponse {

    private Long featureId;
    private String name;
    private String icon;
}
