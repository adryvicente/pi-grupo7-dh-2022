package dh.ctd.back.dto.request;

import dh.ctd.back.entity.Product;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CreateImageRequest {

    private String title;
    private String imageUrl;
    private Product product;
}
