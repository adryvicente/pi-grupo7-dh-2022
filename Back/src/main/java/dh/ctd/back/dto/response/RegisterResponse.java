package dh.ctd.back.dto.response;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class RegisterResponse {
    private Long id;
    private String name;
    private String lastname;
    private String email;
    private String token;
}
