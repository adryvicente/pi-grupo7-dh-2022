package dh.ctd.back.dto.response;

import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ImageResponse {

    private Long imageId;
    private String title;
    private String imageUrl;

}
