package dh.ctd.back.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserDataResponse {
    private Long idUser;
    private String name;
    private String lastname;
    private String email;
    private String city;
    private String role;
}
