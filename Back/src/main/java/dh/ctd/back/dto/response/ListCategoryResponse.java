package dh.ctd.back.dto.response;

import lombok.*;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class ListCategoryResponse {

    private List<CategoryResponse> listCategoryResponse;
}
