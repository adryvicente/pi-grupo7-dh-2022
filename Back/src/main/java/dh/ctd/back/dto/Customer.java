package dh.ctd.back.dto;

import dh.ctd.back.entity.City;
import dh.ctd.back.entity.Reserve;
import dh.ctd.back.entity.Role;
import dh.ctd.back.entity.User;
import lombok.*;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
//@Builder
public class Customer extends User {
    public Customer(Long idUser, String name, String lastname, String email, String password, City city, Role role, List<Reserve> reserves) {
        super(idUser, name, lastname, email, password, city, role, reserves);
    }



}
