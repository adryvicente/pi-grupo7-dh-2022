package dh.ctd.back.dto.response;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class CityResponse {

    private Long idCity;
    private String name;
    private String province;
    private String country;
}
