package dh.ctd.back.dto.response;

import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ListImageResponse {
    private List<ImageResponse> listImageResponse;
}
