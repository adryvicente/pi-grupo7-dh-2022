package dh.ctd.back.exception;

public class EntityNotFoundExc extends RuntimeException {
    public EntityNotFoundExc(String message){
        super(message);
    }

}
