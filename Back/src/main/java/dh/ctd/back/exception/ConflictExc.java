package dh.ctd.back.exception;

public class ConflictExc extends Exception{

    public ConflictExc(String message) {
        super(message);
    }
}

