package dh.ctd.back.exception;

import dh.ctd.back.dto.response.ErrorResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.List;

@ControllerAdvice
public class DefaultExceptionHandler {

    @ExceptionHandler(value = EntityNotFoundExc.class)
    protected ResponseEntity<ErrorResponse> handleEntityNotFound(EntityNotFoundExc e) {
        ErrorResponse errorResponse = buildErrorResponse(
                HttpStatus.NOT_FOUND,
                "Entity not found.",
                List.of(e.getMessage()));
        return new ResponseEntity<>(errorResponse, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = UserAlreadyExistExc.class)
    protected ResponseEntity<ErrorResponse> handleUserAlreadyExistException(
            UserAlreadyExistExc e) {
        ErrorResponse errorResponse = buildErrorResponse(
                HttpStatus.CONFLICT,
                e.getMessage(),
                List.of("The server could not complete the user registration because "
                        + "the email address entered is already in use."));
        return new ResponseEntity<>(errorResponse, HttpStatus.CONFLICT);
    }

    @ExceptionHandler(value = OperationNotPermittedExc.class)
    public ResponseEntity<ErrorResponse> handleOperationNotPermittedException(
            OperationNotPermittedExc e) {
        ErrorResponse errorResponse = buildErrorResponse(
                HttpStatus.FORBIDDEN,
                "Operation not permitted.",
                List.of(e.getMessage()));
        return new ResponseEntity<>(errorResponse, HttpStatus.FORBIDDEN);
    }

    private ErrorResponse buildErrorResponse(HttpStatus httpStatus, String message,
                                             List<String> moreInfo) {
        return ErrorResponse.builder()
                .statusCode(httpStatus.value())
                .message(message)
                .moreInfo(moreInfo)
                .build();
    }
}
