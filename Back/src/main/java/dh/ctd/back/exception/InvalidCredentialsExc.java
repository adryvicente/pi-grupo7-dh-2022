package dh.ctd.back.exception;

public class InvalidCredentialsExc extends RuntimeException{
    public InvalidCredentialsExc(String message) {
        super(message);
    }
}
