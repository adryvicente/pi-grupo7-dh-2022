package dh.ctd.back.exception;

public class OperationNotPermittedExc extends RuntimeException {

    public OperationNotPermittedExc(String message) {
        super(message);
    }
}
