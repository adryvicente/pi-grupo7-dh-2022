package dh.ctd.back.exception;

public class UserAlreadyExistExc extends RuntimeException {
    public UserAlreadyExistExc(String message) {
        super(message);
    }

}
