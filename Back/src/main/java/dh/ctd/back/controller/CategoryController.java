package dh.ctd.back.controller;

import dh.ctd.back.dto.request.CreateCategoryRequest;
import dh.ctd.back.dto.response.CategoryResponse;
import dh.ctd.back.dto.response.ListCategoryResponse;
import dh.ctd.back.service.abstraction.ICategoryService;
import dh.ctd.back.util.Jsons;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/categories")
public class CategoryController {

    @Autowired
    private ICategoryService categoriaService;

    @GetMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ListCategoryResponse> listCategories(){
        return ResponseEntity.ok().body(categoriaService.listCategories());
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CategoryResponse> getFeatureId(@PathVariable Long id){
        return  ResponseEntity.ok().body(categoriaService.getById(id));
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CategoryResponse> save(@RequestBody CreateCategoryRequest createCategoryRequest){
        return ResponseEntity.ok().body(categoriaService.save(createCategoryRequest));
    }

    @PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CategoryResponse> update(@RequestBody CreateCategoryRequest createCategoryRequest, @PathVariable Long id){
        return ResponseEntity.ok().body(categoriaService.update(createCategoryRequest,id));
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> delete(@PathVariable Long id){
        categoriaService.delete(id);
        String mensajeJSON = Jsons.asJsonString( "Category with ID:" + id + " was successfully removed.");
        return new ResponseEntity<>(mensajeJSON, HttpStatus.NO_CONTENT);
    }
}