package dh.ctd.back.controller;

import dh.ctd.back.dto.request.CreateReserveRequest;
import dh.ctd.back.dto.response.ListReservesResponse;
import dh.ctd.back.dto.response.ReserveResponse;
import dh.ctd.back.service.abstraction.IReserveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/reserves")
public class ReserveController {

    @Autowired
    private IReserveService reserveService;

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ReserveResponse> createReserve(@RequestBody CreateReserveRequest createReserveRequest){
        return ResponseEntity.ok().body(reserveService.save(createReserveRequest));
    }
    
    @GetMapping(value = "/products/{idProduct}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ListReservesResponse> findAllReservesByProduct(@PathVariable Long idProduct) {
        return ResponseEntity.ok().body(reserveService.listReservesByProduct(idProduct));
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ListReservesResponse> listMyReserves() {
        return ResponseEntity.ok().body(reserveService.listMyReserves());
    }

}
