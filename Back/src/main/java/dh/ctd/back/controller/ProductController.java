package dh.ctd.back.controller;

import dh.ctd.back.dto.request.CreateProductoRequest;
import dh.ctd.back.dto.request.UpdateProductRequest;
import dh.ctd.back.dto.response.ListProductsResponse;
import dh.ctd.back.dto.response.ProductResponse;
import dh.ctd.back.exception.EntityNotFoundExc;
import dh.ctd.back.service.abstraction.IProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;


@RestController
@RequestMapping("/products")
public class ProductController {

    @Autowired
    private IProductoService productService;

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ListProductsResponse> listProducts() {
        return ResponseEntity.ok().body(productService.listProducts());
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ProductResponse> create(@RequestBody CreateProductoRequest createProductoRequest) {

        ProductResponse response = productService.save(createProductoRequest);

        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(response.getIdProduct())
                .toUri();

        return ResponseEntity.created(location).body(response);
    }

    @PatchMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ProductResponse> update(@PathVariable Long id, @RequestBody CreateProductoRequest createProductRequest) {
        return ResponseEntity.ok().body(productService.update(id, createProductRequest));
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ProductResponse> getById(@PathVariable Long id) {
        return ResponseEntity.ok().body(productService.getById(id));
    }

    @GetMapping(value = "/category/{category}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ListProductsResponse> listProductsByCategory(@PathVariable Long category) throws EntityNotFoundExc {
        return ResponseEntity.ok().body(productService.listProductsByCategory(category));
    }

    @GetMapping(value = "/cities/{city}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ListProductsResponse> listProductsByCity(@PathVariable Long city) throws EntityNotFoundExc {
        return ResponseEntity.ok().body(productService.listProductsByCity(city));
    }

    @GetMapping(value = "/filters", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ListProductsResponse> listProductFiltered(
            @RequestParam String city,
            @RequestParam String checkin,
            @RequestParam String checkout) {
        return ResponseEntity.ok().body(productService.listProductsByFilters(Long.valueOf(city), checkin, checkout));
    }

    @GetMapping(value = "/my-products", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ListProductsResponse> listMyProducts() {
        return ResponseEntity.ok().body(productService.listMyProducts());
    }

}
