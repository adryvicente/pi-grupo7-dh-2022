package dh.ctd.back.controller;

import dh.ctd.back.dto.request.AuthenticationRequest;
import dh.ctd.back.dto.request.RegisterRequest;
import dh.ctd.back.dto.response.AuthenticationResponse;
import dh.ctd.back.dto.response.RegisterResponse;
import dh.ctd.back.dto.response.UserDataResponse;
import dh.ctd.back.service.abstraction.IAuthenticationService;
import dh.ctd.back.service.abstraction.IRegisterService;
import dh.ctd.back.service.abstraction.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/auth")
public class AuthenticationController {

    @Autowired
    private IAuthenticationService authService;
    @Autowired
    private IRegisterService registerService;
    @Autowired
    private IUserService userService;

    @PostMapping(path = "/register", produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RegisterResponse> register(@RequestBody RegisterRequest registerRequest) {
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(registerService.register(registerRequest));
    }

    @PostMapping(path = "/login", produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<AuthenticationResponse> login(@RequestBody AuthenticationRequest authenticationRequest) {
        return ResponseEntity.ok().body(authService.login(authenticationRequest));
    }

    @GetMapping(path = "/me", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserDataResponse> getUser() {
        return ResponseEntity.ok().body(userService.getUserAuthenticated());
    }

}
