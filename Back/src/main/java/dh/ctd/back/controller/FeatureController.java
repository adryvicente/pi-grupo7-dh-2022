package dh.ctd.back.controller;

import dh.ctd.back.dto.request.CreateCityRequest;
import dh.ctd.back.dto.request.CreateFeatureRequest;
import dh.ctd.back.dto.response.FeatureResponse;
import dh.ctd.back.dto.response.ListFeaturesResponse;
import dh.ctd.back.exception.EntityNotFoundExc;
import dh.ctd.back.service.abstraction.IFeatureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/features")
public class FeatureController {

    @Autowired
    private IFeatureService featureService;

    @GetMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ListFeaturesResponse> listFeatures(){
        return ResponseEntity.ok().body(featureService.listFeatures());
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<FeatureResponse> getFeatureById(@PathVariable Long id){
        return ResponseEntity.ok().body(featureService.getById(id));
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public  ResponseEntity<FeatureResponse> save(@RequestBody CreateFeatureRequest createFeatureRequest){
        return ResponseEntity.ok().body(featureService.save(createFeatureRequest));
    }

    @PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<FeatureResponse> update(@RequestBody CreateFeatureRequest createFeatureRequest, @PathVariable Long id) {
        return ResponseEntity.ok().body(featureService.update(createFeatureRequest, id));
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity delete(@PathVariable Long id) {
        featureService.delete(id);
        return new ResponseEntity(HttpStatus.OK);
    }

}
