package dh.ctd.back.controller;

import dh.ctd.back.dto.request.CreateCityRequest;
import dh.ctd.back.dto.response.CityResponse;
import dh.ctd.back.dto.response.ListCityResponse;
import dh.ctd.back.service.abstraction.ICityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/cities")
public class CityController {
    @Autowired
    private ICityService cityService;

    @GetMapping
    public ResponseEntity<ListCityResponse> listCities() {
        return ResponseEntity.ok().body(cityService.listCities());
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CityResponse> getCityById(@PathVariable Long id) {
        return ResponseEntity.ok().body(cityService.getById(id));
    }

    @GetMapping(value = "/name/{name}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CityResponse> getCityByName(@PathVariable String name) {
        return ResponseEntity.ok().body(cityService.getByName(name));
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CityResponse> save(@RequestBody CreateCityRequest createCityRequest) {
        return ResponseEntity.ok().body(cityService.save(createCityRequest));
    }

    @PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CityResponse> update(@RequestBody CreateCityRequest createCityRequest, @PathVariable Long id) {
        return ResponseEntity.ok().body(cityService.update(createCityRequest, id));
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> delete(@PathVariable Long id){
        cityService.delete(id);
        return ResponseEntity.noContent().build();
    }


}
