package dh.ctd.back.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import dh.ctd.back.dto.request.CreateReserveRequest;
import dh.ctd.back.dto.response.ListReservesResponse;
import dh.ctd.back.dto.response.ReserveResponse;
import dh.ctd.back.entity.Product;
import dh.ctd.back.entity.Reserve;
import dh.ctd.back.entity.User;
import dh.ctd.back.exception.EntityNotFoundExc;
import dh.ctd.back.repository.IProductRepository;
import dh.ctd.back.repository.IReserveRepository;
import dh.ctd.back.repository.IUserRepository;
import dh.ctd.back.service.abstraction.IEmailService;
import dh.ctd.back.service.abstraction.IProductoService;
import dh.ctd.back.service.abstraction.IReserveService;
import dh.ctd.back.util.ReserveMapper;
import dh.ctd.back.util.SecurityUtils;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.logging.Logger;

@Service
@AllArgsConstructor
public class ReserveServiceImpl implements IReserveService {

    @Autowired
    private ReserveMapper reserveMapper;
    @Autowired
    private ObjectMapper mapper;

    private final Logger logger = Logger.getLogger(String.valueOf(ReserveServiceImpl.class));

    @Autowired
    private IReserveRepository reserveRepository;
    @Autowired
    private IProductoService productoService;
    @Autowired
    private IUserRepository userRepository;
    @Autowired
    private IProductRepository productRepository;
    @Autowired
    private IEmailService emailService;
    @Autowired
    private SecurityUtils securityUtils;

    @Override
    public ReserveResponse save(CreateReserveRequest createReserveRequest) throws EntityNotFoundExc {

        Optional<User> user = userRepository.findById(createReserveRequest.getCustomerId());
        if (user.isEmpty()) {
            throw new EntityNotFoundExc("User not found.");
        }

        Optional<Product> product = productRepository.findById(createReserveRequest.getProductId());
        if (product.isEmpty()) {
            throw new EntityNotFoundExc("User not found.");
        }

        Reserve reserveSaved = reserveRepository
                .save(reserveMapper.ReserveRequestToReserve(createReserveRequest, product.get(), user.get()));
        logger.info("Saving new Reserve");

        if (reserveSaved != null) {
            emailService.sendReserveEmail(user.get(), product.get(), reserveSaved);
        }
        return reserveMapper.ReserveToReserveResponse(reserveSaved);

    }

    @Override
    public ListReservesResponse listReservesByProduct(Long productId) throws EntityNotFoundExc {
        Optional<Product> optionalProductSelected = productRepository.findById(productId);
        if (optionalProductSelected.isEmpty()) {
            throw new EntityNotFoundExc("Product not found.");
        }

        return ListReservesResponse.builder()
                .listReservesResponse((reserveMapper.listReservesToListReservesResponse(
                        reserveRepository.findAllByProduct(optionalProductSelected.get()))))
                .build();
    }

    @Override
    public ListReservesResponse listMyReserves() {
        User userAuthenticated = (User) securityUtils.getUserAuthenticated();

        return ListReservesResponse.builder()
                .listReservesResponse(reserveMapper
                        .listReservesToListReservesResponse(reserveRepository.findAllByCustomer(userAuthenticated)))
                .build();
    }

    @Override
    public ReserveResponse getById(Long id) {
        return null;
    }

    @Override
    public void delete(Long id) {

    }

    @Override
    public ReserveResponse update(CreateReserveRequest createReserveRequest) {
        return null;
    }

    @Override
    public ListReservesResponse listReserves() {
        return null;
    }

}
