package dh.ctd.back.service;

import com.sendgrid.Method;
import com.sendgrid.Request;
import com.sendgrid.Response;
import com.sendgrid.SendGrid;
import com.sendgrid.helpers.mail.Mail;
import com.sendgrid.helpers.mail.objects.Content;
import com.sendgrid.helpers.mail.objects.Email;
import dh.ctd.back.entity.Product;
import dh.ctd.back.entity.Reserve;
import dh.ctd.back.entity.User;
import dh.ctd.back.service.abstraction.IEmailService;
import dh.ctd.back.util.email.ReserveTemplate;
import dh.ctd.back.util.email.WelcomeTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class EmailServiceImpl implements IEmailService {

    @Autowired
    private Environment env;
    @Value("${digital.booking.email.sender}")
    private String emailSender;
    @Value("${digital.booking.email.enabled}")
    private boolean enabled;

    @Override
    public void sendWelcomeEmailTo(String to) {
        if (!enabled) {
            return;
        }
        String apiKey = env.getProperty("EMAIL_API_KEY");
        WelcomeTemplate welcomeTemplate = new WelcomeTemplate();

        Email fromEmail = new Email(emailSender);
        Email toEmail = new Email(to);
        Content content = new Content(
                "text/html", welcomeTemplate.getBody()
        );
        String subject = "Digital Booking";

        Mail mail = new Mail(fromEmail, subject, toEmail, content);
        SendGrid sg = new SendGrid(apiKey);
        Request request = new Request();

        try {
            request.setMethod(Method.POST);
            request.setEndpoint("mail/send");
            request.setBody(mail.build());
            Response response = sg.api(request);

            System.out.println(response.getStatusCode());
            System.out.println(response.getBody());
            System.out.println(response.getHeaders());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void sendReserveEmail(User user, Product product, Reserve reserve) {
        if (!enabled) {
            return;
        }
        String apiKey = env.getProperty("EMAIL_API_KEY");
        ReserveTemplate reserveTemplate = new ReserveTemplate(user, product, reserve);

        Email fromEmail = new Email(emailSender);
        Email toEmail = new Email(user.getEmail());
        Content content = new Content(
                "text/html", reserveTemplate.getBody(user, product, reserve)
        );
        String subject = "Detalles de su reserva generada en Digital Booking";

        Mail mail = new Mail(fromEmail, subject, toEmail, content);
        SendGrid sg = new SendGrid(apiKey);
        Request request = new Request();

        try {
            request.setMethod(Method.POST);
            request.setEndpoint("mail/send");
            request.setBody(mail.build());
            Response response = sg.api(request);

            System.out.println(response.getStatusCode());
            System.out.println(response.getBody());
            System.out.println(response.getHeaders());
        } catch (IOException e) {
            System.out.println("Fallo envio de email");
            throw new RuntimeException(e);
        }
    }
}
