package dh.ctd.back.service.abstraction;

import dh.ctd.back.dto.response.UserDataResponse;
import dh.ctd.back.entity.User;

public interface IUserService {
    UserDataResponse getUserAuthenticated();
}
