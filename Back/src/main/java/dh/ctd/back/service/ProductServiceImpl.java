package dh.ctd.back.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import dh.ctd.back.dto.request.CreateImageRequest;
import dh.ctd.back.dto.request.CreateProductoRequest;
import dh.ctd.back.dto.response.ListProductsResponse;
import dh.ctd.back.dto.response.ProductResponse;
import dh.ctd.back.entity.Category;
import dh.ctd.back.entity.City;
import dh.ctd.back.entity.Feature;
import dh.ctd.back.entity.Image;
import dh.ctd.back.entity.Product;
import dh.ctd.back.entity.User;
import dh.ctd.back.exception.EntityNotFoundExc;
import dh.ctd.back.exception.OperationNotPermittedExc;
import dh.ctd.back.repository.ICategoryRepository;
import dh.ctd.back.repository.ICityRepository;
import dh.ctd.back.repository.IFeatureRepository;
import dh.ctd.back.repository.IImageRepository;
import dh.ctd.back.repository.IProductRepository;
import dh.ctd.back.repository.IReserveRepository;
import dh.ctd.back.service.abstraction.ICategoryService;
import dh.ctd.back.service.abstraction.ICityService;
import dh.ctd.back.service.abstraction.IProductoService;
import dh.ctd.back.util.ImageMapper;
import dh.ctd.back.util.ProductMapper;
import dh.ctd.back.util.ReserveMapper;
import dh.ctd.back.util.SecurityUtils;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.sql.Date;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class ProductServiceImpl implements IProductoService {

    @Autowired
    private IProductRepository productoRepository;

    @Autowired
    private ICategoryRepository categoryRepository;

    @Autowired
    private ICityRepository cityRepository;

    @Autowired
    private IReserveRepository reserveRepository;

    @Autowired
    private IFeatureRepository featureRepository;

    @Autowired
    private IImageRepository imageRepository;

    @Autowired
    private ICategoryService categoriaService;

    @Autowired
    private ICityService cityService;

    @Autowired
    private ObjectMapper mapper;

    @Autowired
    private ProductMapper productMapper;

    @Autowired
    private ImageMapper imageMapper;

    @Autowired
    private SecurityUtils securityUtils;

    @Autowired
    private ReserveMapper reserveMapper;

    @Override
    public ProductResponse save(CreateProductoRequest createProductoRequest) {
        User userAuthenticated = (User) securityUtils.getUserAuthenticated();

        Category category = getCategory(createProductoRequest.getCategoryId());
        City city = getCity(createProductoRequest.getCityId());
        List<Feature> features = getFeatures(createProductoRequest.getFeaturesId());

        Product product = productMapper.productRequestToProduct(createProductoRequest);
        buildProduct(product, category, city, features, userAuthenticated);
        Product productSaved = productoRepository.save(product);

        saveImages(createProductoRequest.getImages(), productSaved);
        saveFeatures(features, productSaved);

        ProductResponse productResponse = productMapper.productToProductResponse(productSaved);

        return productResponse;
    }


    @Override
    public ListProductsResponse listProducts() {
        return ListProductsResponse.builder()
                .listProductResponse(productMapper.listProductToListProductResponse(productoRepository.findAll()))
                .build();
    }

    @Override
    public ProductResponse getById(Long id) {
        Optional<Product> optionalProduct = productoRepository.findById(id);
        if(optionalProduct.isEmpty()) {
            throw new EntityNotFoundExc("Product not found.");
        }
        return productMapper.productToProductResponse(optionalProduct.get());
    }

    @Override
    public ListProductsResponse listProductsByCategory(Long category) throws EntityNotFoundExc {
        Category categorySelected = mapper.convertValue(categoriaService.getById(category), Category.class);
        return ListProductsResponse.builder()
                .listProductResponse(productMapper.listProductToListProductResponse(productoRepository.findAllByCategory(categorySelected)))
                .build();
    }

    @Override
    public ListProductsResponse listProductsByCity(Long city) throws EntityNotFoundExc {
        City citySelected = mapper.convertValue(cityService.getById(city), City.class);
        return ListProductsResponse.builder()
                .listProductResponse(productMapper.listProductToListProductResponse(productoRepository.findAllByCity(citySelected)))
                .build();
    }

    @Override
    public ProductResponse update(Long id, CreateProductoRequest updateProductRequest) {
        User userAuthenticated = (User) securityUtils.getUserAuthenticated();
        Product productUpdate = findBy(id);

        if (!userAuthenticated.getEmail().equals(productUpdate.getOwner().getEmail())) {
            throw new OperationNotPermittedExc("No permission to perform this operation.");
        }

        Category category = getCategory(updateProductRequest.getCategoryId());
        City city = getCity(updateProductRequest.getCityId());
        List<Feature> features = getFeatures(updateProductRequest.getFeaturesId());

        buildProduct(productUpdate, category, city, features, userAuthenticated);

        deleteOldImages(productUpdate);
        saveImages(updateProductRequest.getImages(), productUpdate);

        productUpdate.setName(updateProductRequest.getName());
        productUpdate.setDescriptionTitle(updateProductRequest.getDescriptionTitle());
        productUpdate.setDescription(updateProductRequest.getDescription());
        productUpdate.setAddress(updateProductRequest.getAddress());
        productUpdate.setRules(updateProductRequest.getRules());
        productUpdate.setHealthAndSecurity(updateProductRequest.getHealthAndSecurity());
        productUpdate.setCancellationPolicies(updateProductRequest.getCancellationPolicies());
        productUpdate.setLatitude(updateProductRequest.getLatitude());
        productUpdate.setLongitude(updateProductRequest.getLongitude());

        return productMapper.productToProductResponse(productoRepository.save(productUpdate));
    }
    private void deleteOldImages(Product productUpdate) {
        imageRepository.deleteAllByProduct(productUpdate);
    }

    @Override
    public ListProductsResponse listProductsByFilters(Long city, String checkin, String checkout) {
        Date dateIn = Date.valueOf(reserveMapper.stringToLocalDate(checkin));
        Date dateOut = Date.valueOf(reserveMapper.stringToLocalDate(checkout));
        ListProductsResponse response = new ListProductsResponse();

        if (city == 0) {
            response.setListProductResponse(productMapper.listProductToListProductResponse(productoRepository.findByDateFilter(dateIn, dateOut)));
        } else {
            response.setListProductResponse(productMapper.listProductToListProductResponse(productoRepository.findByDateAndCityFilter(city, dateIn, dateOut)));
        }

        return response;
    }

    @Override
    public ListProductsResponse listMyProducts() {
        User userAuthenticated = (User) securityUtils.getUserAuthenticated();
        return ListProductsResponse.builder()
                .listProductResponse(productMapper.listProductToListProductResponse(productoRepository.findAllByOwner(userAuthenticated))).build();
    }

    private Product findBy(Long id) {
        Optional<Product> optionalProduct = productoRepository.findById(id);
        if (optionalProduct.isEmpty()) {
            throw new EntityNotFoundExc("Product not found.");
        }
        return optionalProduct.get();
    }

    private void buildProduct(Product product, Category category, City city, List<Feature> features, User user) {
        product.setFeatures(new HashSet<>());
        product.setCategory(category);
        product.setCity(city);
        product.setOwner(user);
        for (Feature feature : features) {
            product.getFeatures().add(feature);
        }
    }

    private Category getCategory(Long id) {
        Optional<Category> optionalCategory = categoryRepository.findById(id);
        if (optionalCategory.isEmpty()) {
            throw new EntityNotFoundExc("Category not found");
        }
        return optionalCategory.get();
    }

    private void saveImages(List<CreateImageRequest> createImageRequestList, Product product) {
        ArrayList<Image> imagenes = new ArrayList<>();
        for (CreateImageRequest imagen : createImageRequestList) {
            imagen.setProduct(product);
            imagenes.add(imageRepository.save(imageMapper.imageRequestToProduct(imagen)));
        }
        product.setImages(imagenes);
        productoRepository.save(product);
    }

    private void saveFeatures(List<Feature> features, Product productSaved) {
        for (Feature feature : features) {
            feature.getProducts().add(productSaved);
            featureRepository.save(feature);
        }
    }

    public City getCity(Long id) {
        Optional<City> optionalCity = cityRepository.findById(id);
        if (optionalCity.isEmpty()) {
            throw new EntityNotFoundExc("City not found");
        }
        return optionalCity.get();
    }

    private List<Feature> getFeatures(ArrayList<Long> featuresId) {
        List<Feature> listFeatures = new ArrayList<>();
        for (Long featureId : featuresId) {
            Optional<Feature> optionalFeature = featureRepository.findById(featureId);
            if (optionalFeature.isEmpty()) {
                throw new EntityNotFoundExc("Feature with ID: '" + featureId + "' not found");
            }
            listFeatures.add(optionalFeature.get());
        }
        return listFeatures;
    }

}
