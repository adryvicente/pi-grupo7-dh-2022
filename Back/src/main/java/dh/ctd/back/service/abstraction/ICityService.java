package dh.ctd.back.service.abstraction;

import dh.ctd.back.dto.request.CreateCityRequest;
import dh.ctd.back.dto.response.CityResponse;
import dh.ctd.back.dto.response.ListCityResponse;

public interface ICityService {

    CityResponse save(CreateCityRequest createCityRequest);
    ListCityResponse listCities();
    CityResponse getById(Long id);
    void delete(Long id);
    CityResponse update(CreateCityRequest createCityRequest, Long id);

    CityResponse getByName(String name);

}
