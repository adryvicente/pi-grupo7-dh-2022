package dh.ctd.back.service.abstraction;

import dh.ctd.back.dto.request.CreateProductoRequest;
import dh.ctd.back.dto.request.UpdateProductRequest;
import dh.ctd.back.dto.response.ListProductsResponse;
import dh.ctd.back.dto.response.ProductResponse;
import dh.ctd.back.entity.Feature;
import dh.ctd.back.exception.EntityNotFoundExc;

public interface IProductoService {

    ProductResponse save(CreateProductoRequest createProductoRequest);

    ListProductsResponse listProducts();

    ProductResponse getById(Long id);

    ListProductsResponse listProductsByCategory(Long category) throws EntityNotFoundExc;

    ListProductsResponse listProductsByCity(Long city) throws EntityNotFoundExc;

    ProductResponse update(Long id, CreateProductoRequest createProductRequest);

    ListProductsResponse listProductsByFilters(Long city, String checkin, String checkout);

    ListProductsResponse listMyProducts();
}
