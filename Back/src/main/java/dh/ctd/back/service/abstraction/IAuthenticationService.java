package dh.ctd.back.service.abstraction;

import dh.ctd.back.dto.request.AuthenticationRequest;
import dh.ctd.back.dto.response.AuthenticationResponse;

public interface IAuthenticationService {

    AuthenticationResponse login(AuthenticationRequest authenticationRequest);
}
