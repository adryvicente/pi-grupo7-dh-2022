package dh.ctd.back.service;

import dh.ctd.back.dto.request.AuthenticationRequest;
import dh.ctd.back.dto.request.RegisterRequest;
import dh.ctd.back.dto.response.AuthenticationResponse;
import dh.ctd.back.dto.response.RegisterResponse;
import dh.ctd.back.entity.City;
import dh.ctd.back.entity.Role;
import dh.ctd.back.entity.User;
import dh.ctd.back.exception.EntityNotFoundExc;
import dh.ctd.back.exception.InvalidCredentialsExc;
import dh.ctd.back.exception.UserAlreadyExistExc;
import dh.ctd.back.repository.ICityRepository;
import dh.ctd.back.repository.IRoleRepository;
import dh.ctd.back.repository.IUserRepository;
import dh.ctd.back.security.JwtUtils;
import dh.ctd.back.service.abstraction.IAuthenticationService;
import dh.ctd.back.service.abstraction.IEmailService;
import dh.ctd.back.service.abstraction.IRegisterService;
import dh.ctd.back.util.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AuthenticationServiceImpl implements IAuthenticationService, IRegisterService {

    @Autowired
    private AuthenticationManager authManager;
    @Autowired
    private JwtUtils jwtUtils;
    @Autowired
    private IUserRepository userRepository;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private IRoleRepository roleRepository;
    @Autowired
    private ICityRepository cityRepository;
    @Autowired
    private IEmailService emailService;


    @Override
    public AuthenticationResponse login(AuthenticationRequest authenticationRequest) {
        Authentication authentication;
        try {
            authentication = authManager.authenticate(new UsernamePasswordAuthenticationToken(
                    authenticationRequest.getEmail(),
                    authenticationRequest.getPassword()));
        } catch (Exception e) {
            throw new InvalidCredentialsExc("Invalid email or password.");
        }

        String jwt = jwtUtils.generateToken((UserDetails) authentication.getPrincipal());
        return new AuthenticationResponse(jwt);
    }

    @Override
    public RegisterResponse register(RegisterRequest registerRequest) {
        if(userRepository.findByEmail(registerRequest.getEmail()) != null) {
            throw new UserAlreadyExistExc("Email is already in use.");
        }

        Role userRole = roleRepository.findByName(registerRequest.getRole().getFullRoleName());
        if (userRole == null) {
            throw new EntityNotFoundExc("Missing record in role table.");
        }

        Optional<City> optionalUserCity = cityRepository.findById(registerRequest.getCityId());
        if (optionalUserCity.isEmpty()) {
            throw new EntityNotFoundExc("City not found");
        }

        User user = userMapper.userRequestToUser(registerRequest);
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setRole(userRole);
        user.setCity(optionalUserCity.get());
        user = userRepository.save(user);

        if (user != null) {
            emailService.sendWelcomeEmailTo(user.getEmail());
        }

        RegisterResponse registerResponse = userMapper.userToRegisterResponse(user);
        registerResponse.setToken(jwtUtils.generateToken(user));
        return registerResponse;
    }
}
