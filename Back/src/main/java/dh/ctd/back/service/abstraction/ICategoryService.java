package dh.ctd.back.service.abstraction;

import dh.ctd.back.dto.request.CreateCategoryRequest;
import dh.ctd.back.dto.response.CategoryResponse;
import dh.ctd.back.dto.response.ListCategoryResponse;

public interface ICategoryService {

    CategoryResponse save(CreateCategoryRequest createCategoryRequest);
    ListCategoryResponse listCategories();
    CategoryResponse getById(Long id);
    void delete(Long id);
    CategoryResponse update(CreateCategoryRequest createCategoryRequest, Long id);

}