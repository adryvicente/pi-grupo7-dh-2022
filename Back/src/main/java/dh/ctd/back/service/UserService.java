package dh.ctd.back.service;

import dh.ctd.back.dto.response.UserDataResponse;
import dh.ctd.back.entity.User;
import dh.ctd.back.repository.IUserRepository;
import dh.ctd.back.security.PasswordEncoder;
import dh.ctd.back.service.abstraction.IUserService;
import dh.ctd.back.util.SecurityUtils;
import dh.ctd.back.util.UserMapper;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class UserService implements UserDetailsService, IUserService {
    @Autowired
    private IUserRepository userRepository;
    @Autowired
    private SecurityUtils securityUtils;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private UserMapper userMapper;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        return getUser(email);
    }

    private User getUser(String username) {
        User userEntity = userRepository.findByEmail(username);
        if (userEntity == null) {
            throw new UsernameNotFoundException("User not found.");
        }
        return userEntity;
    }
    @Override
    public UserDataResponse getUserAuthenticated() {
        return userMapper.userToUserDataResponse((User) securityUtils.getUserAuthenticated());
    }
}
