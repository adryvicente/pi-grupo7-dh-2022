package dh.ctd.back.service.abstraction;

import dh.ctd.back.dto.request.CreateFeatureRequest;
import dh.ctd.back.dto.response.FeatureResponse;
import dh.ctd.back.dto.response.ListFeaturesResponse;

public interface IFeatureService {

    FeatureResponse save(CreateFeatureRequest createFeatureRequest);
    ListFeaturesResponse listFeatures();
    FeatureResponse getById(Long id);
    void delete(Long id);
    FeatureResponse update(CreateFeatureRequest createFeatureRequest, Long id);



}
