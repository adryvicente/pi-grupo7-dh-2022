package dh.ctd.back.service.abstraction;

import dh.ctd.back.dto.request.RegisterRequest;
import dh.ctd.back.dto.response.RegisterResponse;

public interface IRegisterService {

    RegisterResponse register(RegisterRequest registerRequest);
}
