package dh.ctd.back.service;

import com.fasterxml.jackson.databind.ObjectMapper;

import dh.ctd.back.dto.request.CreateCategoryRequest;
import dh.ctd.back.dto.response.CategoryResponse;
import dh.ctd.back.dto.response.ListCategoryResponse;
import dh.ctd.back.entity.Category;
import dh.ctd.back.exception.ResourceNotFoundException;
import dh.ctd.back.repository.ICategoryRepository;
import dh.ctd.back.service.abstraction.ICategoryService;
import dh.ctd.back.util.CategoryMapper;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.logging.Logger;

@Service
@AllArgsConstructor
public class CategoryServiceImpl implements ICategoryService {

    @Autowired
    private ICategoryRepository categoryRepository;
    @Autowired
    private ObjectMapper mapper;

    @Autowired
    private CategoryMapper categoryMapper;
    
    private final Logger logger = Logger.getLogger(String.valueOf(CategoryServiceImpl.class));

    public ListCategoryResponse listCategories() {

        logger.info("Listing all available categories");
        return ListCategoryResponse.builder()
        .listCategoryResponse(categoryMapper.listCategoryToListCategoryResponse(categoryRepository.findAll()))
        .build();
    }

    public CategoryResponse getById(Long id) {

         logger.info("Listing category ID: "+id);
         return categoryMapper.CategoryToCategoryResponse(categoryRepository
            .findById(id)
            .orElseThrow( () -> new ResourceNotFoundException("Category","ID", id))
         );
    }

    public CategoryResponse save(CreateCategoryRequest createCategoryRequest) {

        Category category = mapper.convertValue(createCategoryRequest, Category.class);
        logger.info("Saving new Category");
        return categoryMapper.CategoryToCategoryResponse(categoryRepository.save(category));
    }

    public void delete(Long id) {

        try{
            categoryRepository.deleteById(id);
            logger.info("Deleting category by ID: "+id);
        }catch (Exception e) {
            throw new ResourceNotFoundException("Category", "ID", id);
        }
        
    }

    public CategoryResponse update(CreateCategoryRequest createCategoryRequest, Long id) {
        
        Category category = categoryRepository
        .findById(id)
        .orElseThrow(() -> new ResourceNotFoundException("Category", "ID", id));
        
        category.setTitle(createCategoryRequest.getTitle());
        category.setDescription(createCategoryRequest.getDescription());
        category.setUrlImage(createCategoryRequest.getUrlImage());
        Category categoryUpdate = categoryRepository.save(category);
        
        logger.info("Updating category by ID: "+id);
        return categoryMapper.CategoryToCategoryResponse(categoryUpdate);       
    }

}