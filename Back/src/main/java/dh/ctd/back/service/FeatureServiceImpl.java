package dh.ctd.back.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import dh.ctd.back.dto.request.CreateFeatureRequest;
import dh.ctd.back.dto.response.FeatureResponse;
import dh.ctd.back.dto.response.ListFeaturesResponse;
import dh.ctd.back.dto.response.ListProductsResponse;
import dh.ctd.back.entity.Feature;
import dh.ctd.back.entity.Product;
import dh.ctd.back.exception.EntityNotFoundExc;
import dh.ctd.back.exception.ResourceNotFoundException;
import dh.ctd.back.repository.IFeatureRepository;
import dh.ctd.back.repository.IProductRepository;
import dh.ctd.back.service.abstraction.IFeatureService;
import dh.ctd.back.service.abstraction.IProductoService;
import dh.ctd.back.util.FeatureMapper;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.logging.Logger;

@Service
@AllArgsConstructor
public class FeatureServiceImpl implements IFeatureService {
    @Autowired
    private IFeatureRepository featureRepository;

    @Autowired
    private ObjectMapper mapper;

    @Autowired
    private FeatureMapper featureMapper;

    private final Logger logger = Logger.getLogger(String.valueOf(FeatureServiceImpl.class));

    @Override
    public FeatureResponse save(CreateFeatureRequest createFeatureRequest) {

        Feature feature = mapper.convertValue(createFeatureRequest, Feature.class);
        logger.info("Saving new Feature");
        return featureMapper.featureToFeatureResponse(featureRepository.save(feature));
    }

    @Override
    public ListFeaturesResponse listFeatures() {
        logger.info("Listing all available features");
        return ListFeaturesResponse.builder()
                .listFeatureResponse(featureMapper.listFeatureToListFeatureResponse(featureRepository.findAll()))
                .build();
    }

    @Override
    public FeatureResponse getById(Long id) {
        logger.info("Listing feature id=" +id);
        return featureMapper.featureToFeatureResponse(featureRepository
                .findById(id)
                .orElseThrow( () -> new ResourceNotFoundException("Feature", "id", id))
        );
    }

    @Override
    public void delete(Long id) {
        try {
            featureRepository.deleteById(id);
            logger.info("DELETING feature by id="+id);
        } catch (Exception e) {
            throw new ResourceNotFoundException("Feature", "id", id);
        }
    }

    @Override
    public FeatureResponse update(CreateFeatureRequest createFeatureRequest, Long id) {
        Feature feature = featureRepository
                .findById(id)
                .orElseThrow( () -> new ResourceNotFoundException("Feature", "id", id));

        feature.setName(createFeatureRequest.getName());
        feature.setIcon(createFeatureRequest.getIcon());

        Feature featureUpdated = featureRepository.save(feature);

        logger.info("UPDATING feature by id="+id);
        return featureMapper.featureToFeatureResponse(featureUpdated);
    }


}
