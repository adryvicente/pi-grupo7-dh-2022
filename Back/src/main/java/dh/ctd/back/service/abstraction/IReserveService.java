package dh.ctd.back.service.abstraction;

import dh.ctd.back.dto.request.CreateReserveRequest;
import dh.ctd.back.dto.response.ListReservesResponse;
import dh.ctd.back.dto.response.ReserveResponse;
import dh.ctd.back.exception.EntityNotFoundExc;

public interface IReserveService {


    ReserveResponse save(CreateReserveRequest createReserveRequest);

    ReserveResponse getById(Long id);

     void delete (Long id);

    ReserveResponse update(CreateReserveRequest createReserveRequest);


    ListReservesResponse listReserves();


    ListReservesResponse listReservesByProduct(Long product) throws EntityNotFoundExc;


    ListReservesResponse listMyReserves();
}
