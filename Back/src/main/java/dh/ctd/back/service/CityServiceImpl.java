package dh.ctd.back.service;


import com.fasterxml.jackson.databind.ObjectMapper;
import dh.ctd.back.dto.request.CreateCityRequest;
import dh.ctd.back.dto.response.CityResponse;
import dh.ctd.back.dto.response.ListCityResponse;
import dh.ctd.back.entity.City;
import dh.ctd.back.exception.ResourceNotFoundException;
import dh.ctd.back.repository.ICityRepository;
import dh.ctd.back.service.abstraction.ICityService;
import dh.ctd.back.util.CityMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.logging.Logger;

@Service
public class CityServiceImpl implements ICityService {

    @Autowired
    private ICityRepository cityRepository;

    @Autowired
    private ObjectMapper mapper;

    @Autowired
    private CityMapper cityMapper;

    private final Logger logger = Logger.getLogger(String.valueOf(CityServiceImpl.class));


    @Override
    public CityResponse save(CreateCityRequest createCityRequest) {
        City city = mapper.convertValue(createCityRequest, City.class);
        logger.info("Saving new City");
        return cityMapper.cityToCityResponse(cityRepository.save(city));
    }

    @Override
    public ListCityResponse listCities() {
        logger.info("Listing all stored cities");
        return ListCityResponse.builder()
                .listCityResponse(cityMapper.listCityToListCityResponse(cityRepository.findAll()))
                .build();
    }

    @Override
    public CityResponse getById(Long id) {
        logger.info("Showing city by id=" + id);
        return cityMapper.cityToCityResponse(cityRepository
                .findById(id)
                .orElseThrow( () -> new ResourceNotFoundException("City", "id", id))
        );
    }

    @Override
    public void delete(Long id) {
        try {
            cityRepository.deleteById(id);
            logger.info("DELETING city by id=" + id);
        } catch (Exception e) {
            throw new ResourceNotFoundException("City", "id", id);
        }

    }

    @Override
    public CityResponse update(CreateCityRequest createCityRequest, Long id) {


        City city = cityRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("City", "Id", id));


        city.setName(createCityRequest.getName());
        city.setProvince(createCityRequest.getProvince());
        city.setCountry(createCityRequest.getCountry());

        City cityUpdated = cityRepository.save(city);
        logger.info("UPDATING city by id=" + id);
        return cityMapper.cityToCityResponse(cityUpdated);
    }

    @Override
    public CityResponse getByName(String name) {
        return cityMapper.cityToCityResponse(cityRepository
                        .findCityByName(name)

        );
    }

}
