package dh.ctd.back.service.abstraction;

import dh.ctd.back.entity.Product;
import dh.ctd.back.entity.Reserve;
import dh.ctd.back.entity.User;

public interface IEmailService {

    void sendWelcomeEmailTo(String to);

    void sendReserveEmail(User user, Product product, Reserve reserve);
}
