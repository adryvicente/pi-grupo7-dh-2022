package dh.ctd.back.util.email;

public class WelcomeTemplate {

    public String getBody() {
        return "<body style=\"padding: 5px 10px; max-width: 600px; background-color:#F0572D;"
                + " font-family: 'Roboto', sans-serif; color: #FFF;\">"
                + "<div style=\"border: 1px solid grey; width: 90%; margin: 10px 20px;\"></div>"
                + "<div style=\"margin: 20px 20px;\">"
                + "<h2>Bienvenido a Digital Booking 👌" + "</h2>"
                + "<br>"
                + "<p style=\"font-size:130%;\">Estamos muy contentos de que te hayas unido a nosotros!.</p>"
                + "</div>"
                + "<br>"
                + "<div style=\"border: 1px solid grey; width: 90%; margin: 10px 20px;\"></div>"
                + "</body>";
    }
}
