package dh.ctd.back.util.email;

import dh.ctd.back.dto.response.ReserveResponse;
import dh.ctd.back.entity.Product;
import dh.ctd.back.entity.Reserve;
import dh.ctd.back.entity.User;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
public class ReserveTemplate {

    private User user;
    private Product product;
    private Reserve reserve;

    public String getBody(User user, Product product, Reserve reserve) {
        return "<body style=\"padding: 5px 10px; max-width: 600px; background-color:#F0572D;"
                + " font-family: 'Roboto', sans-serif; border-radius: 10px; color: #FFF;\">"
                + "<div style=\"margin: 20px 20px;\">"
                + "<h2>Felicitaciones " + user.getName() + ", su reserva ha sido generada con éxito!" + "</h2>"
                + "<br>"
                + "<p style=\"font-size:130%; width:80%\">A continuación le enviamos los detalles de su reserva en <b>" + product.getName() +"</b>.</p>"
                + "</div>"
                + "<br>"
                + "<div style=\"border: 1px solid grey; width: 90%; margin: 10px 20px; padding: 8px; border-radius: 5px; background-color:#FFFFFF; color: #000000\">"
                + "<small>- Código de reserva: " + reserve.getIdReserve()
                + "<br>" + "- Fecha de ingreso: " + reserve.getCheckIn()
                + "<br>" + "- Fecha de salida: " + reserve.getCheckIn()
                + "<br>" + "- Horario de ingreso: " + reserve.getHourIn()
                + "<br>" + "- Dirección: " + product.getAddress()
                + "<br>" + "- Link al producto: http://localhost:8080/products/" + product.getIdProduct()
                + "</small>"
                + "</div>"
                + "</body>";
    }
}
