package dh.ctd.back.util;

import dh.ctd.back.dto.request.RegisterRequest;
import dh.ctd.back.dto.response.RegisterResponse;
import dh.ctd.back.dto.response.UserDataResponse;
import dh.ctd.back.entity.User;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {

    public User userRequestToUser(RegisterRequest registerRequest) {
        User user = User.builder()
                .name(registerRequest.getName())
                .lastname((registerRequest.getLastname()))
                .email(registerRequest.getEmail())
                .password(registerRequest.getPassword())
                .build();

        return user;
    }

    public RegisterResponse userToRegisterResponse(User user) {
        RegisterResponse registerResponse = RegisterResponse.builder()
                .id(user.getIdUser())
                .name(user.getName())
                .lastname(user.getLastname())
                .email(user.getEmail())
                .build();
        return registerResponse;
    }

    public UserDataResponse userToUserDataResponse(User user) {
        UserDataResponse userDataResponse = UserDataResponse.builder()
                .idUser(user.getIdUser())
                .name(user.getName())
                .lastname(user.getLastname())
                .email(user.getEmail())
                .city(user.getCity().getName())
                .role(user.getRole().getName())
                .build();
        return userDataResponse;
    }
}
