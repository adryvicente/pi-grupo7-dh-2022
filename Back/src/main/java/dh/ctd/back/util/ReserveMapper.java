package dh.ctd.back.util;

import dh.ctd.back.dto.request.CreateReserveRequest;
import dh.ctd.back.dto.response.ReserveResponse;
import dh.ctd.back.entity.Product;
import dh.ctd.back.entity.Reserve;
import dh.ctd.back.entity.User;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Component
public class ReserveMapper {

    public Reserve ReserveRequestToReserve(CreateReserveRequest createReserveRequest, Product product, User user){

        return Reserve.builder()
                .hourIn(stringToLocalTime(createReserveRequest.getHourIn()))
                .checkIn(stringToLocalDate(createReserveRequest.getCheckIn()))
                .checkOut(stringToLocalDate(createReserveRequest.getCheckOut()))
                .product(product)
                .customer(user)
                .build();
    }

    public ReserveResponse ReserveToReserveResponse(Reserve reserve){
        return ReserveResponse.builder()
                .idReserve(reserve.getIdReserve())
                .hourIn(localTimeToString(reserve.getHourIn()))
                .checkIn(localDateToString(reserve.getCheckIn()))
                .checkOut(localDateToString(reserve.getCheckOut()))
                .productId(reserve.getProduct().getIdProduct())
                .customerId(reserve.getCustomer().getIdUser())
                .build();
    }


    public LocalDate stringToLocalDate (String stringDate){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate date = LocalDate.parse(stringDate, formatter);
        return date;
    }

    public String localDateToString(LocalDate date){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        String stringDate = date.format(formatter);
        return stringDate;
    }

    public LocalTime stringToLocalTime (String stringTime){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm");
        LocalTime hour = LocalTime.parse(stringTime, formatter);
        return hour;
    }

    public String localTimeToString(LocalTime time){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm");
        String stringHour = time.format(formatter);
        return stringHour;
    }


    public List<ReserveResponse> listReservesToListReservesResponse(List<Reserve> reservesList) {
        ArrayList<ReserveResponse> listReservesResponse = new ArrayList<>();
        for (Reserve reserve : reservesList){
            listReservesResponse.add(this.ReserveToReserveResponse(reserve));
        }
        return listReservesResponse;
    }



}
