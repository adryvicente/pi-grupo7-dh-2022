package dh.ctd.back.util;

import dh.ctd.back.dto.response.FeatureResponse;
import dh.ctd.back.entity.Feature;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class FeatureMapper {
    public FeatureResponse featureToFeatureResponse(Feature feature){
        return FeatureResponse.builder()
                .featureId(feature.getFeatureId())
                .name(feature.getName())
                .icon(feature.getIcon())
                .build();
    }

    public List<FeatureResponse> listFeatureToListFeatureResponse(List<Feature> featureList){
        ArrayList<FeatureResponse> listFeatureResponse = new ArrayList<>();
        for (Feature feature : featureList) {
            listFeatureResponse.add(this.featureToFeatureResponse(feature));
        }
        return listFeatureResponse;
    }


}
