package dh.ctd.back.util;

import dh.ctd.back.dto.response.CategoryResponse;
import dh.ctd.back.entity.Category;
import org.springframework.stereotype.Component;
import java.util.ArrayList;
import java.util.List;

@Component
public class CategoryMapper {
    public CategoryResponse CategoryToCategoryResponse(Category category){
        return CategoryResponse.builder()
        .idCategory(category.getIdCategory())
        .title(category.getTitle())
        .description(category.getDescription())
        .urlImage(category.getUrlImage())
        .build();
    }

    public List<CategoryResponse> listCategoryToListCategoryResponse(List<Category> categoryList){
        ArrayList<CategoryResponse> listCategoryResponse = new ArrayList<>();
        for(Category category : categoryList) {
            listCategoryResponse.add(this.CategoryToCategoryResponse(category));
        }
        return listCategoryResponse;
    }   
}