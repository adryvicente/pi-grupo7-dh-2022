package dh.ctd.back.util;

import dh.ctd.back.dto.response.CityResponse;
import dh.ctd.back.entity.City;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class CityMapper {

    public CityResponse cityToCityResponse(City city) {
        return  CityResponse.builder()
                .idCity(city.getIdCity())
                .name(city.getName())
                .province(city.getProvince())
                .country(city.getCountry())
                .build();
    }


    public List<CityResponse> listCityToListCityResponse(List<City> cityList) {
        ArrayList<CityResponse> listCityResponse = new ArrayList<>();
        for (City city : cityList) {
            listCityResponse.add(this.cityToCityResponse(city));
        }
        return listCityResponse;
    }
}
