package dh.ctd.back.util;

import dh.ctd.back.dto.request.CreateProductoRequest;
import dh.ctd.back.dto.response.ProductResponse;
import dh.ctd.back.entity.Feature;
import dh.ctd.back.entity.Image;
import dh.ctd.back.entity.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ProductMapper {

    @Autowired
    private FeatureMapper featureMapper;

    @Autowired
    private ImageMapper imageMapper;
    @Autowired
    private CityMapper cityMapper;

    public ProductResponse productToProductResponse(Product product) {
        ArrayList<Feature> features = new ArrayList<>();
        features.addAll(product.getFeatures());

        ArrayList<Image> images = new ArrayList<>();
        images.addAll(product.getImages());

        return ProductResponse.builder()
                .idProduct(product.getIdProduct())
                .name(product.getName())
                .descriptionTitle(product.getDescriptionTitle())
                .description(product.getDescription())
                .address(product.getAddress())
                .categoryTitle(product.getCategory().getTitle())
                .city(cityMapper.cityToCityResponse(product.getCity()))
                .features(featureMapper.listFeatureToListFeatureResponse(features))
                .images(imageMapper.listImageToListImageResponse(images))//ListFeatureResponse
                .rules(product.getRules())
                .helathAndSecurity(product.getHealthAndSecurity())
                .cancellationPolicies(product.getCancellationPolicies())
                .latitude(product.getLatitude())
                .longitude(product.getLongitude())
                .owner(product.getOwner().getName())
                .build();
    }

    public List<ProductResponse> listProductToListProductResponse(List<Product> productList) {
        ArrayList<ProductResponse> listProductResponse = new ArrayList<>();
        for (Product product : productList) {
            listProductResponse.add(this.productToProductResponse(product));
        }
        return listProductResponse;
    }

    public Product productRequestToProduct(CreateProductoRequest productoRequest) {

        return Product.builder()
                .name(productoRequest.getName())
                .descriptionTitle(productoRequest.getDescriptionTitle())
                .description(productoRequest.getDescription())
                .address(productoRequest.getAddress())
                .rules(productoRequest.getRules())
                .healthAndSecurity(productoRequest.getHealthAndSecurity())
                .cancellationPolicies(productoRequest.getCancellationPolicies())
                .latitude(productoRequest.getLatitude())
                .longitude(productoRequest.getLongitude())
                .build();
    }

}
