package dh.ctd.back.util;

import dh.ctd.back.dto.request.CreateImageRequest;
import dh.ctd.back.dto.response.ImageResponse;
import dh.ctd.back.entity.Image;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ImageMapper {
    public ImageResponse imageToImageResponse(Image image){
        return ImageResponse.builder()
                .imageId(image.getImageId())
                .title(image.getTitle())
                .imageUrl(image.getImageUrl())
                .build();
    }

    public List<ImageResponse> listImageToListImageResponse(List<Image> imageList) {
        ArrayList<ImageResponse> listImageResponse = new ArrayList<>();
        for (Image image : imageList) {
            listImageResponse.add(this.imageToImageResponse(image));
        }
        return listImageResponse;
    }

    public Image imageRequestToProduct(CreateImageRequest createImageRequest) {
        return Image.builder()
                .title(createImageRequest.getTitle())
                .imageUrl(createImageRequest.getImageUrl())
                .product(createImageRequest.getProduct())// Aca esta el error
                .build();
    }
}
