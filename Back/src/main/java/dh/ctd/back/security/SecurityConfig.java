package dh.ctd.back.security;

import dh.ctd.back.security.filter.CustomAccessDeniedHandler;
import dh.ctd.back.security.filter.CustomAuthenticationEntryPoint;
import dh.ctd.back.security.filter.JwtRequestFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.Filter;

@EnableWebSecurity
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private JwtRequestFilter jwtRequestFilter;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Bean
    public AccessDeniedHandler accessDeniedHandler() {
        return new CustomAccessDeniedHandler();
    }

    @Bean
    public AuthenticationEntryPoint authenticationEntryPoint() {
        return new CustomAuthenticationEntryPoint();
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    public void configure(AuthenticationManagerBuilder managerBuilder) throws Exception {
        managerBuilder.userDetailsService(userDetailsService)
                .passwordEncoder(passwordEncoder);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf()
                .disable()
                .cors()
                .and()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeRequests()
                .antMatchers("/api/docs/**")
                .permitAll()
                .antMatchers("/v2/api-docs")
                .permitAll()
                .antMatchers(HttpMethod.GET,"/cities")
                .permitAll()
                .antMatchers(HttpMethod.GET,"/cities/*")
                .permitAll()
                .antMatchers(HttpMethod.GET,"/cities/name/*")
                .permitAll()
                .antMatchers(HttpMethod.GET,"/categories/")
                .permitAll()
                .antMatchers(HttpMethod.GET,"/categories/*")
                .permitAll()
                .antMatchers(HttpMethod.POST, "/auth/login")
                .permitAll()
                .antMatchers(HttpMethod.POST, "/auth/register")
                .permitAll()
                .antMatchers(HttpMethod.GET, "/auth/me")
                .hasAnyRole(Role.ADMIN.name(), Role.USER.name())
                .antMatchers(HttpMethod.GET, "/products/filters/*")
                .permitAll()
                .antMatchers(HttpMethod.GET, "/products")
                .permitAll()
                .antMatchers(HttpMethod.GET, "/products/*")
                .permitAll()
                .antMatchers(HttpMethod.GET, "/reserves/products/*")
                .permitAll()
                .antMatchers(HttpMethod.GET, "/reserves")
                .hasRole(Role.USER.name())
                .antMatchers(HttpMethod.GET, "/products/category/*")
                .permitAll()
                .antMatchers(HttpMethod.GET, "/products/cities/*")
                .permitAll()
                .antMatchers(HttpMethod.GET, "/features/")
                .permitAll()
                .antMatchers(HttpMethod.GET, "/features/*")
                .permitAll()
                .antMatchers(HttpMethod.POST, "/reserves")
                .permitAll()
                .antMatchers(HttpMethod.POST, "/categories")
                .hasRole(Role.ADMIN.name())
                .antMatchers(HttpMethod.POST, "/products")
                .hasRole(Role.ADMIN.name())
                .antMatchers(HttpMethod.GET, "/products/my-products")
                .hasRole(Role.ADMIN.name())
                .antMatchers(HttpMethod.PATCH, "/products")
                .hasRole(Role.ADMIN.name())
                .anyRequest()
                .authenticated()
                .and()
                .addFilterBefore((Filter) jwtRequestFilter, UsernamePasswordAuthenticationFilter.class)
                .exceptionHandling()
                .accessDeniedHandler(accessDeniedHandler())
                .authenticationEntryPoint(authenticationEntryPoint());
    }

}
