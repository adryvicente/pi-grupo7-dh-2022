package dh.ctd.back.entity;


import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "RESERVES")
public class Reserve {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "RESERVE_ID")
    private Long idReserve;

    @Column(name = "HOUR_IN")
    private LocalTime hourIn;
    @Column(name = "CHECK_IN")
    private LocalDate checkIn;
    @Column(name = "CHECK_OUT")
    private LocalDate checkOut;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PRODUCT_ID")
    private Product product;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "USER_ID")
    private User customer;

}
