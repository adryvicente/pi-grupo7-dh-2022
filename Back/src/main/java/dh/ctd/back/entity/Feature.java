package dh.ctd.back.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "FEATURES")
public class Feature {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "FEATURE_ID")
    private Long featureId;

    @Column(name = "NAME")
    private String name;

    @Column(name = "ICON")
    private String icon;

    @ManyToMany(mappedBy = "features", fetch = FetchType.EAGER)
    private Set<Product> products;

}
