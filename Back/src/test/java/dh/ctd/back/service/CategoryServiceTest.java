/*package dh.ctd.back.service;

import dh.ctd.back.dto.CategoryDTO;
import dh.ctd.back.exception.ConflictExc;
import dh.ctd.back.service.abstraction.ICategoryService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

@SpringBootTest
public class CategoryServiceTest {

    @Autowired
    ICategoryService categoryService;

    public CategoryDTO createNewCategory() {
        CategoryDTO category = new CategoryDTO();
        category.setTitle("Titulo X");
        category.setDescription("Descripción del titulo X");
        category.setUrlImage("https://urldelaimagendedescripcionX.com");

        return category;
    }

    @Test
    void saveCategoryTest() throws NoFoundExc, ConflictExc {
        CategoryDTO category = createNewCategory();
        CategoryDTO categorySaved = categoryService.saveCategory(category);
        CategoryDTO categoryFound = categoryService.findCategoryById(categorySaved.getIdCategory());
        assertEquals("Titulo X", categoryFound.getTitle());
    }

    @Test
    void findAllCategoriesTest() throws ConflictExc {

       // categoryService.saveCategoria(createNewCategory());
        List<CategoryDTO> categories = categoryService.findAllCategories();
        assertTrue(categories.size() >= 1);
        
    }

    @Test
    void searchCategoryTest() throws NoFoundExc, ConflictExc {
        CategoryDTO categoria = createNewCategory();
        CategoryDTO catSaved = categoryService.saveCategory(categoria);
        CategoryDTO catFound = categoryService.findCategoryById(catSaved.getIdCategory());
        assertEquals("Titulo X", catFound.getTitle());
    }


    @Test
    void updateCategoryTest() throws NoFoundExc, ConflictExc {
        CategoryDTO category = createNewCategory();
        CategoryDTO catSaved = categoryService.saveCategory(category);
        CategoryDTO catFound = categoryService.findCategoryById(catSaved.getIdCategory());
        assertEquals("Titulo X", catFound.getTitle());
        CategoryDTO catUpdate = catFound;
        catUpdate.setDescription("Descripcion del titulo actualizada");
        catUpdate.setTitle("Titulo ACTUALIZADO");
        catUpdate.setUrlImage("https://urldelaimagendedescripcionxACTUALIZADA.com");
        categoryService.updateCategory(catUpdate);
        CategoryDTO catEncontrada = categoryService.findCategoryById(catSaved.getIdCategory());
        assertEquals("Titulo ACTUALIZADO", catEncontrada.getTitle());
    }

    @Test
    void deleteCategoriaTest() throws NoFoundExc, ConflictExc {
        CategoryDTO category = createNewCategory();
        CategoryDTO catSaved = categoryService.saveCategory(category);
        CategoryDTO catFound = categoryService.findCategoryById(catSaved.getIdCategory());
        assertEquals("Titulo X", catFound.getTitle());
        categoryService.deleteCategory(catSaved.getIdCategory());

        NoFoundExc thrown = assertThrows(NoFoundExc.class, () -> {
            categoryService.findCategoryById(catSaved.getIdCategory());
        });
        assertEquals("La category con Id " + catSaved.getIdCategory() + " no es posible encontrarla o no existe.",thrown.getMessage());

    }
  
}

 */
